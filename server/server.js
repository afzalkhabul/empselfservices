var loopback = require('loopback');
var boot = require('loopback-boot');

var app = module.exports = loopback();

app.use('**', function (req,res,next) {

  //console.log("Req Headers::"+JSON.stringify(req.headers.access_token));
  if(req.headers.access_token!=undefined && req.headers.access_token!="undefined" && req.headers.access_token!=null && req.headers.access_token!="null" && req.headers.access_token!='') {

  //console.log('accesstoken '+req.headers.access_token);
    var accessToken = req.headers.access_token;
    var SessionInfo = app.models.SessionInfo;
    SessionInfo.find({"where":{"accessToken":accessToken}}, function (err, data){
      if(err){
        next(err,null);
      }else if (data) {

        var currentTime = new Date();
        if(data.length > 0) {
          var currentSession = data[0];
          var lastAccessedTime = currentSession.lastAccessedTime;
          var timeInterval;
          timeInterval = currentTime.getTime() - lastAccessedTime.getTime();
          if(timeInterval < currentSession.sessionDuration){
            currentSession.updateAttributes({"lastAccessedTime":currentTime},function(err,updatedInfo){
              if(err){
                next(err,null);
              }else {
                next();
              }
            });
          }else{
            var error = new Error("Session Expired");
            error.statusCode = 401;
          /*  next(error);
            var error = {'message':'Session Expired'};
*/
            next(error,null);
          }
        }else{
          SessionInfo.create({"accessToken":accessToken,"lastAccessedTime":currentTime,"sessionDuration":(60*60*1000)},function(err,newSession){
            if(err){
              next(err,null);
            }else{
              next();
            }
          });
        }
      }
    });
  }else{
    next();
  }
});

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
//    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
//      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
