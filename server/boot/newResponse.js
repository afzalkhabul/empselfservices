module.exports = function (server) {

  var remotes = server.remotes();
    remotes.before('**', function (ctx, next) {
      var Citizen = server.models.Citizen;
      if ((ctx.req.originalUrl.toString().indexOf('/api/Citizens/login') > -1 ) && ctx.req.method.toString() == 'POST') {
        ctx.req.body['ttl'] = 60*60*1000;
        next();
      }else  if (ctx.req.originalUrl.toString().indexOf('/api/CreateCases') > -1 && ctx.req.method.toString() == "PUT") {
      var createCase = server.models.CreateCase;
      createCase.findById(ctx.instance.id,function (err, Createcase) {
        var advName = Createcase.advocate;
        var caseStatus = Createcase.status;
        ctx.instance['caseStatus']=caseStatus;
        ctx.instance['advName'] = advName;
      });
      var Createcase = server.models.CreateCase;
      var CaseHistory = server.models.CaseHistory;
      var CaseUpload = server.models.CaseUpload;
      var Employee = server.models.Employee;
      Createcase.findById(ctx.instance.id, function (err, emp) {
        var caseJson = {
          "caseDetails": emp.caseDetails,
          "caseNumber": emp.caseNumber,
          "caseType": emp.caseType,
          "advocate": emp.advocate,
          "status": emp.status,
          "priority": emp.priority,
          "act": emp.act,
          "stakeHolder": emp.stakeHolder,
          "department": emp.department,
          "courtName": emp.courtName,
          "caseDate": emp.caseDate,
          "empannel": emp.empannel,
          "createdTime": emp.createdTime,
          "createdPerson": emp.createdPerson,
        }

        CaseHistory.create(caseJson);
        next()
      });
    }
    else if (ctx.req.originalUrl.toString().indexOf('/api/CaseTypes') > -1 && ctx.req.method.toString() == "PUT"){
      var casetype = server.models.CaseType;
      casetype.findById(ctx.instance.id,function (err, Casetype) {
        var previousType = Casetype.caseType;
        ctx.instance['previousType'] = previousType;
       next();
      });
    }
    else if (ctx.req.originalUrl.toString().indexOf('/api/Courts') > -1 && ctx.req.method.toString() == "PUT"){
      var court = server.models.Court;
      court.findById(ctx.instance.id,function (err, Court) {
        var previousCourt = Court.name;
        ctx.instance['previousCourt'] = previousCourt;
        next();
      });
    }

    else if (ctx.req.originalUrl.toString().indexOf('/api/Priorities') > -1 && ctx.req.method.toString() == "PUT"){
      var priority = server.models.Priority;
      priority.findById(ctx.instance.id,function (err, Priority) {
        var previousPriority = Priority.priority;
        ctx.instance['previousPriority'] = previousPriority;
        next();
      });
    }

    else if (ctx.req.originalUrl.toString().indexOf('/api/Statuses') > -1 && ctx.req.method.toString() == "PUT"){
      var status = server.models.Status;
      status.findById(ctx.instance.id,function (err, Status) {
        var previousStatus = Status.field1;
        ctx.instance['previousStatus'] = previousStatus;
        next();
      });
    }

    else if (ctx.req.originalUrl.toString().indexOf('/api/caseDepartments') > -1 && ctx.req.method.toString() == "PUT"){
      var Department = server.models.caseDepartment;
      Department.findById(ctx.instance.id,function (err, caseDepartment) {
        var previousDepartment = caseDepartment.name;
        ctx.instance['previousDepartment'] = previousDepartment;
        next();
      });
    }
    else if (ctx.req.originalUrl.toString().indexOf('/api/Acts') > -1 && ctx.req.method.toString() == "PUT"){
      var acts = server.models.Acts;
      acts.findById(ctx.instance.id,function (err, Acts) {
        var previousAct = Acts.actNumber;
        ctx.instance['previousAct'] = previousAct;
        next();
      });
    }
    else if (ctx.req.originalUrl.toString().indexOf('/api/Advocates') > -1 && ctx.req.method.toString() == "PUT"){
      var advocate = server.models.Advocate;
      advocate.findById(ctx.instance.id,function (err, Advocate) {
        var previousAdvocate = Advocate.name;
        var previousEmail = Advocate.email;
        ctx.instance['previousAdvocate'] = previousAdvocate;
        ctx.instance['previousEmail'] = previousEmail;
        next();
      });
    }
    else if (ctx.req.originalUrl.toString().indexOf('/api/EmpannelGroups') > -1 && ctx.req.method.toString() == "PUT"){
      var empannelGroup = server.models.EmpannelGroup;
      empannelGroup.findById(ctx.instance.id,function (err, EmpannelGroup) {
        var previousGroup = EmpannelGroup.groupName;
        ctx.instance['previousGroup'] = previousGroup;
        next();
      });
    }
    else if (ctx.req.originalUrl.toString().indexOf('/api/StakeHolders') > -1 && ctx.req.method.toString() == "PUT"){
      var stakeHolder = server.models.StakeHolder;
      stakeHolder.findById(ctx.instance.id,function (err, StakeHolder) {
        var previousHolder = StakeHolder.name;
        var previousMail = StakeHolder.email;
        ctx.instance['previousHolder'] = previousHolder;
        ctx.instance['previousMail'] = previousMail;
        next();
      });
    }

   else if (ctx.req.originalUrl.toString().indexOf('/api/HearingDates') > -1 && ctx.req.method.toString() == "PUT") {
      var hearingDate = server.models.HearingDate;
      var hearingHistory = server.models.HearingHistory;
      hearingDate.findById(ctx.instance.id,function (err, HearingDate) {
        var advoName = HearingDate.advocateName;
        var previousStatus = HearingDate.status;
        ctx.instance['previousStatus']=previousStatus;
        ctx.instance['advoName'] = advoName;
        var datePrevious = HearingDate.dateHearing;
        ctx.instance['datePrevious']=datePrevious;
        var hearingJson = {
          "caseNumber": HearingDate.caseNumber,
          "status": HearingDate.status,
          "advocateName": HearingDate.advocateName,
          "dateHearing": HearingDate.dateHearing,
          "comment": HearingDate.comment,

        }
        hearingHistory.create(hearingJson)
      });
      next()
    }

    else if (ctx.req.originalUrl.toString().indexOf('/api/Judgements') > -1 && ctx.req.method.toString() == "PUT") {
      var judgement = server.models.Judgement;
      var judgementHistory = server.models.JudgementHistory;
      judgement.findById(ctx.instance.id,function (err, Judgement) {
        var judgementJson = {
          "caseNumber": Judgement.caseNumber,
          "status": Judgement.status,
          "court": Judgement.court,
          "judgementDate": Judgement.judgementDate,
          "judgementDetails": Judgement.judgementDetails,
        }
        judgementHistory.create(judgementJson)
      });
      next()
    }

    else if(ctx.req.originalUrl.toString().indexOf('/api/ProjectComments') > -1 && ctx.req.method.toString() == "PUT"){

      var ProjectComments = server.models.ProjectComments;
      var ProjectHistory = server.models.ProjectHistory;
      ProjectComments.findById(ctx.req.params.id,function (err, comment) {
        var  previousCommnets=ctx.req.body;
        if(comment!=undefined && comment!=null ){
          if(previousCommnets!=undefined && previousCommnets!=null ){

            if((previousCommnets.comment!=undefined && previousCommnets.comment!=null && previousCommnets.comment.trim()!=comment.comment.trim())
           || (previousCommnets.createdPerson!=undefined && previousCommnets.createdPerson!=null && previousCommnets.createdPerson.trim()!=comment.createdPerson.trim())
           || (previousCommnets.visitorName!=undefined && previousCommnets.visitorName!=null && previousCommnets.visitorName.trim()!=comment.visitorName.trim())
           || (previousCommnets.planId!=undefined && previousCommnets.planId!=null && previousCommnets.planId.trim()!=comment.planId.trim())
            ){
              var details={
                "objectDataId": comment.id+'',
                "comment": comment.comment,
                "createdPerson": comment.createdPerson,
                "visitorName": comment.visitorName,
                "planId": comment.planId,
                "type":"projectComments",
                "createdTime":new Date()
              }
              ProjectHistory.create(details);
              next();
            }else {
              next();
            }

          }
        }else{
          next();
        }
      });
    }
    else {

      next();
    }
  })


  remotes.after('**', function (ctx, next) {
    var Employee = server.models.Employee;
    var Citizen = server.models.Citizen;
    var SessionInfo = server.models.SessionInfo;
    var RoleModel = server.models.RoleModel;
    var RoleEmployees = server.models.RoleEmployees;
    if (ctx.req.originalUrl.toString().indexOf('/api/Employees/login') > -1) {
       Employee.findById(ctx.result.userId, function (err, employee) {
        if (err) {
          next(err, null);
        } else {
          ctx.result['roleObject']={};
          ctx.req.body['ttl'] = 15000;
          ctx.result['userInfo'] = employee;
          if(employee.role=='superAdmin'){
            addSchemeRole(ctx.result,next);
            addProjectRole(ctx.result,next);
            addLegalRole(ctx.result,next);
            addLandRole(ctx.result,next);
            addWorkflowRole(ctx.result,next);
          }else if(employee.role=='schemeAdmin'){
            addSchemeRole(ctx.result,next);
            addWorkflowRole(ctx.result,next);
          }else if(employee.role=='projectAdmin'){

            addProjectRole(ctx.result,next);
            addWorkflowRole(ctx.result,next);
          }else if(employee.role=='legalAdmin'){

            addLegalRole(ctx.result,next);
            addWorkflowRole(ctx.result,next);
          }else if(employee.role=='landAdmin'){

            addLandRole(ctx.result,next);
            addWorkflowRole(ctx.result,next);
          }else {
            RoleEmployees.find({"where":{"status": "Active"}}, function (err, roleModelList) {
              if(roleModelList!=null && roleModelList.length>0){
                var roleList=[];
                for(var i=0;i<roleModelList.length;i++){
                  if(roleModelList[i].formDetails!=undefined && roleModelList[i].formDetails!=null && roleModelList[i].formDetails.length>0){
                    for(var j=0;j<roleModelList[i].formDetails.length;j++){
                        if(roleModelList[i].formDetails[j].view!=undefined && roleModelList[i].formDetails[j].view){
                          if(roleModelList[i].formDetails[j].employeeId==ctx.result.userInfo.employeeId){
                            var object={
                              'id':roleModelList[i].roleId
                            }
                            roleList.push(object);
                          }
                        }

                    }
                  }
                }
                RoleModel.find({'where':{'or':roleList}},function(err, roleFormDetails){
                  if(roleFormDetails!=null && roleFormDetails.length>0){
                    checkAccessDetailsDetails(ctx.result,roleFormDetails,next);
                  }else{
                    next();
                  }
                });
              }else{
                next();
              }
            });

          }
        }
      });
    }

    else if (ctx.req.originalUrl.toString().indexOf('/api/CreateCases') > -1 && ctx.req.method.toString() == "PUT") {
      var currenctStatus = ctx.instance.status;
      var currentCourt = ctx.instance.courtName;
      var judgement = server.models.Judgement;
      var hearingDate = server.models. HearingDate
      for (var j = 0; j < ctx.instance.advocate.length; j++) {
        var exist = false;
        for(var k = 0; k < ctx.instance.advName.length; k++){
          if (ctx.instance.advName[k].email == ctx.instance.advocate[j].email) {
            exist = true;
            break;
          }
        }
        if(exist == false){
            var Emailtemplete = server.models.EmailTemplete;
            var Advocate = server.models.Advocate;
            var Sms = server.models.Sms;
            Emailtemplete.find({"where": {"emailType": "legal"}}, function (err, legalemailtemplate) {
              var Dhanbademail = server.models.DhanbadEmail;
              for (var i = 0; i < ctx.instance.advocate.length; i++) {
                Dhanbademail.create({
                  "to": ctx.instance.advocate[i].email,
                  "subject": legalemailtemplate[0].caseSubject,
                  "text": "Details :For Case Number : " + ctx.instance.caseNumber + " You Are Assigned" + legalemailtemplate[0].caseText
                }, function (err, email) {
                });
                if(legalemailtemplate[0].caseSMS) {
                  Advocate.find({"where": {"email": ctx.instance.advocate[i].email}}, function (err, advocateData) {
                    if (err) {

                    } else {
                      if (advocateData.length > 0) {
                        if (advocateData[0].phoneNumber) {
                          var smsData = {
                            "message": legalemailtemplate[0].caseSMS,
                            "mobileNo": advocateData[0].phoneNumber,
                            "smsservicetype": "singlemsg"
                          };
                          Sms.create(smsData, function (err, smsInfo) {
                          });
                        }
                      }
                    }
                  });
                }
              }
            });
        }
      }
      if(ctx.instance.caseStatus!=ctx.instance.status){

        judgement.find({"where":{"caseNumber":ctx.instance.caseNumber},
          order:'judgementDate DESC'},function (err, Judgement) {
          if(Judgement.length>0) {
            Judgement[0].updateAttributes({status: currenctStatus, court: currentCourt}, function (err, sucuss) {
             })
          }
          hearingDate.find({"where":{"caseNumber":ctx.instance.caseNumber},
            order:'dateHearing DESC'},function (err, HearingDate) {
            if(HearingDate.length>0) {
              HearingDate[0].updateAttributes({status: currenctStatus}, function (err, sucuss) {
              })
            }
            next();
          });
        });
      }
      else{
        next();
      }
    }

    else if (ctx.req.originalUrl.toString().indexOf('/api/HearingDates') > -1 && ctx.req.method.toString() == "PUT") {
    var hearingStatus = ctx.instance.status;
      var createCase = server.models.CreateCase;
      var judgement = server.models. Judgement;
      var fitstDate = new Date(ctx.instance.datePrevious);
    var firstDatee = fitstDate.getTime();
      var secondDate = new Date(ctx.instance.dateHearing);
    var secondDatee = secondDate.getTime();
      for (var j = 0; j < ctx.instance.advocateName.length; j++) {
           var exist = false;
        for(var k = 0; k < ctx.instance.advocateList.length; k++){
          if (ctx.instance.advocateList[k].email == ctx.instance.advocateName[j].email && firstDatee==secondDatee) {
            exist = true;
            break;
          }
        }
        if(exist == false){
          var Emailtemplete = server.models.EmailTemplete;
          var Advocate = server.models.Advocate;
          var Sms = server.models.Sms;
          Emailtemplete.find({"where": {"emailType": "legal"}}, function (err, legalemailtemplate) {
            var Dhanbademail = server.models.DhanbadEmail;
            for (var i = 0; i < ctx.instance.advocateName.length; i++) {
              Dhanbademail.create({
                "to": ctx.instance.advocateName[i].email,
                "subject": legalemailtemplate[0].hearingSubject,
                "text":  "Details : For Case Number : " + ctx.instance.caseNumber + " Hearing Date is :" + ctx.instance.dateHearing + legalemailtemplate[0].hearingText
              }, function (err, email) {

              });
              if(legalemailtemplate[0].hearingSMS) {
                Advocate.find({"where": {"email": ctx.instance.advocateName[0].email}}, function (err, advocateData) {
                  if (err) {

                  } else {
                    if (advocateData.length > 0) {
                      if (advocateData[0].phoneNumber) {
                        var smsData = {
                          "message": legalemailtemplate[0].hearingSMS + " Details : For Case Number : " + ctx.instance.caseNumber + " Hearing Date is :" + ctx.instance.dateHearing,
                          "mobileNo": advocateData[0].phoneNumber,
                          "smsservicetype": "singlemsg"
                        };

                        Sms.create(smsData, function (err, smsInfo) {
                        });
                      }
                    }
                  }
                });
              }
            }
            //next();
          });
          //next();
        }
      }
      if(ctx.instance.previousStatus!=ctx.instance.status){
       createCase.find({"where":{"caseNumber":ctx.instance.caseNumber}},function (err, CreateCase) {
          CreateCase[0].updateAttributes({status : hearingStatus},function(err,sucuss){
          })
          judgement.find({"where":{"caseNumber":ctx.instance.caseNumber},
            order:'judgementDate DESC'},function (err, Judgement) {
            if(Judgement.length>0) {
              Judgement[0].updateAttributes({status: hearingStatus}, function (err, sucuss) {
              })
            }
            next();
          });
        });
      }
      else{
        next();
      }
    }

    else if (ctx.req.originalUrl.toString().indexOf('/api/Judgements') > -1 && ctx.req.method.toString() == "PUT") {

      var judgeStatus = ctx.instance.status;
      var judgeCourt = ctx.instance.court;
      var createCase = server.models.CreateCase;
      var hearingDate = server.models.HearingDate
      createCase.find({"where":{"caseNumber":ctx.instance.caseNumber}},function (err, CreateCase) {
         CreateCase[0].updateAttributes({status : judgeStatus,courtName:judgeCourt},function(err,sucuss){
        })
        hearingDate.find({"where":{"caseNumber":ctx.instance.caseNumber},
          order:'dateHearing DESC'},function (err, HearingDate) {
    if(HearingDate.length>0) {
      HearingDate[0].updateAttributes({status: judgeStatus}, function (err, sucuss) {

      })
    }
          next();
        });

      });

    }
    else if (ctx.req.originalUrl.toString().indexOf('/api/CaseTypes') > -1 && ctx.req.method.toString() == "PUT") {
      var currentType = ctx.instance.caseType;
      if (ctx.instance.previousType != ctx.instance.caseType) {
        var createCase = server.models.CreateCase;
        createCase.find({"where": {"caseType": ctx.instance.previousType}}, function (err, CreateCase) {
          if (CreateCase.length > 0) {
            for(var i=0;i<CreateCase.length;i++) {
              CreateCase[i].updateAttributes({caseType: currentType}, function (err, sucuss) {
              })
            }
            next();
          }

        });
      }else{
        next();
      }
    }

    else if (ctx.req.originalUrl.toString().indexOf('/api/Courts') > -1 && ctx.req.method.toString() == "PUT") {
       var currentCourt = ctx.instance.name;
      if (ctx.instance.previousCourt != ctx.instance.name) {
        var createCase = server.models.CreateCase;
        var judgement = server.models.Judgement;

        createCase.find({"where": {"courtName": ctx.instance.previousCourt}}, function (err, CreateCase) {
          if (CreateCase.length > 0) {
            for (var i = 0; i < CreateCase.length; i++) {
              CreateCase[i].updateAttributes({courtName: currentCourt}, function (err, sucuss) {
              })
            }
          }
          judgement.find({"where": {"court": ctx.instance.previousCourt}}, function (err, Judgement) {
            if (Judgement.length > 0) {
              for (var j = 0; j < Judgement.length; j++) {
                Judgement[j].updateAttributes({court: currentCourt}, function (err, success) {

                })
              }
            }
            next();

          });
        });
      }else{
        next();
      }
    }

    else if (ctx.req.originalUrl.toString().indexOf('/api/Priorities') > -1 && ctx.req.method.toString() == "PUT") {

      var currentPriority = ctx.instance.priority;

      if (ctx.instance.previousPriority != ctx.instance.priority) {
        var createCase = server.models.CreateCase;
        createCase.find({"where": {"priority": ctx.instance.previousPriority}}, function (err, CreateCase) {
          if (CreateCase.length > 0) {
            for(var i=0;i<CreateCase.length;i++) {
              CreateCase[i].updateAttributes({priority: currentPriority}, function (err, sucuss) {
              })
            }
            next();
          }

        });
      }else{
        next();
      }
    }

    else if (ctx.req.originalUrl.toString().indexOf('/api/Statuses') > -1 && ctx.req.method.toString() == "PUT") {
       var currentStatus = ctx.instance.field1;
      if (ctx.instance.previousStatus != ctx.instance.field1) {
        var createCase = server.models.CreateCase;
        var judgement = server.models.Judgement;
        var hearingDate = server.models.HearingDate;
        createCase.find({"where": {"status": ctx.instance.previousStatus}}, function (err, CreateCase) {
          if (CreateCase.length > 0) {
            for (var i = 0; i < CreateCase.length; i++) {
              CreateCase[i].updateAttributes({status: currentStatus}, function (err, sucuss) {
              })
            }
          }
          judgement.find({"where": {"status": ctx.instance.previousStatus}}, function (err, Judgement) {
            if (Judgement.length > 0) {
              for (var j = 0; j < Judgement.length; j++) {
                Judgement[j].updateAttributes({status: currentStatus}, function (err, success) {

                })
              }
            }
            hearingDate.find({"where": {"status": ctx.instance.previousStatus}}, function (err, HearingDate) {
              if (HearingDate.length > 0) {
                for (var j = 0; j < HearingDate.length; j++) {
                  HearingDate[j].updateAttributes({status: currentStatus}, function (err, success) {

                  })
                }
              }
              next();
            });
          });
        });
      }else{
        next();
      }
    }

    else if (ctx.req.originalUrl.toString().indexOf('/api/caseDepartments') > -1 && ctx.req.method.toString() == "PUT") {
      var currentDepartment = ctx.instance.name;
      if (ctx.instance.previousDepartment != ctx.instance.name) {
        var createCase = server.models.CreateCase;
        createCase.find({"where": {"department": ctx.instance.previousDepartment}}, function (err, CreateCase) {
          if (CreateCase.length > 0) {
            for(var i=0;i<CreateCase.length;i++) {
              CreateCase[i].updateAttributes({department: currentDepartment}, function (err, sucuss) {
              })
            }
            next();
          }
        });
      }else{
        next();
      }
    }
    else if (ctx.req.originalUrl.toString().indexOf('/api/Acts') > -1 && ctx.req.method.toString() == "PUT") {
      var currentAct = ctx.instance.actNumber;
      if (ctx.instance.previousAct != ctx.instance.actNumber) {
        var createCase = server.models.CreateCase;
        createCase.find({"where": {"act": ctx.instance.previousAct}}, function (err, CreateCase) {
          if (CreateCase.length > 0) {
            for(var i=0;i<CreateCase.length;i++) {
              CreateCase[i].updateAttributes({act: currentAct}, function (err, sucuss) {
              })
            }
            next();
          }
        });
      }else{
        next();
      }
    }
    else if (ctx.req.originalUrl.toString().indexOf('/api/EmpannelGroups') > -1 && ctx.req.method.toString() == "PUT") {
      var currentGroup = ctx.instance.groupName;
      if (ctx.instance.previousGroup != ctx.instance.groupName) {
        var createCase = server.models.CreateCase;
        createCase.find({"where": {"empannel": ctx.instance.previousGroup}}, function (err, CreateCase) {
          if (CreateCase.length > 0) {
            for(var i=0;i<CreateCase.length;i++) {
              CreateCase[i].updateAttributes({empannel: currentGroup}, function (err, sucuss) {
              })
            }
            next();
          }
        });
      }else{
        next();
      }
    }

    else if (ctx.req.originalUrl.toString().indexOf('/api/StakeHolders') > -1 && ctx.req.method.toString() == "PUT") {
      var currentHolder = ctx.instance.name;
      var currentMail=ctx.instance.email;
      if (ctx.instance.previousHolder != ctx.instance.name || ctx.instance.previousMail !=ctx.instance.email ) {
        var caseStakeHolder = server.models.CaseStakeHolder;

        caseStakeHolder.find({"where": {"email": ctx.instance.previousMail}}, function (err, CaseStakeHolder) {
          if (CaseStakeHolder.length > 0) {
            for(var i=0;i<CaseStakeHolder.length;i++) {
              CaseStakeHolder[i].updateAttributes({name: currentHolder,email:currentMail}, function (err, sucuss) {
              })
            }
            next();
          }

        });
      }else{
        next();
      }
    }
    else {
      next();
    }

  });
  function addSchemeRole(ctx,next){
    ctx.roleObject['configuration']=true;
      ctx.roleObject['ministriesAccess']=true;
      ctx.roleObject['ministriesview'] = true;
      ctx.roleObject['ministriesedit'] = true;

      ctx.roleObject['beneficiariesAccess'] = true;
      ctx.roleObject['beneficiariesview'] = true;
      ctx.roleObject['beneficiariesedit'] = true;

      ctx.roleObject['schemeDepartmentAccess'] = true;
      ctx.roleObject['schemeDepartmentview'] = true;
      ctx.roleObject['schemeDepartmentedit'] = true;

      ctx.roleObject['emailAccess']=true;
      ctx.roleObject['emailview']=true;
      ctx.roleObject['emailedit']=true;

    ctx.roleObject['scheme']=true;
    ctx.roleObject['schemeManagementAccess']=true;
    ctx.roleObject['schemeManagementview']=true;
    ctx.roleObject['schemeManagementedit']=true;

    ctx.roleObject['schemeRequestsAccess']=true;
    ctx.roleObject['schemeRequestsview']=true;
    ctx.roleObject['schemeRequestsedit']=true;



  }
  function addProjectRole(ctx,next){

     ctx.roleObject['projectMaster']=true;
       ctx.roleObject['project']=true;
         ctx.roleObject['headOrSchemeAccess'] = true;
         ctx.roleObject['headOrSchemeview'] = true;
         ctx.roleObject['headOrSchemeedit'] = true;

          ctx.roleObject['ulbAccess'] = true;
          ctx.roleObject['ulbview'] = true;
          ctx.roleObject['ulbedit'] = true;

         ctx.roleObject['taskStatusAccess'] = true;
         ctx.roleObject['taskStatusview'] = true;
         ctx.roleObject['taskStatusedit'] = true;

         ctx.roleObject['planStateAccess'] = true;
         ctx.roleObject['planStateview'] = true;
         ctx.roleObject['planStateedit'] = true;

        ctx.roleObject['nocRequestsAccess'] = true;
         ctx.roleObject['nocRequestsview'] = true;
         ctx.roleObject['nocRequestsedit'] = true;

         ctx.roleObject['fieldReportsAccess'] = true;
         ctx.roleObject['fieldReportsview'] = true;
         ctx.roleObject['fieldReportsedit'] = true;

    ctx.roleObject['projectRequestsAccess'] = true;
    ctx.roleObject['projectRequestsview'] = true;
    ctx.roleObject['projectRequestsedit'] = true;

    ctx.roleObject['projectWardworksEmailTempleteAccess'] = true;
    ctx.roleObject['projectWardworksEmailTempleteview'] = true;
    ctx.roleObject['projectWardworksEmailTempleteedit'] = true;

    ctx.roleObject['documentTypeAccess'] = true;
    ctx.roleObject['documentTypeview'] = true;
    ctx.roleObject['documentTypeedit'] = true;


     ctx.roleObject['planDepartmentAccess'] = true;
    ctx.roleObject['planDepartmentview'] = true;
    ctx.roleObject['planDepartmentedit'] = true;


    ctx.roleObject['projectMgmt']=true;
      ctx.roleObject['projectClosureAccess']=true;
      ctx.roleObject['projectClosureview']=true;
      ctx.roleObject['projectClosureedit']=true;

      ctx.roleObject['projectRisksAccess']=true;
      ctx.roleObject['projectRisksview']=true;
      ctx.roleObject['projectRisksedit']=true;

      ctx.roleObject['projectDepartmentWiseBudgetAccess']=true;
      ctx.roleObject['projectDepartmentWiseBudgetview']=true;
      ctx.roleObject['projectDepartmentWiseBudgetedit']=true;

      ctx.roleObject['projectPaymentMilestonesAccess']=true;
      ctx.roleObject['projectPaymentMilestonesview']=true;
      ctx.roleObject['projectPaymentMilestonesedit']=true;

      ctx.roleObject['projectDeliverableAccess']=true;
      ctx.roleObject['projectDeliverableview']=true;
      ctx.roleObject['projectDeliverabledit']=true;

      ctx.roleObject['projectLayoutUploadAccess']=true;
      ctx.roleObject['projectLayoutUploadview']=true;
      ctx.roleObject['projectLayoutUploadedit']=true;

      ctx.roleObject['projectMilestonesAccess']=true;
      ctx.roleObject['pprojectMilestonesview']=true;
      ctx.roleObject['projectMilestonesedit']=true;

      ctx.roleObject['projectCommentsAccess']=true;
      ctx.roleObject['projectCommentsview']=true;
      ctx.roleObject['projectCommentsedit']=true;

      ctx.roleObject['projectUploadsAccess']=true;
      ctx.roleObject['projectUploadsview']=true;
      ctx.roleObject['projectUploadsedit']=true;


      ctx.roleObject['projectTransferFoundsAccess']=true;
      ctx.roleObject['projectTransferFoundsview']=true;
      ctx.roleObject['projectTransferFoundsedit']=true;

      ctx.roleObject['projectFieldVisitAccess']=true;
      ctx.roleObject['projectFieldVisitview']=true;
      ctx.roleObject['projectFieldVisitedit']=true;

      ctx.roleObject['projectBillGenerationAccess']=true;
      ctx.roleObject['projectBillGenerationview']=true;
      ctx.roleObject['projectBillGenerationdit']=true;

        ctx.roleObject['projectMeasurementAccessAccess']=true;
      ctx.roleObject['projectMeasurementAccessview']=true;
      ctx.roleObject['projectMeasurementAccessedit']=true;

        ctx.roleObject['projectPaymentAccessAccess']=true;
      ctx.roleObject['projectPaymentAccessview']=true;
      ctx.roleObject['projectPaymentAccessedit']=true;

      ctx.roleObject['projectTaskAccess']=true;
      ctx.roleObject['projectTaskview']=true;
      ctx.roleObject['projectTaskedit']=true;

      ctx.roleObject['projectTORAccess']=true;
      ctx.roleObject['projectTORview']=true;
      ctx.roleObject['projectTORedit']=true;

      ctx.roleObject['projectIssuesAccess']=true;
      ctx.roleObject['projectIssuesview']=true;
      ctx.roleObject['projectIssuesedit']=true;

      ctx.roleObject['projectWorkOrderAccess'] = true;
      ctx.roleObject['projectWorkOrderview'] = true;
      ctx.roleObject['projectWorkOrderedit'] = true;

      ctx.roleObject['projectRebaseLineAccess']=true;
      ctx.roleObject['projectRebaseLineview']=true;
      ctx.roleObject['projectRebaseLineedit']=true;

      ctx.roleObject['projectPlanAccess']=true;
      ctx.roleObject['projectPlanview']=true;
      ctx.roleObject['projectPlanedit']=true;

      ctx.roleObject['projectCharterAccess']=true;
      ctx.roleObject['projectCharterview']=true;
      ctx.roleObject['projectCharteredit']=true;

      ctx.roleObject['misReport'] = true;

      ctx.roleObject['weeklyReportAccess'] = true;
      ctx.roleObject['weeklyReportview'] = true;
      ctx.roleObject['weeklyReportedit'] = true;

      ctx.roleObject['projectPerformanceAccess'] = true;
      ctx.roleObject['projectPerformanceview'] = true;
      ctx.roleObject['projectPerformanceedit'] = true;

      ctx.roleObject['deptOverspentAccess'] = true;
      ctx.roleObject['deptOverspentview'] = true;
      ctx.roleObject['deptOverspentedit'] = true;

      ctx.roleObject['budgetUtilizationAccess'] = true;
      ctx.roleObject['budgetUtilizationview'] = true;
      ctx.roleObject['budgetUtilizationedit'] = true;

  }
  function addLegalRole(ctx,next){

    ctx.roleObject['legalManagement']=true;
    ctx.roleObject['legalMater']=true;
        ctx.roleObject['caseTypeAccess']=true;
        ctx.roleObject['caseTypeview']=true;
        ctx.roleObject['caseTypeedit']=true;

        ctx.roleObject['holidaysAccess']=true;
        ctx.roleObject['holidaysview']=true;
        ctx.roleObject['holidaysedit']=true;

        ctx.roleObject['empannelGroupAccess']=true;
        ctx.roleObject['empannelGroupview']=true;
        ctx.roleObject['empannelGroupedit']=true;

        ctx.roleObject['advocateListsAccess']=true;
        ctx.roleObject['advocateListsview']=true;
        ctx.roleObject['advocateListsedit']=true;

        ctx.roleObject['statusAccess']=true;
        ctx.roleObject['statusview']=true;
        ctx.roleObject['statusedit']=true;

        ctx.roleObject['priorityAccess']=true;
        ctx.roleObject['priorityview']=true;
        ctx.roleObject['priorityedit']=true;

        ctx.roleObject['actsAccess']=true;
        ctx.roleObject['actsview']=true;
        ctx.roleObject['actsedit']=true;

        ctx.roleObject['stakeHoldersAccess']=true;
        ctx.roleObject['stakeHoldersview']=true;
        ctx.roleObject['stakeHoldersedit']=true;

        ctx.roleObject['courtInfoAccess']=true;
        ctx.roleObject['courtInfoview']=true;
        ctx.roleObject['courtInfoedit']=true;

        ctx.roleObject['caseDepartmentsAccess']=true;
        ctx.roleObject['caseDepartmentsview']=true;
        ctx.roleObject['caseDepartmentsedit']=true;

        ctx.roleObject['paymentModeAccess']=true;
        ctx.roleObject['paymentModeview']=true;
        ctx.roleObject['paymentModesedit']=true;

        ctx.roleObject['legalEmailTemplateAccess']=true;
        ctx.roleObject['legalEmailTemplateview']=true;
        ctx.roleObject['legalEmailTemplateedit']=true;

        ctx.roleObject['legalCase']=true;
        ctx.roleObject['createCaseAccess']=true;
        ctx.roleObject['createCaseview']=true;
        ctx.roleObject['createCaseedit']=true;

        ctx.roleObject['advocatePaymentsAccess']=true;
        ctx.roleObject['advocatePaymentsview']=true;
        ctx.roleObject['advocatePaymentsedit']=true;

        ctx.roleObject['hearingDateInfoAccess']=true;
        ctx.roleObject['hearingDateInfoview']=true;
        ctx.roleObject['hearingDateInfoedit']=true;

        ctx.roleObject['noticeCommentsAccess']=true;
        ctx.roleObject['noticeCommentsview']=true;
        ctx.roleObject['noticeCommentsedit']=true;

        ctx.roleObject['caseStakeHolderAccess']=true;
        ctx.roleObject['caseStakeHolderview']=true;
        ctx.roleObject['caseStakeHolderedit']=true;

        ctx.roleObject['docUploadAccess']=true;

        ctx.roleObject['docUploadview']=true;
        ctx.roleObject['docUploadedit']=true;

        ctx.roleObject['writePetitionAccess']=true;
        ctx.roleObject['writePetitionview']=true;
        ctx.roleObject['writePetitionedit']=true;

        ctx.roleObject['judgementAccess']=true;
        ctx.roleObject['judgementview']=true;
        ctx.roleObject['judgementedit']=true;

        ctx.roleObject['caseReportsAccess']=true;
        ctx.roleObject['caseReportsview']=true;
        ctx.roleObject['caseReportsedit']=true;

  }
  function addLandRole(ctx,next){
    ctx.roleObject['landAndAsset']=true;
    ctx.roleObject['landAndAssetManagement']=true;
    ctx.roleObject['masterData']=true;
        ctx.roleObject['assetTypeAccess']=true;
        ctx.roleObject['assetTypeview']=true;
        ctx.roleObject['assetTypeedit']=true;

        ctx.roleObject['assetGroupAccess']=true;
        ctx.roleObject['assetGroupview']=true;
        ctx.roleObject['assetGroupedit']=true;

        ctx.roleObject['assetProfileAccess']=true;
        ctx.roleObject['assetProfileview']=true;
        ctx.roleObject['assetProfileedit']=true;

        ctx.roleObject['assetCriticalityAccess']=true;
        ctx.roleObject['assetCriticalityview']=true;
        ctx.roleObject['assetCriticalityedit']=true;

        ctx.roleObject['maintenanceObjectivesAccess']=true;
        ctx.roleObject['maintenanceObjectivesview']=true;
        ctx.roleObject['maintenanceObjectivesedit']=true;

        ctx.roleObject['typeOfDutyAccess']=true;
        ctx.roleObject['typeOfDutyview']=true;
        ctx.roleObject['typeOfDutyedit']=true;

        ctx.roleObject['accountDetailsAccess']=true;
        ctx.roleObject['accountDetailsview']=true;
        ctx.roleObject['accountDetailsedit']=true;

        ctx.roleObject['failureClassesAccess']=true;
        ctx.roleObject['failureClassesview']=true;
        ctx.roleObject['failureClassesedit']=true;

        ctx.roleObject['assetSparesAndConsumablesAccess']=true;
        ctx.roleObject['assetSparesAndConsumablesview']=true;
        ctx.roleObject['assetSparesAndConsumablesedit']=true;

        ctx.roleObject['documentListMasterAccess']=true;
        ctx.roleObject['documentListMasterview']=true;
        ctx.roleObject['documentListMasteredit']=true;

        ctx.roleObject['assetStatusAccess']=true;
        ctx.roleObject['assetStatusview']=true;
        ctx.roleObject['assetStatusedit']=true;

        ctx.roleObject['assetKPIsAccess']=true;
        ctx.roleObject['assetKPIsview']=true;
        ctx.roleObject['assetKPIsedit']=true;

        ctx.roleObject['assetServiceLevelAccess']=true;
        ctx.roleObject['assetServiceLevelview']=true;
        ctx.roleObject['assetServiceLeveledit']=true;

        ctx.roleObject['assetDisposalMethodsAccess']=true;
        ctx.roleObject['assetDisposalMethodsview']=true;
        ctx.roleObject['assetDisposalMethodsedit']=true;

        ctx.roleObject['assetRisksAccess']=true;
        ctx.roleObject['assetRisksview']=true;
        ctx.roleObject['assetRisksedit']=true;

        ctx.roleObject['costCenterAccess']=true;
        ctx.roleObject['costCenterview']=true;
        ctx.roleObject['costCenteredit']=true;

        ctx.roleObject['assetPoliciesAccess']=true;
        ctx.roleObject['assetPoliciesview']=true;
        ctx.roleObject['assetPoliciesedit']=true;

        ctx.roleObject['assetLifeCyclePlanAccess']=true;
        ctx.roleObject['assetLifeCyclePlanview']=true;
        ctx.roleObject['assetLifeCyclePlanedit']=true;

        ctx.roleObject['assetDepartmentAccess']=true;
        ctx.roleObject['assetDepartmentview']=true;
        ctx.roleObject['assetDepartmentedit']=true;

        ctx.roleObject['assetEmailConfigurationAccess']=true;
        ctx.roleObject['assetEmailConfigurationview']=true;
        ctx.roleObject['assetEmailConfigurationedit']=true;

    ctx.roleObject['createAssetLandAccess']=true;
    ctx.roleObject['createAssetLandview']=true;
    ctx.roleObject['createAssetLandedit']=true;

    ctx.roleObject['createAssetOthersAccess'] = true;
    ctx.roleObject['createAssetOthersview'] = true;
    ctx.roleObject['createAssetOthersedit'] = true;

    ctx.roleObject['costTransferFundsAccess']=true;
    ctx.roleObject['costTransferFundsview']=true;
    ctx.roleObject['costTransferFundsedit']=true;

    ctx.roleObject['createAssetObjectivesAndKPIsAccess']=true;
    ctx.roleObject['createAssetObjectivesAndKPIsview']=true;
    ctx.roleObject['createAssetObjectivesAndKPIsedit']=true;

    ctx.roleObject['createAssetSparesAndConsumablesAccess']=true;
    ctx.roleObject['createAssetSparesAndConsumablesview']=true;
    ctx.roleObject['createAssetSparesAndConsumablesedit']=true;

    ctx.roleObject['createAssetFailureAndMaintenanceAccess']=true;
    ctx.roleObject['createAssetFailureAndMaintenanceview']=true;
    ctx.roleObject['createAssetFailureAndMaintenanceedit']=true;

    ctx.roleObject['createAssetRisksAccess']=true;
    ctx.roleObject['createAssetRisksview']=true;
    ctx.roleObject['createAssetRisksedit']=true;

    ctx.roleObject['createAssetCausesAccess']=true;
    ctx.roleObject['createAssetCausesview']=true;
    ctx.roleObject['createAssetCausesedit']=true;

    ctx.roleObject['createAssetServiceLevelForecastAccess']=true;
    ctx.roleObject['createAssetServiceLevelForecastview']=true;
    ctx.roleObject['createAssetServiceLevelForecastedit']=true;

    ctx.roleObject['assetDemandProjectionAccess']=true;
    ctx.roleObject['assetDemandProjectionview']=true;
    ctx.roleObject['assetDemandProjectionedit']=true;

    ctx.roleObject['vehicleManagement']=true;
    ctx.roleObject['purchaseRequestsAccess']=true;
    ctx.roleObject['requestsview']=true;
    ctx.roleObject['requestsedit']=true;

    ctx.roleObject['purchaseRequestsReportAccess']=true;
    ctx.roleObject['purchaseRequestsReportview']=true;
    ctx.roleObject['purchaseRequestsReportedit']=true;

    ctx.roleObject['purchaseAgendaAccess']=true;
    ctx.roleObject['purchaseAgendaview']=true;
    ctx.roleObject['purchaseAgendaedit']=true;

    ctx.roleObject['assignDriverAccess']=true;
    ctx.roleObject['assignDriverview']=true;
    ctx.roleObject['assignDriveredit']=true;

    ctx.roleObject['logBookAccess']=true;
    ctx.roleObject['logBookview']=true;
    ctx.roleObject['logBookedit']=true;

    ctx.roleObject['submitBillsAccess']=true;
    ctx.roleObject['submitBillsview']=true;
    ctx.roleObject['submitBillsedit']=true;
  }
  function addWorkflowRole(ctx,next){

    ctx.roleObject['userAdmin']=true;

    ctx.roleObject['appConfigAccess']=true;
    ctx.roleObject['appConfigview']=true;
    ctx.roleObject['appConfigedit']=true;

    ctx.roleObject['workflowFormsAccess']=true;
    ctx.roleObject['workflowFormsview']=true;
    ctx.roleObject['workflowFormsedit']=true;

    ctx.roleObject['workflowEmployeeAccess']=true;
    ctx.roleObject['workflowEmployeeview']=true;
    ctx.roleObject['workflowEmployeeedit']=true;

    ctx.roleObject['contractorsListAccess']=true;
    ctx.roleObject['contractorsListview']=true;
    ctx.roleObject['contractorsListedit']=true;


    ctx.roleObject['workflowLevelsAccess']=true;
    ctx.roleObject['workflowLevelsview']=true;
    ctx.roleObject['workflowLevelsedit']=true;

    ctx.roleObject['workflowAccess']=true;
    ctx.roleObject['workflowview']=true;
    ctx.roleObject['workflowedit']=true;

    ctx.roleObject['roleAccess']=true;
    ctx.roleObject['roleview']=true;
    ctx.roleObject['roleedit']=true;

  ctx.roleObject['languageDataAccess']=true;
    ctx.roleObject['languageDataview']=true;
    ctx.roleObject['languageDataedit']=true;


    ctx.roleObject['employeeTypeAccess']=true;
    ctx.roleObject['employeeTypeview']=true;
    ctx.roleObject['employeeTypeedit']=true;

    ctx.roleObject['employeeDepartmentAccess']=true;
    ctx.roleObject['employeeDepartmentview']=true;
    ctx.roleObject['employeeDepartmentedit']=true;

    ctx.roleObject['employeeDesignationAccess']=true;
    ctx.roleObject['employeeDesignationview']=true;
    ctx.roleObject['employeeDesignationedit']=true;

    ctx.roleObject['employeeAccess']=true;
    ctx.roleObject['employeeview']=true;
    ctx.roleObject['employeeedit']=true;

    next();
  }

  function checkAccessDetailsDetails(ctx, roleForms,next){
    if(roleForms.length>0){

      for(var i=0;i<roleForms.length;i++){
        if(roleForms[i].formDetails!=undefined && roleForms[i].formDetails!=null && roleForms[i].formDetails.length>0){
          for(var j=0;j<roleForms[i].formDetails.length;j++) {
            var formDetails = roleForms[i].formDetails[j];
            if (formDetails.value != undefined && formDetails.value == "schemeManagement") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['scheme'] = true;
                ctx.roleObject['schemeManagementAccess'] = true;
                ctx.roleObject['schemeManagementview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['scheme'] = true;
                ctx.roleObject['schemeManagementAccess'] = true;
                ctx.roleObject['schemeManagementview'] = true;
                ctx.roleObject['schemeManagementedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "schemeRequests") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['scheme'] = true;
                ctx.roleObject['schemeRequestsAccess'] = true;
                ctx.roleObject['schemeRequestsview'] = true;
              }

              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['scheme'] = true;
                ctx.roleObject['schemeRequestsAccess'] = true;
                ctx.roleObject['schemeRequestsview'] = true;
                ctx.roleObject['schemeRequestsedit'] = true;
              }
            }
            // for Scheme Master Details
            else if (formDetails.value != undefined && formDetails.value == "ministries") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['configuration'] = true;
                ctx.roleObject['ministriesAccess'] = true;
                ctx.roleObject['ministriesview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['configuration'] = true;
                ctx.roleObject['ministriesAccess'] = true;
                ctx.roleObject['ministriesview'] = true;
                ctx.roleObject['ministriesedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "schemeDepartment") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['configuration'] = true;
                ctx.roleObject['schemeDepartmentAccess'] = true;
                ctx.roleObject['schemeDepartmentview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['configuration'] = true;
                ctx.roleObject['schemeDepartmentAccess'] = true;
                ctx.roleObject['schemeDepartmentview'] = true;
                ctx.roleObject['schemeDepartmentedit'] = true
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "beneficiaries") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['configuration'] = true;
                ctx.roleObject['beneficiariesAccess'] = true;
                ctx.roleObject['beneficiariesview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['configuration'] = true;
                ctx.roleObject['beneficiariesAccess'] = true;
                ctx.roleObject['beneficiariesview'] = true;
                ctx.roleObject['beneficiariesedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "emailAccess") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['configuration'] = true;
                ctx.roleObject['emailAccess'] = true;
                ctx.roleObject['emailview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['configuration'] = true;
                ctx.roleObject['emailAccess'] = true;
                ctx.roleObject['emailview'] = true;
                ctx.roleObject['emailedit'] = true;

              }
            }
            // For Project ward works Master Details
            else if (formDetails.value != undefined && formDetails.value == "headOrScheme") {
              if (formDetails.view != undefined && formDetails.view) {

                ctx.roleObject['projectMaster'] = true;
                console.log('details for all are showing' + ctx.roleObject.projectMaster);
                ctx.roleObject['project'] = true;
                ctx.roleObject['headOrSchemeAccess'] = true;
                ctx.roleObject['headOrSchemeview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['headOrSchemeAccess'] = true;
                ctx.roleObject['headOrSchemeview'] = true;
                ctx.roleObject['headOrSchemeedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "ulb") {
              if (formDetails.view != undefined && formDetails.view) {

                ctx.roleObject['projectMaster'] = true;
                console.log('details for all are showing' + ctx.roleObject.projectMaster);
                ctx.roleObject['project'] = true;
                ctx.roleObject['ulbAccess'] = true;
                ctx.roleObject['ulbview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['ulbAccess'] = true;
                ctx.roleObject['ulbview'] = true;
                ctx.roleObject['ulbedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "taskStatus") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['taskStatusAccess'] = true;
                ctx.roleObject['taskStatusview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['taskStatusAccess'] = true;
                ctx.roleObject['taskStatusview'] = true;
                ctx.roleObject['taskStatusedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "planState") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['planStateAccess'] = true;
                ctx.roleObject['planStateview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['planStateAccess'] = true;
                ctx.roleObject['planStateview'] = true;
                ctx.roleObject['planStateedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "projectWardworksEmailTemplete") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectWardworksEmailTempleteAccess'] = true;
                ctx.roleObject['projectWardworksEmailTempleteview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectWardworksEmailTempleteAccess'] = true;
                ctx.roleObject['projectWardworksEmailTempleteview'] = true;
                ctx.roleObject['projectWardworksEmailTempleteedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "documentType") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['documentTypeAccess'] = true;
                ctx.roleObject['documentTypeview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['documentTypeAccess'] = true;
                ctx.roleObject['documentTypeview'] = true;
                ctx.roleObject['documentTypeedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "planDepartment") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['planDepartmentAccess'] = true;
                ctx.roleObject['planDepartmentview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['planDepartmentAccess'] = true;
                ctx.roleObject['planDepartmentview'] = true;
                ctx.roleObject['planDepartmentedit'] = true;
              }
            }

            // Email and Document List is not displayed


            // For Plan Details
            else if (formDetails.value != undefined && formDetails.value == "projectCharter") {
              if (formDetails.view != undefined && formDetails.view) {

                ctx.roleObject['project'] = true;
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['projectCharterAccess'] = true;
                ctx.roleObject['projectCharterview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectCharterAccess'] = true;
                ctx.roleObject['projectCharterview'] = true;
                ctx.roleObject['projectCharteredit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "nocRequests") {
              if (formDetails.view != undefined && formDetails.view) {

                ctx.roleObject['project'] = true;
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['nocRequestsAccess'] = true;
                ctx.roleObject['nocRequestsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['nocRequestsAccess'] = true;
                ctx.roleObject['nocRequestsview'] = true;
                ctx.roleObject['nocRequestsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "fieldReports") {
              if (formDetails.view != undefined && formDetails.view) {

                ctx.roleObject['project'] = true;
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['fieldReportsAccess'] = true;
                ctx.roleObject['fieldReportsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['fieldReportsAccess'] = true;
                ctx.roleObject['fieldReportsview'] = true;
                ctx.roleObject['fieldReportsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectRequests") {
              if (formDetails.view != undefined && formDetails.view) {

                ctx.roleObject['project'] = true;
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['projectRequestsAccess'] = true;
                ctx.roleObject['projectRequestsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectRequestsAccess'] = true;
                ctx.roleObject['projectRequestsview'] = true;
                ctx.roleObject['projectRequestsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectPlan") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectPlanAccess'] = true;
                ctx.roleObject['projectPlanview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectPlanAccess'] = true;
                ctx.roleObject['projectPlanview'] = true;
                ctx.roleObject['projectPlanedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectTask") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectTaskAccess'] = true;
                ctx.roleObject['projectTaskview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectTaskAccess'] = true;
                ctx.roleObject['projectTaskview'] = true;
                ctx.roleObject['projectTaskedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectMeasurementBook") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectMeasurementAccessAccess'] = true;
                ctx.roleObject['projectMeasurementAccessview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectMeasurementAccessAccess'] = true;
                ctx.roleObject['projectMeasurementAccessview'] = true;
                ctx.roleObject['projectMeasurementAccessedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectPaymentOrder") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectPaymentAccessAccess'] = true;
                ctx.roleObject['projectPaymentAccessview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectPaymentAccessAccess'] = true;
                ctx.roleObject['projectPaymentAccessview'] = true;
                ctx.roleObject['projectPaymentAccessedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectTOR") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectTORAccess'] = true;
                ctx.roleObject['projectTORview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectTORAccess'] = true;
                ctx.roleObject['projectTORview'] = true;
                ctx.roleObject['projectTORedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectIssues") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectIssuesAccess'] = true;
                ctx.roleObject['projectIssuesview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectIssuesAccess'] = true;
                ctx.roleObject['projectIssuesview'] = true;
                ctx.roleObject['projectIssuesedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectRebaseLine") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectRebaseLineAccess'] = true;
                ctx.roleObject['projectRebaseLinesview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectRebaseLineAccess'] = true;
                ctx.roleObject['projectRebaseLineview'] = true;
                ctx.roleObject['projectRebaseLineedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectLayoutUpload") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectLayoutUploadAccess'] = true;
                ctx.roleObject['projectLayoutUploadview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectLayoutUploadAccess'] = true;
                ctx.roleObject['projectLayoutUploadview'] = true;
                ctx.roleObject['projectLayoutUploadedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectBillGeneration") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectBillGenerationAccess'] = true;
                ctx.roleObject['projectBillGenerationview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectBillGenerationAccess'] = true;
                ctx.roleObject['projectBillGenerationview'] = true;
                ctx.roleObject['projectBillGenerationedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectFieldVisit") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectFieldVisitAccess'] = true;
                ctx.roleObject['projectFieldVisitview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectFieldVisitAccess'] = true;
                ctx.roleObject['projectFieldVisitview'] = true;
                ctx.roleObject['projectFieldVisitedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectTransferFounds") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectTransferFoundsAccess'] = true;
                ctx.roleObject['projectTransferFoundsview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectTransferFoundsAccess'] = true;
                ctx.roleObject['projectTransferFoundsview'] = true;
                ctx.roleObject['projectTransferFoundsedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectUploads") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectUploadsAccess'] = true;
                ctx.roleObject['projectUploadsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectUploadsAccess'] = true;
                ctx.roleObject['projectUploadsview'] = true;
                ctx.roleObject['projectUploadsedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectWorkOrder") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectWorkOrderAccess'] = true;
                ctx.roleObject['projectWorkOrderview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectWorkOrderAccess'] = true;
                ctx.roleObject['projectWorkOrderview'] = true;
                ctx.roleObject['projectWorkOrderedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectComments") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectCommentsAccess'] = true;
                ctx.roleObject['projectCommentsview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectCommentsAccess'] = true;
                ctx.roleObject['projectCommentsview'] = true;
                ctx.roleObject['projectCommentsedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectDeliverable") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectDeliverableAccess'] = true;
                ctx.roleObject['projectDeliverableview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectDeliverableAccess'] = true;
                ctx.roleObject['projectDeliverableview'] = true;
                ctx.roleObject['projectDeliverabledit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectPaymentMilestones") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectPaymentMilestonesAccess'] = true;
                ctx.roleObject['projectPaymentMilestonesview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectPaymentMilestonesAccess'] = true;
                ctx.roleObject['projectPaymentMilestonesview'] = true;
                ctx.roleObject['projectPaymentMilestonesedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectMilestones") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectMilestonesAccess'] = true;
                ctx.roleObject['pprojectMilestonesview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectMilestonesAccess'] = true;
                ctx.roleObject['pprojectMilestonesview'] = true;
                ctx.roleObject['projectMilestonesedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectDepartmentWiseBudget") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectDepartmentWiseBudgetAccess'] = true;
                ctx.roleObject['projectDepartmentWiseBudgetview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectDepartmentWiseBudgetAccess'] = true;
                ctx.roleObject['projectDepartmentWiseBudgetview'] = true;
                ctx.roleObject['projectDepartmentWiseBudgetedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectRisks") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectRisksAccess'] = true;
                ctx.roleObject['projectRisksview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectRisksAccess'] = true;
                ctx.roleObject['projectRisksview'] = true;
                ctx.roleObject['projectRisksedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectClosure") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectClosureAccess'] = true;
                ctx.roleObject['projectClosureview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectClosureAccess'] = true;
                ctx.roleObject['projectClosureview'] = true;
                ctx.roleObject['projectClosureedit'] = true;
              }
            }
            // MIS report
            else if (formDetails.value != undefined && formDetails.value == "budgetUtilization") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['misReport'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['budgetUtilizationAccess'] = true;
                ctx.roleObject['budgetUtilizationview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['misReport'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['budgetUtilizationAccess'] = true;
                ctx.roleObject['budgetUtilizationview'] = true;
                ctx.roleObject['budgetUtilizationedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "deptOverspent") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['misReport'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['deptOverspentAccess'] = true;
                ctx.roleObject['deptOverspentview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['misReport'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['deptOverspentAccess'] = true;
                ctx.roleObject['deptOverspentview'] = true;
                ctx.roleObject['deptOverspentedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectPerformance") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['misReport'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectPerformanceAccess'] = true;
                ctx.roleObject['projectPerformanceview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['misReport'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectPerformanceAccess'] = true;
                ctx.roleObject['projectPerformanceview'] = true;
                ctx.roleObject['projectPerformanceedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "weeklyReport") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['misReport'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['weeklyReportAccess'] = true;
                ctx.roleObject['weeklyReportview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['misReport'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['weeklyReportAccess'] = true;
                ctx.roleObject['weeklyReportview'] = true;
                ctx.roleObject['weeklyReportedit'] = true;
              }
            }

            // For legal mgmt Master
            else if (formDetails.value != undefined && formDetails.value == "holidays") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['holidaysAccess'] = true;
                ctx.roleObject['holidaysview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['holidaysAccess'] = true;
                ctx.roleObject['holidaysview'] = true;
                ctx.roleObject['holidaysedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "empannelGroup") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['empannelGroupAccess'] = true;
                ctx.roleObject['empannelGroupview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['empannelGroupAccess'] = true;
                ctx.roleObject['empannelGroupview'] = true;
                ctx.roleObject['empannelGroupedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "advocateLists") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['advocateListsAccess'] = true;
                ctx.roleObject['advocateListsview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['advocateListsAccess'] = true;
                ctx.roleObject['advocateListsview'] = true;
                ctx.roleObject['advocateListsedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "status") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['statusAccess'] = true;
                ctx.roleObject['statusview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['statusAccess'] = true;
                ctx.roleObject['statusview'] = true;
                ctx.roleObject['statusedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "priority") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['priorityAccess'] = true;
                ctx.roleObject['priorityview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['priorityAccess'] = true;
                ctx.roleObject['priorityview'] = true;
                ctx.roleObject['priorityedit'] = true;

              }
            }

            else if (formDetails.value != undefined && formDetails.value == "acts") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['actsAccess'] = true;
                ctx.roleObject['actsview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['actsAccess'] = true;
                ctx.roleObject['actsview'] = true;
                ctx.roleObject['actsedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "stakeHolders") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['stakeHoldersAccess'] = true;
                ctx.roleObject['stakeHoldersview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['stakeHoldersAccess'] = true;
                ctx.roleObject['stakeHoldersview'] = true;
                ctx.roleObject['stakeHoldersedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "courtInfo") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['courtInfoAccess'] = true;
                ctx.roleObject['courtInfoview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['courtInfoAccess'] = true;
                ctx.roleObject['courtInfoview'] = true;
                ctx.roleObject['courtInfoedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "caseDepartments") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;

                ctx.roleObject['caseDepartmentsAccess'] = true;
                ctx.roleObject['caseDepartmentsview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;

                ctx.roleObject['caseDepartmentsAccess'] = true;
                ctx.roleObject['caseDepartmentsview'] = true;
                ctx.roleObject['caseDepartmentsedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "paymentMode") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['paymentModeAccess'] = true;
                ctx.roleObject['paymentModeview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['paymentModeAccess'] = true;
                ctx.roleObject['paymentModeview'] = true;
                ctx.roleObject['paymentModesedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "legalEmailTemplate") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['legalEmailTemplateAccess'] = true;
                ctx.roleObject['legalEmailTemplateview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['legalEmailTemplateAccess'] = true;
                ctx.roleObject['legalEmailTemplateview'] = true;
                ctx.roleObject['legalEmailTemplateedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "caseType") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['caseTypeAccess'] = true;
                ctx.roleObject['caseTypeview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['caseTypeAccess'] = true;
                ctx.roleObject['caseTypeview'] = true;
                ctx.roleObject['caseTypeedit'] = true;
              }
            }

            // For Case details
            else if (formDetails.value != undefined && formDetails.value == "createCase") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['createCaseAccess'] = true;
                ctx.roleObject['createCaseview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['createCaseAccess'] = true;
                ctx.roleObject['createCaseview'] = true;
                ctx.roleObject['createCaseedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "advocatePayments") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['advocatePaymentsAccess'] = true;
                ctx.roleObject['advocatePaymentsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['advocatePaymentsAccess'] = true;
                ctx.roleObject['advocatePaymentsview'] = true;
                ctx.roleObject['advocatePaymentsedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "hearingDateInfo") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['hearingDateInfoAccess'] = true;
                ctx.roleObject['hearingDateInfoview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['hearingDateInfoAccess'] = true;
                ctx.roleObject['hearingDateInfoview'] = true;
                ctx.roleObject['hearingDateInfoedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "noticeComments") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['noticeCommentsAccess'] = true;
                ctx.roleObject['noticeCommentsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['noticeCommentsAccess'] = true;
                ctx.roleObject['noticeCommentsview'] = true;
                ctx.roleObject['noticeCommentsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "docUpload") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['docUploadAccess'] = true;
                ctx.roleObject['docUploadview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['docUploadAccess'] = true;
                ctx.roleObject['docUploadview'] = true;
                ctx.roleObject['docUploadedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "writePetition") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['writePetitionAccess'] = true;
                ctx.roleObject['writePetitionview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['writePetitionAccess'] = true;
                ctx.roleObject['writePetitionview'] = true;
                ctx.roleObject['writePetitionedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "judgement") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['judgementAccess'] = true;
                ctx.roleObject['judgementview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['judgementAccess'] = true;
                ctx.roleObject['judgementview'] = true;
                ctx.roleObject['judgementedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "caseReports") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['caseReportsAccess'] = true;
                ctx.roleObject['caseReportsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['caseReportsAccess'] = true;
                ctx.roleObject['caseReportsview'] = true;
                ctx.roleObject['caseReportsedit'] = true;

              }
            }


            // For User Administartion
            else if (formDetails.value != undefined && formDetails.value == "workflowForms") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['workflowFormsAccess'] = true;
                ctx.roleObject['workflowFormsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['workflowFormsAccess'] = true;
                ctx.roleObject['workflowFormsview'] = true;
                ctx.roleObject['workflowFormsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "workflowEmployee") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['workflowEmployeeAccess'] = true;
                ctx.roleObject['workflowEmployeeview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['workflowEmployeeAccess'] = true;
                ctx.roleObject['workflowEmployeeview'] = true;
                ctx.roleObject['workflowEmployeeedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "workflowLevels") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['workflowLevelsAccess'] = true;
                ctx.roleObject['workflowLevelsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['workflowLevelsAccess'] = true;
                ctx.roleObject['workflowLevelsview'] = true;
                ctx.roleObject['workflowLevelsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "workflow") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['workflowAccess'] = true;
                ctx.roleObject['workflowview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['workflowAccess'] = true;
                ctx.roleObject['workflowview'] = true;
                ctx.roleObject['workflowedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "contractorsList") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['contractorsListAccess'] = true;
                ctx.roleObject['contractorsListview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['contractorsListAccess'] = true;
                ctx.roleObject['contractorsListview'] = true;
                ctx.roleObject['contractorsListedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "role") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['roleAccess'] = true;
                ctx.roleObject['roleview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['roleAccess'] = true;
                ctx.roleObject['roleview'] = true;
                ctx.roleObject['roleedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "employeeType") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['employeeTypeAccess'] = true;
                ctx.roleObject['employeeTypeview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['employeeTypeAccess'] = true;
                ctx.roleObject['employeeTypeview'] = true;
                ctx.roleObject['employeeTypeedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "employeeDepartment") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['employeeDepartmentAccess'] = true;
                ctx.roleObject['employeeDepartmentview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['employeeDepartmentAccess'] = true;
                ctx.roleObject['employeeDepartmentview'] = true;
                ctx.roleObject['employeeDepartmentedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "employeeDesignation") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['employeeDesignationAccess'] = true;
                ctx.roleObject['employeeDesignationview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['employeeDesignationAccess'] = true;
                ctx.roleObject['employeeDesignationview'] = true;
                ctx.roleObject['employeeDesignationedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "appConfig") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['appConfigAccess'] = true;
                ctx.roleObject['appConfigview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['appConfigAccess'] = true;
                ctx.roleObject['appConfigview'] = true;
                ctx.roleObject['appConfigedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "languageData") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['languageDataAccess'] = true;
                ctx.roleObject['languageDataview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['languageDataAccess'] = true;
                ctx.roleObject['languageDataview'] = true;
                ctx.roleObject['languageDataedit'] = true;

              }
            }

            else if (formDetails.value != undefined && formDetails.value == "employee") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['employeeAccess'] = true;
                ctx.roleObject['employeeview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['employeeAccess'] = true;
                ctx.roleObject['employeeview'] = true;
                ctx.roleObject['employeeedit'] = true;
              }
            }

            // Land And Asset Management
            else if (formDetails.value != undefined && formDetails.value == "submitBills") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['submitBillsAccess'] = true;
                ctx.roleObject['submitBillsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['submitBillsAccess'] = true;
                ctx.roleObject['submitBillsview'] = true;
                ctx.roleObject['submitBillsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "logBook") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['logBookAccess'] = true;
                ctx.roleObject['logBookview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['logBookAccess'] = true;
                ctx.roleObject['logBookview'] = true;
                ctx.roleObject['logBookedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assignDriver") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['assignDriverAccess'] = true;
                ctx.roleObject['assignDriverview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['assignDriverAccess'] = true;
                ctx.roleObject['assignDriverview'] = true;
                ctx.roleObject['assignDriveredit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "purchaseAgenda") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['purchaseAgendaAccess'] = true;
                ctx.roleObject['purchaseAgendaview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['purchaseAgendaAccess'] = true;
                ctx.roleObject['purchaseAgendaview'] = true;
                ctx.roleObject['purchaseAgendaedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "purchaseRequestsReport") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['purchaseRequestsReportAccess'] = true;
                ctx.roleObject['purchaseRequestsReportview'] = true;


              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['purchaseRequestsReportAccess'] = true;
                ctx.roleObject['purchaseRequestsReportview'] = true;
                ctx.roleObject['purchaseRequestsReportedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "purchaseRequests") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['purchaseRequestsAccess'] = true;
                ctx.roleObject['requestsview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['purchaseRequestsAccess'] = true;
                ctx.roleObject['requestsview'] = true;
                ctx.roleObject['requestsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetDemandProjection") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetDemandProjectionAccess'] = true;
                ctx.roleObject['assetDemandProjectionview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetDemandProjectionAccess'] = true;
                ctx.roleObject['assetDemandProjectionview'] = true;
                ctx.roleObject['assetDemandProjectionedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "createAssetRisks") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetRisksAccess'] = true;
                ctx.roleObject['createAssetRisksview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetRisksAccess'] = true;
                ctx.roleObject['createAssetRisksview'] = true;
                ctx.roleObject['createAssetRisksedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "createAssetCauses") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetCausesAccess'] = true;
                ctx.roleObject['createAssetCausesview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetCausesAccess'] = true;
                ctx.roleObject['createAssetCausesview'] = true;
                ctx.roleObject['createAssetCausesedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "createAssetServiceLevelForecast") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetServiceLevelForecastAccess'] = true;
                ctx.roleObject['createAssetServiceLevelForecastview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetServiceLevelForecastAccess'] = true;
                ctx.roleObject['createAssetServiceLevelForecastview'] = true;
                ctx.roleObject['createAssetServiceLevelForecastedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "createAssetFailureAndMaintenance") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetFailureAndMaintenanceAccess'] = true;
                ctx.roleObject['createAssetFailureAndMaintenanceview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetFailureAndMaintenanceAccess'] = true;
                ctx.roleObject['createAssetFailureAndMaintenanceview'] = true;
                ctx.roleObject['createAssetFailureAndMaintenanceedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "createAssetSparesAndConsumables") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetSparesAndConsumablesAccess'] = true;
                ctx.roleObject['createAssetSparesAndConsumablesview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetSparesAndConsumablesAccess'] = true;
                ctx.roleObject['createAssetSparesAndConsumablesview'] = true;
                ctx.roleObject['createAssetSparesAndConsumablesedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "createAssetObjectivesAndKPIs") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetObjectivesAndKPIsAccess'] = true;
                ctx.roleObject['createAssetObjectivesAndKPIsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetObjectivesAndKPIsAccess'] = true;
                ctx.roleObject['createAssetObjectivesAndKPIsview'] = true;
                ctx.roleObject['createAssetObjectivesAndKPIsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "costTransferFunds") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['costTransferFundsAccess'] = true;
                ctx.roleObject['costTransferFundsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['costTransferFundsAccess'] = true;
                ctx.roleObject['costTransferFundsview'] = true;
                ctx.roleObject['costTransferFundsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "createAssetOthers") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetOthersAccess'] = true;
                ctx.roleObject['createAssetOthersview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetOthersAccess'] = true;
                ctx.roleObject['createAssetOthersview'] = true;
                ctx.roleObject['createAssetOthersedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "createAssetLand") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetLandAccess'] = true;
                ctx.roleObject['createAssetLandview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetLandAccess'] = true;
                ctx.roleObject['createAssetLandview'] = true;
                ctx.roleObject['createAssetLandedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetEmailConfiguration") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetEmailConfigurationAccess'] = true;
                ctx.roleObject['assetEmailConfigurationview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetEmailConfigurationAccess'] = true;
                ctx.roleObject['assetEmailConfigurationview'] = true;
                ctx.roleObject['assetEmailConfigurationedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetLifeCyclePlan") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetLifeCyclePlanAccess'] = true;
                ctx.roleObject['assetLifeCyclePlanview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetLifeCyclePlanAccess'] = true;
                ctx.roleObject['assetLifeCyclePlanview'] = true;
                ctx.roleObject['assetLifeCyclePlanedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "assetDepartment") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetDepartmentAccess'] = true;
                ctx.roleObject['assetDepartmentview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetDepartmentAccess'] = true;
                ctx.roleObject['assetDepartmentview'] = true;
                ctx.roleObject['assetDepartmentedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetPolicies") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetPoliciesAccess'] = true;
                ctx.roleObject['assetPoliciesview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetPoliciesAccess'] = true;
                ctx.roleObject['assetPoliciesview'] = true;
                ctx.roleObject['assetPoliciesedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "costCenter") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['costCenterAccess'] = true;
                ctx.roleObject['costCenterview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['costCenterAccess'] = true;
                ctx.roleObject['costCenterview'] = true;
                ctx.roleObject['costCenteredit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetRisks") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetRisksAccess'] = true;
                ctx.roleObject['assetRisksview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetRisksAccess'] = true;
                ctx.roleObject['assetRisksview'] = true;
                ctx.roleObject['assetRisksedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetDisposalMethods") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetDisposalMethodsAccess'] = true;
                ctx.roleObject['assetDisposalMethodsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetDisposalMethodsAccess'] = true;
                ctx.roleObject['assetDisposalMethodsview'] = true;
                ctx.roleObject['assetDisposalMethodsedit'] = true;
              }
            }


            else if (formDetails.value != undefined && formDetails.value == "assetDisposalMethods") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetDisposalMethodsAccess'] = true;
                ctx.roleObject['assetDisposalMethodsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetDisposalMethodsAccess'] = true;
                ctx.roleObject['assetDisposalMethodsview'] = true;
                ctx.roleObject['assetDisposalMethodsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "accountDetails") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['accountDetailsAccess'] = true;
                ctx.roleObject['accountDetailsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['accountDetailsAccess'] = true;
                ctx.roleObject['accountDetailsview'] = true;
                ctx.roleObject['accountDetailsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetKPIs") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetKPIsAccess'] = true;
                ctx.roleObject['assetKPIsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetServiceLevelAccess'] = true;
                ctx.roleObject['assetServiceLevelview'] = true;
                ctx.roleObject['assetServiceLeveledit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetStatus") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetStatusAccess'] = true;
                ctx.roleObject['assetStatusview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetStatusAccess'] = true;
                ctx.roleObject['assetStatusview'] = true;
                ctx.roleObject['assetStatusedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "documentListMaster") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['documentListMasterAccess'] = true;
                ctx.roleObject['documentListMasterview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['documentListMasterAccess'] = true;
                ctx.roleObject['documentListMasterview'] = true;
                ctx.roleObject['documentListMasteredit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetSparesAndConsumables") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetSparesAndConsumablesAccess'] = true;
                ctx.roleObject['assetSparesAndConsumablesview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetSparesAndConsumablesAccess'] = true;
                ctx.roleObject['assetSparesAndConsumablesview'] = true;
                ctx.roleObject['assetSparesAndConsumablesedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "failureClasses") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['failureClassesAccess'] = true;
                ctx.roleObject['failureClassesview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['failureClassesAccess'] = true;
                ctx.roleObject['failureClassesview'] = true;
                ctx.roleObject['failureClassesedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "typeOfDuty") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['typeOfDutyAccess'] = true;
                ctx.roleObject['typeOfDutyview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['typeOfDutyAccess'] = true;
                ctx.roleObject['typeOfDutyview'] = true;
                ctx.roleObject['typeOfDutyedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "maintenanceObjectives") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['maintenanceObjectivesAccess'] = true;
                ctx.roleObject['maintenanceObjectivesview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['maintenanceObjectivesAccess'] = true;
                ctx.roleObject['maintenanceObjectivesview'] = true;
                ctx.roleObject['maintenanceObjectivesedit'] = true;


              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetCriticality") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetCriticalityAccess'] = true;
                ctx.roleObject['assetCriticalityview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetCriticalityAccess'] = true;
                ctx.roleObject['assetCriticalityview'] = true;
                ctx.roleObject['assetCriticalityedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetProfile") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;

                ctx.roleObject['assetProfileAccess'] = true;
                ctx.roleObject['assetProfileview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;

                ctx.roleObject['assetProfileAccess'] = true;
                ctx.roleObject['assetProfileview'] = true;
                ctx.roleObject['assetProfileedit'] = true;


              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetGroup") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetGroupAccess'] = true;
                ctx.roleObject['assetGroupview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetGroupAccess'] = true;
                ctx.roleObject['assetGroupview'] = true;
                ctx.roleObject['assetGroupedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetType") {
                if (formDetails.view != undefined && formDetails.view) {
                  ctx.roleObject['landAndAsset'] = true;
                  ctx.roleObject['landAndAssetManagement'] = true;
                  ctx.roleObject['assetTypeAccess'] = true;
                  ctx.roleObject['assetTypeview'] = true;
                }
                if (formDetails.edit != undefined && formDetails.edit) {
                  ctx.roleObject['landAndAsset'] = true;
                  ctx.roleObject['landAndAssetManagement'] = true;
                  ctx.roleObject['assetTypeAccess'] = true;
                  ctx.roleObject['assetTypeview'] = true;
                  ctx.roleObject['assetTypeedit'] = true;
                }
              }
          }
        }
      }
      next();

    }else{
      next();
    }

  }

};
