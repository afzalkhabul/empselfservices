
var server = require('../../server/server');

module.exports = function(Petition) {
  Petition.validatesUniquenessOf('petitionId', {message: 'Petition ID could be unique'});
  Petition.observe('before save', function (ctx, next) {
     if(ctx.instance){
      ctx.instance.createdTime = new Date();
     next();
     }
    else {
      ctx.data.updatedTime = new Date();
      next();
         }
  });
  };
