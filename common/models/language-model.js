var fs = require('fs');
var wordsList = require("../../language/newWords.json");
module.exports = function(Languagemodel) {
  Languagemodel.updateData = function (words, cb) {
  var finalWords=words.word;
    if(finalWords!=undefined && finalWords!=null && finalWords.length>0){
      var duplicateData=[];
      Languagemodel.find({}, function (err, data) {
        if(data!=undefined &&data!=null && data.length>0){
          var updateObject=data[0];
          var finalData=data[0].token;
          for(var i=0;i<finalWords.length;i++){
            var keyName=finalWords[i].english;
            if((keyName in finalData) ){
              finalData[keyName]=finalWords[i].hindi;
            }else{
            }
          }

          updateObject.updateAttributes({'token':finalData},function(err, data){
            console.log('update successfully');
            /*  if()*/
            /*  var finalData={
             'Message':
             }*/
            cb(null,duplicateData);
          })
        }else{
          cb(null,'no data found');
        }

      });
    }else{
      cb(null,'Please send correct details')
    }
  }



  Languagemodel.remoteMethod(
    'updateData',
    {
      accepts: [
        {arg: 'words', type: 'object', http: {source: 'body'}}
      ],
      returns: [{type: 'object',arg:'data',root:true}],
      http: {
        path: '/updateData',
        verb: 'POST'
      }
    }
  );



  Languagemodel.postLanguageWords = function (words, cb) {

    console.log('words are'+JSON.stringify(words));
    var finalWords=words.word;
    if(finalWords!=undefined && finalWords!=null && finalWords.length>0){
      var duplicateData=[];
      Languagemodel.find({}, function (err, data) {
        if(data!=undefined &&data!=null && data.length>0){
          var updateObject=data[0];
          var finalData=data[0].token;
          for(var i=0;i<finalWords.length;i++){
            var keyName=finalWords[i].english;
            if(keyName!=null && keyName!=''){
              if((keyName in finalData) ){
                duplicateData.push(finalWords[i]);
              }else{
                finalData[keyName]=finalWords[i].hindi;
              }
            }
          }
          updateObject.updateAttributes({'token':finalData},function(err, data){
            console.log('update successfully');
          /*  if()*/
          /*  var finalData={
              'Message':
            }*/
            cb(null,duplicateData);
          })
        }else{
          cb(null,'no data found');
        }

      });
    }else{
      cb(null,'Please send correct details')
    }
  }



  Languagemodel.remoteMethod(
    'postLanguageWords',
    {
      accepts: [
        {arg: 'words', type: 'object', http: {source: 'body'}}
      ],
      returns: [{type: 'object',arg:'data',root:true}],
      http: {
        path: '/postLanguageWords',
        verb: 'POST'
      }
    }
  );

  Languagemodel.getLanguageData = function ( cb) {
      Languagemodel.find({}, function (err, data) {
          cb(null,data[0]);
      });
  }



  Languagemodel.remoteMethod(
    'getLanguageData',
    {
      accepts: [],
      returns: [{type: 'object',arg:'data',root:true}],
      http: {
        path: '/getLanguageData',
        verb: 'GET'
      }
    }
  );

  Languagemodel.addTranslationWords = function (cb) {
    console.log('..........................'+ JSON.stringify(wordsList));
    var keys = [];
    keys = Object.keys(wordsList);
    console.log(keys);

    if(keys.length > 0){
      var duplicateData=[];
      Languagemodel.find({}, function (err, data) {
        if(data!=undefined &&data!=null && data.length>0){
          var updateObject=data[0];
          var finalData=data[0].token;
          for(var i=0;i<keys.length;i++){
            var keyName=keys[i];
            if(keyName!=null && keyName!=''){
              if((keyName in finalData) ){
                duplicateData.push(keys[i]);
              }else{
                if(keyName == 'teacher'){
                  finalData[keyName]=wordsList[keyName];
                }else {
                  finalData[keyName] = '';
                }
              }
            }
          }
          updateObject.updateAttributes({'token':finalData},function(err, data){
            console.log('update successfully');
            /*  if()*/
            /*  var finalData={
             'Message':
             }*/
            cb(null,duplicateData);
          })
        }else{
          cb(null,'no data found');
        }

      });
    }else{
      cb('No data', null);
    }
  };



  Languagemodel.remoteMethod(
    'addTranslationWords',
    {
      accepts: [],
      returns: [{type: 'object',arg:'data',root:true}],
      http: {
        path: '/addTranslationWords',
        verb: 'GET'
      }
    }
  );

};
