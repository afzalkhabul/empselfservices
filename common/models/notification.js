var server = require('../../server/server');
module.exports = function(Notification) {
  Notification.observe('before save',function(ctx,next){
    if(ctx.instance){
      ctx.instance.createdTime=new Date();
      next();
    }else{
      ctx.data.updatedTime=new Date();
      next();
    }
  });
  Notification.getNotificationDetails = function (employeeId, cb) {
    var date = new Date();
    var yesterday = date - 1000 * 60 * 60 * 24 * 30;
    Notification.find({'where':{"createdTime":{"gt":new Date(yesterday)}}}, function (err, notificationDetails) {
      var finalNotification=[];
      var notificationCount=0;
      if(notificationDetails!=undefined && notificationDetails!=null && notificationDetails.length>0){
        for(var i=0;i<notificationDetails.length;i++){
          if(notificationDetails[i].to!=undefined && notificationDetails[i].to!=null && notificationDetails[i].to.length){
            var availableStatus=false;
            for(var x=0;x<notificationDetails[i].to.length;x++){
              if(notificationDetails[i].to[x].employeeId==employeeId){
                var objectDetails={
                  'id':notificationDetails[i].id,
                  'message':notificationDetails[i].text,
                  'readStatus':notificationDetails[i].to[x].readStatus,
                  "urlData": notificationDetails[i].urlData,
                  "type": notificationDetails[i].type,
                  'createdTime':notificationDetails[i].createdTime
                }
                if(!notificationDetails[i].to[x].readStatus){
                  notificationCount++;
                }
                finalNotification.push(objectDetails);
                availableStatus=true;
              break;
              }
            }
            if(!availableStatus) {
              if(notificationDetails[i].cc != undefined && notificationDetails[i].cc != null && notificationDetails[i].cc != '' && notificationDetails[i].cc.length > 0)
              {
                for (var x = 0; x < notificationDetails[i].cc.length; x++) {
                  if (notificationDetails[i].cc[x].employeeId == employeeId) {
                    var objectDetails = {
                      'id': notificationDetails[i].id,
                      'message': notificationDetails[i].message,
                      'readStatus': notificationDetails[i].cc[x].readStatus,
                      "urlData": notificationDetails[i].urlData,
                      "type": notificationDetails[i].type,
                      'createdTime': notificationDetails[i].createdTime
                    }
                    if (!notificationDetails[i].cc[x].readStatus) {
                      notificationCount++;
                    }
                    finalNotification.push(objectDetails);
                    break;
                  }
                }
              }


            }
          }
        }
        var details={
          'count':notificationCount,
          'data':finalNotification
        }
        cb(null,details);
      }else{
        var details={
          'count':0,
          'data':finalNotification
        }
        cb(null,details);
      }

    });
  }

  Notification.remoteMethod('getNotificationDetails', {
    description: "Send Valid employee Id",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'employeeId', type: 'string', http: {source: 'query'}}],
    http: {
      path: '/getNotificationDetails',
      verb: 'GET'
    }
  });
 Notification.updateNotification = function (notificationData, cb) {
   if(notificationData.notificationId!=undefined && notificationData.notificationId!=null && notificationData.notificationId!=''){
     Notification.findById(notificationData.notificationId, function (err, notification) {
       if(notification!=undefined  && notification!=null){
         var data={};
         if(notification.to!=undefined && notification.to!=null && notification.to.length>0){
           var toNotifications=notification.to;
           for(var x=0;x<notification.to.length;x++){
             if(notification.to[x].employeeId==notificationData.employeeId){
               toNotifications[x].readStatus=true;
             }
           }
           data.to=toNotifications;
         }
         if(notification.cc!=undefined && notification.cc!=null && notification.cc.length>0){
           var ccNotifications=notification.cc;
           for(var x=0;x<notification.cc.length;x++){
             if(notification.cc[x].employeeId==notificationData.employeeId){
               ccNotifications[x].readStatus=true;
             }
           }
           data.cc=ccNotifications;
         }
         notification.updateAttributes(data,function(err,latestData){
           cb(null, latestData)
         });
       }else{
         cb(null,'No Notification is available');
       }
     });
   }else{
     cb(null,'please send proper Input');
   }

  }

  Notification.remoteMethod('updateNotification', {
    description: "Send Valid employee Id",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'notificationData', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateNotification',
      verb: 'POST'
    }
  });


  var cron = require('node-cron');
  cron.schedule('0 20 11 * * *', function(){
    var ProjectPlan=server.models.ProjectPlan;
    var ProjectTORDetails=server.models.ProjectTORDetails;
    var FieldVisit=server.models.FieldVisit;
    var ProjectUploads=server.models.ProjectUploads;
    var TransferFund=server.models.TransferFund;
    var ProjectMilestone=server.models.ProjectMilestone;
    var ProjectRebaseline=server.models.ProjectRebaseline;
    var AppConfig=server.models.AppConfig;
    var date = new Date();

    AppConfig.find({},function(err, appData){
      if(appData!=undefined && appData!=null && appData.length>0){
       if(appData[0].autoEscalation!=undefined && appData[0].autoEscalation!=null && appData[0].autoEscalation
         && appData[0].escalationDays!=undefined && appData[0].escalationDays!=null && appData[0].escalationDays>0 ){
         var yesterday = date - 1000 * 60 * 60 * 24 * appData[0].escalationDays;
         ProjectPlan.find({"where":{"and":[{"lastUpdateTime":{"lt":new Date(yesterday)}},{'finalStatus':false}]}}, function (err, planDetails) {
           if(planDetails!=undefined && planDetails!=null && planDetails.length>0){
             for(var i=0;i<planDetails.length;i++){
               if(planDetails[i].acceptLevel!=undefined && planDetails[i].acceptLevel!=null && planDetails[i].acceptLevel!='' ){
                 var workflowDetails=planDetails[i].workflowData;
                 if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
                   var nextLevelEmployee=[];
                   var currentLevelEmployees=[];
                   for(var x=0;x<workflowDetails.length;x++){
                     if((parseInt(workflowDetails[x].levelNo)+1)>planDetails[i].maxlevel){
                       nextLevelEmployee.push({'employeeId':'projectadmin00001'});
                     }
                     else  if(workflowDetails[x].levelNo!=undefined && workflowDetails[x].levelNo!=null && workflowDetails[x].levelNo!='' && parseInt(workflowDetails[x].levelNo)==(planDetails[i].acceptLevel+2)){
                       if(workflowDetails[x].employees!=undefined && workflowDetails[x].employees!=null &&  workflowDetails[x].employees.length>0){
                         for(var count=0;count<workflowDetails[x].employees.length;count++){
                           var details={
                             'employeeId':workflowDetails[x].employees[count]
                           }
                           nextLevelEmployee.push(details);
                         }
                       }
                       nextLevelEmployee.push({'employeeId':'projectadmin00001'});
                     }
                     if(workflowDetails[x].levelNo!=undefined && workflowDetails[x].levelNo!=null && workflowDetails[x].levelNo!='' && parseInt(workflowDetails[x].levelNo)==(planDetails[i].acceptLevel+1)){
                       if(workflowDetails[x].employees!=undefined && workflowDetails[x].employees!=null &&  workflowDetails[x].employees.length>0){
                         for(var count=0;count<workflowDetails[x].employees.length;count++){
                           var details={
                             'employeeId':workflowDetails[x].employees[count]
                           }
                           currentLevelEmployees.push(details);
                         }
                       }
                     }
                   }
                   var  Employee=server.models.Employee;
                   var DhanbadEmail=server.models.DhanbadEmail;
                   var ccEmployeeEmails=[];
                   var toEmployeeEmails=[];
                   var toEmployeeNotification=[];
                   var ccEmployeeNotification=[];
                   Employee.find({'where':{'or':nextLevelEmployee}},function(err, nextLevelEmployeeData){
                     if(nextLevelEmployeeData!=undefined && nextLevelEmployeeData!=null && nextLevelEmployeeData.length>0){
                       for(var firstData=0;firstData<nextLevelEmployeeData.length;firstData++){
                         ccEmployeeEmails.push(nextLevelEmployeeData[firstData].email);
                         ccEmployeeNotification.push({'employeeId':nextLevelEmployeeData[firstData].employeeId,'readStatus':false});
                       }
                     }
                     if(currentLevelEmployees!=undefined && currentLevelEmployees!=null && currentLevelEmployees.length>0){
                       Employee.find({'where':{'or':currentLevelEmployees}},function(err, currentLevelEmployeeData){
                         if(currentLevelEmployeeData!=undefined && currentLevelEmployeeData!=null && currentLevelEmployeeData.length>0){
                           for(var firstData=0;firstData<currentLevelEmployeeData.length;firstData++){
                             toEmployeeEmails.push(currentLevelEmployeeData[firstData].email);
                             toEmployeeNotification.push({'employeeId':currentLevelEmployeeData[firstData].employeeId,'readStatus':false});

                           }
                         }
                         DhanbadEmail.create({
                           "to": toEmployeeEmails.toString(),
                           "subject":  'Pending Tasks',
                           "text":"Please Approve Project Plan Request",
                           "cc":ccEmployeeEmails.toString()
                         }, function (err, email) {
                           console.log(email);
                         });

                         Notification.create({
                           'to':toEmployeeNotification,
                           "subject":  'Pending Tasks',
                           "text":"Please Approve Project Plan Request",
                           "message":"Project Plan Request Approval is pending from your below level persons",
                           "cc":ccEmployeeNotification,
                           'urlData':'projectPlanDetails',
                           "type":"escalate"
                         }, function (err, notificationDetails) {
                         })
                       });
                     }else{
                       console.log('to employeeEmails'+toEmployeeEmails);
                       console.log('cc employeeEmails'+ccEmployeeEmails);
                       DhanbadEmail.create({
                         "to": toEmployeeEmails.toString(),
                         "subject":  'Pending Tasks',
                         "text":"Please Approve Project Plan Request",
                         "cc":ccEmployeeEmails.toString()
                       }, function (err, email) {
                         console.log(email);
                       });

                       Notification.create({
                         'to':toEmployeeNotification,
                         "subject":  'Pending Tasks',
                         "text":"Please Approve Project Plan Request",
                         "message":"Project Plan Request Approval is pending from your below level persons",
                         "cc":ccEmployeeNotification,
                         'urlData':'projectPlanDetails',
                         "type":"escalate"
                       }, function (err, notificationDetails) {

                       })
                     }
                   });
                 }
               }
             }
           }
         });
         ProjectTORDetails.find({"where":{"and":[{"lastUpdateTime":{"lt":new Date(yesterday)}},{'finalStatus':false}]}}, function (err, planDetails) {
                   if(planDetails!=undefined && planDetails!=null && planDetails.length>0){
                     for(var i=0;i<planDetails.length;i++){
                       if(planDetails[i].acceptLevel!=undefined && planDetails[i].acceptLevel!=null && planDetails[i].acceptLevel!='' ){
                         var workflowDetails=planDetails[i].workflowData;
                         if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
                           var nextLevelEmployee=[];
                           var currentLevelEmployees=[];
                           for(var x=0;x<workflowDetails.length;x++){
                             if((parseInt(workflowDetails[x].levelNo)+1)>planDetails[i].maxlevel){
                               nextLevelEmployee.push({'employeeId':'projectadmin00001'});
                             }
                             else  if(workflowDetails[x].levelNo!=undefined && workflowDetails[x].levelNo!=null && workflowDetails[x].levelNo!='' && parseInt(workflowDetails[x].levelNo)==(planDetails[i].acceptLevel+2)){
                               if(workflowDetails[x].employees!=undefined && workflowDetails[x].employees!=null &&  workflowDetails[x].employees.length>0){
                                 for(var count=0;count<workflowDetails[x].employees.length;count++){
                                   var details={
                                     'employeeId':workflowDetails[x].employees[count]
                                   }
                                   nextLevelEmployee.push(details);
                                 }
                               }
                               nextLevelEmployee.push({'employeeId':'projectadmin00001'});
                             }
                             if(workflowDetails[x].levelNo!=undefined && workflowDetails[x].levelNo!=null && workflowDetails[x].levelNo!='' && parseInt(workflowDetails[x].levelNo)==(planDetails[i].acceptLevel+1)){
                               if(workflowDetails[x].employees!=undefined && workflowDetails[x].employees!=null &&  workflowDetails[x].employees.length>0){
                                 for(var count=0;count<workflowDetails[x].employees.length;count++){
                                   var details={
                                     'employeeId':workflowDetails[x].employees[count]
                                   }
                                   currentLevelEmployees.push(details);
                                 }
                               }
                             }
                           }
                           var  Employee=server.models.Employee;
                           var DhanbadEmail=server.models.DhanbadEmail;
                           var ccEmployeeEmails=[];
                           var toEmployeeEmails=[];
                           var toEmployeeNotification=[];
                           var ccEmployeeNotification=[];
                           Employee.find({'where':{'or':nextLevelEmployee}},function(err, nextLevelEmployeeData){
                             if(nextLevelEmployeeData!=undefined && nextLevelEmployeeData!=null && nextLevelEmployeeData.length>0){
                               for(var firstData=0;firstData<nextLevelEmployeeData.length;firstData++){
                                 ccEmployeeEmails.push(nextLevelEmployeeData[firstData].email);
                                 ccEmployeeNotification.push({'employeeId':nextLevelEmployeeData[firstData].employeeId,'readStatus':false});
                               }
                             }
                             if(currentLevelEmployees!=undefined && currentLevelEmployees!=null && currentLevelEmployees.length>0){
                               Employee.find({'where':{'or':currentLevelEmployees}},function(err, currentLevelEmployeeData){
                                 if(currentLevelEmployeeData!=undefined && currentLevelEmployeeData!=null && currentLevelEmployeeData.length>0){
                                   for(var firstData=0;firstData<currentLevelEmployeeData.length;firstData++){
                                     toEmployeeEmails.push(currentLevelEmployeeData[firstData].email);
                                     toEmployeeNotification.push({'employeeId':currentLevelEmployeeData[firstData].employeeId,'readStatus':false});
                                   }
                                 }
                                 DhanbadEmail.create({
                                   "to": toEmployeeEmails.toString(),
                                   "subject":  'Pending Tasks',
                                   "text":"Please Approve Project TOR Request",
                                   "cc":ccEmployeeEmails.toString()
                                 }, function (err, email) {
                                   console.log(email);
                                 });

                                 Notification.create({
                                   'to':toEmployeeNotification,
                                   "subject":  'Pending Tasks',
                                   "text":"Please Approve Project TOR Request",
                                   "message":"Project TOR Request Approval is pending from your below level persons",
                                   "cc":ccEmployeeNotification,
                                   'urlData':'projectTORDetails',
                                   "type":"escalate"
                                 }, function (err, notificationDetails) {
                                 })
                               });
                             }else{
                               console.log('to employeeEmails'+toEmployeeEmails);
                               console.log('cc employeeEmails'+ccEmployeeEmails);
                               DhanbadEmail.create({
                                 "to": toEmployeeEmails.toString(),
                                 "subject":  'Pending Tasks',
                                 "text":"Please Approve Project TOR Request",
                                 "cc":ccEmployeeEmails.toString()
                               }, function (err, email) {
                                 console.log(email);
                               });

                               Notification.create({
                                 'to':toEmployeeNotification,
                                 "subject":  'Pending Tasks',
                                 "text":"Please Approve Project TOR Request",
                                 "message":"Project TOR Request Approval is pending from your below level persons",
                                 "cc":ccEmployeeNotification,
                                 'urlData':'projectTORDetails',
                                 "type":"escalate"
                               }, function (err, notificationDetails) {

                               })
                             }
                           });
                         }
                       }
                     }
                   }
                 });
         FieldVisit.find({"where":{"and":[{"lastUpdateTime":{"lt":new Date(yesterday)}},{'finalStatus':false}]}}, function (err, planDetails) {
                   if(planDetails!=undefined && planDetails!=null && planDetails.length>0){
                     for(var i=0;i<planDetails.length;i++){
                       if(planDetails[i].acceptLevel!=undefined && planDetails[i].acceptLevel!=null && planDetails[i].acceptLevel!='' ){
                         var workflowDetails=planDetails[i].workflowData;
                         if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
                           var nextLevelEmployee=[];
                           var currentLevelEmployees=[];
                           for(var x=0;x<workflowDetails.length;x++){
                             if((parseInt(workflowDetails[x].levelNo)+1)>planDetails[i].maxlevel){
                               nextLevelEmployee.push({'employeeId':'projectadmin00001'});
                             }
                             else  if(workflowDetails[x].levelNo!=undefined && workflowDetails[x].levelNo!=null && workflowDetails[x].levelNo!='' && parseInt(workflowDetails[x].levelNo)==(planDetails[i].acceptLevel+2)){
                               if(workflowDetails[x].employees!=undefined && workflowDetails[x].employees!=null &&  workflowDetails[x].employees.length>0){
                                 for(var count=0;count<workflowDetails[x].employees.length;count++){
                                   var details={
                                     'employeeId':workflowDetails[x].employees[count]
                                   }
                                   nextLevelEmployee.push(details);
                                 }
                               }
                               nextLevelEmployee.push({'employeeId':'projectadmin00001'});
                             }
                             if(workflowDetails[x].levelNo!=undefined && workflowDetails[x].levelNo!=null && workflowDetails[x].levelNo!='' && parseInt(workflowDetails[x].levelNo)==(planDetails[i].acceptLevel+1)){
                               if(workflowDetails[x].employees!=undefined && workflowDetails[x].employees!=null &&  workflowDetails[x].employees.length>0){
                                 for(var count=0;count<workflowDetails[x].employees.length;count++){
                                   var details={
                                     'employeeId':workflowDetails[x].employees[count]
                                   }
                                   currentLevelEmployees.push(details);
                                 }
                               }
                             }
                           }
                           var  Employee=server.models.Employee;
                           var DhanbadEmail=server.models.DhanbadEmail;
                           var ccEmployeeEmails=[];
                           var toEmployeeEmails=[];
                           var toEmployeeNotification=[];
                           var ccEmployeeNotification=[];
                           Employee.find({'where':{'or':nextLevelEmployee}},function(err, nextLevelEmployeeData){
                             if(nextLevelEmployeeData!=undefined && nextLevelEmployeeData!=null && nextLevelEmployeeData.length>0){
                               for(var firstData=0;firstData<nextLevelEmployeeData.length;firstData++){
                                 ccEmployeeEmails.push(nextLevelEmployeeData[firstData].email);
                                 ccEmployeeNotification.push({'employeeId':nextLevelEmployeeData[firstData].employeeId,'readStatus':false});
                               }
                             }
                             if(currentLevelEmployees!=undefined && currentLevelEmployees!=null && currentLevelEmployees.length>0){
                               Employee.find({'where':{'or':currentLevelEmployees}},function(err, currentLevelEmployeeData){
                                 if(currentLevelEmployeeData!=undefined && currentLevelEmployeeData!=null && currentLevelEmployeeData.length>0){
                                   for(var firstData=0;firstData<currentLevelEmployeeData.length;firstData++){
                                     toEmployeeEmails.push(currentLevelEmployeeData[firstData].email);
                                     toEmployeeNotification.push({'employeeId':currentLevelEmployeeData[firstData].employeeId,'readStatus':false});
                                   }
                                 }
                                 DhanbadEmail.create({
                                   "to": toEmployeeEmails.toString(),
                                   "subject":  'Pending Tasks',
                                   "text":"Please Approve Project Field Visit Request",
                                   "cc":ccEmployeeEmails.toString()
                                 }, function (err, email) {
                                   console.log(email);
                                 });

                                 Notification.create({
                                   'to':toEmployeeNotification,
                                   "subject":  'Pending Tasks',
                                   "text":"Please Approve Project Field Visit Request",
                                   "message":"Project Field Visit Request Approval is pending from your below level persons",
                                   "cc":ccEmployeeNotification,
                                   'urlData':'fieldVisitReport',
                                   "type":"escalate"
                                 }, function (err, notificationDetails) {
                                 })
                               });
                             }else{

                               DhanbadEmail.create({
                                 "to": toEmployeeEmails.toString(),
                                 "subject":  'Pending Tasks',
                                 "text":"Please Approve Project Field Visit Request",
                                 "cc":ccEmployeeEmails.toString()
                               }, function (err, email) {
                                 console.log(email);
                               });

                               Notification.create({
                                 'to':toEmployeeNotification,
                                 "subject":  'Pending Tasks',
                                 "text":"Please Approve Project Field Visit Request",
                                 "message":"Project Field Visit Request Approval is pending from your below level persons",
                                 "cc":ccEmployeeNotification,
                                 'urlData':'fieldVisitReport',
                                 "type":"escalate"
                               }, function (err, notificationDetails) {

                               })
                             }
                           });
                         }
                       }
                     }
                   }

                       });
         ProjectUploads.find({"where":{"and":[{"lastUpdateTime":{"lt":new Date(yesterday)}},{'finalStatus':false},{ 'type':'layout'}]}}, function (err, planDetails) {
            if(planDetails!=undefined && planDetails!=null && planDetails.length>0){
           for(var i=0;i<planDetails.length;i++){
             if(planDetails[i].acceptLevel!=undefined && planDetails[i].acceptLevel!=null && planDetails[i].acceptLevel!='' ){
               var workflowDetails=planDetails[i].workflowData;
               if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
                 var nextLevelEmployee=[];
                 var currentLevelEmployees=[];
                 for(var x=0;x<workflowDetails.length;x++){
                   if((parseInt(workflowDetails[x].levelNo)+1)>planDetails[i].maxlevel){
                     nextLevelEmployee.push({'employeeId':'projectadmin00001'});
                   }
                   else  if(workflowDetails[x].levelNo!=undefined && workflowDetails[x].levelNo!=null && workflowDetails[x].levelNo!='' && parseInt(workflowDetails[x].levelNo)==(planDetails[i].acceptLevel+2)){
                     if(workflowDetails[x].employees!=undefined && workflowDetails[x].employees!=null &&  workflowDetails[x].employees.length>0){
                       for(var count=0;count<workflowDetails[x].employees.length;count++){
                         var details={
                           'employeeId':workflowDetails[x].employees[count]
                         }
                         nextLevelEmployee.push(details);
                       }
                     }
                     nextLevelEmployee.push({'employeeId':'projectadmin00001'});
                   }
                   if(workflowDetails[x].levelNo!=undefined && workflowDetails[x].levelNo!=null && workflowDetails[x].levelNo!='' && parseInt(workflowDetails[x].levelNo)==(planDetails[i].acceptLevel+1)){
                     if(workflowDetails[x].employees!=undefined && workflowDetails[x].employees!=null &&  workflowDetails[x].employees.length>0){
                       for(var count=0;count<workflowDetails[x].employees.length;count++){
                         var details={
                           'employeeId':workflowDetails[x].employees[count]
                         }
                         currentLevelEmployees.push(details);
                       }
                     }
                   }
                 }
                 var  Employee=server.models.Employee;
                 var DhanbadEmail=server.models.DhanbadEmail;
                 var ccEmployeeEmails=[];
                 var toEmployeeEmails=[];
                 var toEmployeeNotification=[];
                 var ccEmployeeNotification=[];
                 Employee.find({'where':{'or':nextLevelEmployee}},function(err, nextLevelEmployeeData){
                   if(nextLevelEmployeeData!=undefined && nextLevelEmployeeData!=null && nextLevelEmployeeData.length>0){
                     for(var firstData=0;firstData<nextLevelEmployeeData.length;firstData++){
                       ccEmployeeEmails.push(nextLevelEmployeeData[firstData].email);
                       ccEmployeeNotification.push({'employeeId':nextLevelEmployeeData[firstData].employeeId,'readStatus':false});
                     }
                   }
                   if(currentLevelEmployees!=undefined && currentLevelEmployees!=null && currentLevelEmployees.length>0){
                     Employee.find({'where':{'or':currentLevelEmployees}},function(err, currentLevelEmployeeData){
                       if(currentLevelEmployeeData!=undefined && currentLevelEmployeeData!=null && currentLevelEmployeeData.length>0){
                         for(var firstData=0;firstData<currentLevelEmployeeData.length;firstData++){
                           toEmployeeEmails.push(currentLevelEmployeeData[firstData].email);
                           toEmployeeNotification.push({'employeeId':currentLevelEmployeeData[firstData].employeeId,'readStatus':false});
                         }
                       }
                       DhanbadEmail.create({
                         "to": toEmployeeEmails.toString(),
                         "subject":  'Pending Tasks',
                         "text":"Please Approve Project Layout Request",
                         "cc":ccEmployeeEmails.toString()
                       }, function (err, email) {
                         console.log(email);
                       });

                       Notification.create({
                         'to':toEmployeeNotification,
                         "subject":  'Pending Tasks',
                         "text":"Please Approve Project Layout Request",
                         "message":"Project Layout Request Approval is pending from your below level persons",
                         "cc":ccEmployeeNotification,
                         'urlData':'planLayout',
                         "type":"escalate"
                       }, function (err, notificationDetails) {
                       })
                     });
                   }else{

                     DhanbadEmail.create({
                       "to": toEmployeeEmails.toString(),
                       "subject":  'Pending Tasks',
                       "text":"Please Approve Project Layout Request",
                       "cc":ccEmployeeEmails.toString()
                     }, function (err, email) {
                       console.log(email);
                     });

                     Notification.create({
                       'to':toEmployeeNotification,
                       "subject":  'Pending Tasks',
                       "text":"Please Approve Project Layout Request",
                       "message":"Project Layout Request Approval is pending from your below level persons",
                       "cc":ccEmployeeNotification,
                       'urlData':'planLayout',
                       "type":"escalate"
                     }, function (err, notificationDetails) {
                     })
                   }
                 });
               }
             }
           }
         }

         });
         TransferFund.find({"where":{"and":[{"lastUpdateTime":{"lt":new Date(yesterday)}},{'finalStatus':false}]}}, function (err, planDetails) {
           if(planDetails!=undefined && planDetails!=null && planDetails.length>0){
             for(var i=0;i<planDetails.length;i++){
               if(planDetails[i].acceptLevel!=undefined && planDetails[i].acceptLevel!=null && planDetails[i].acceptLevel!='' ){
                 var workflowDetails=planDetails[i].workflowData;
                 if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
                   var nextLevelEmployee=[];
                   var currentLevelEmployees=[];
                   for(var x=0;x<workflowDetails.length;x++){
                     if((parseInt(workflowDetails[x].levelNo)+1)>planDetails[i].maxlevel){
                       nextLevelEmployee.push({'employeeId':'projectadmin00001'});
                     }
                     else  if(workflowDetails[x].levelNo!=undefined && workflowDetails[x].levelNo!=null && workflowDetails[x].levelNo!='' && parseInt(workflowDetails[x].levelNo)==(planDetails[i].acceptLevel+2)){
                       if(workflowDetails[x].employees!=undefined && workflowDetails[x].employees!=null &&  workflowDetails[x].employees.length>0){
                         for(var count=0;count<workflowDetails[x].employees.length;count++){
                           var details={
                             'employeeId':workflowDetails[x].employees[count]
                           }
                           nextLevelEmployee.push(details);
                         }
                       }
                       nextLevelEmployee.push({'employeeId':'projectadmin00001'});
                     }
                     if(workflowDetails[x].levelNo!=undefined && workflowDetails[x].levelNo!=null && workflowDetails[x].levelNo!='' && parseInt(workflowDetails[x].levelNo)==(planDetails[i].acceptLevel+1)){
                       if(workflowDetails[x].employees!=undefined && workflowDetails[x].employees!=null &&  workflowDetails[x].employees.length>0){
                         for(var count=0;count<workflowDetails[x].employees.length;count++){
                           var details={
                             'employeeId':workflowDetails[x].employees[count]
                           }
                           currentLevelEmployees.push(details);
                         }
                       }
                     }
                   }
                   var  Employee=server.models.Employee;
                   var DhanbadEmail=server.models.DhanbadEmail;
                   var ccEmployeeEmails=[];
                   var toEmployeeEmails=[];
                   var toEmployeeNotification=[];
                   var ccEmployeeNotification=[];
                   Employee.find({'where':{'or':nextLevelEmployee}},function(err, nextLevelEmployeeData){
                     if(nextLevelEmployeeData!=undefined && nextLevelEmployeeData!=null && nextLevelEmployeeData.length>0){
                       for(var firstData=0;firstData<nextLevelEmployeeData.length;firstData++){
                         ccEmployeeEmails.push(nextLevelEmployeeData[firstData].email);
                         ccEmployeeNotification.push({'employeeId':nextLevelEmployeeData[firstData].employeeId,'readStatus':false});
                       }
                     }
                     if(currentLevelEmployees!=undefined && currentLevelEmployees!=null && currentLevelEmployees.length>0){
                       Employee.find({'where':{'or':currentLevelEmployees}},function(err, currentLevelEmployeeData){
                         if(currentLevelEmployeeData!=undefined && currentLevelEmployeeData!=null && currentLevelEmployeeData.length>0){
                           for(var firstData=0;firstData<currentLevelEmployeeData.length;firstData++){
                             toEmployeeEmails.push(currentLevelEmployeeData[firstData].email);
                             toEmployeeNotification.push({'employeeId':currentLevelEmployeeData[firstData].employeeId,'readStatus':false});
                           }
                         }
                         DhanbadEmail.create({
                           "to": toEmployeeEmails.toString(),
                           "subject":  'Pending Tasks',
                           "text":"Please Approve Project Transfer Founds Request",
                           "cc":ccEmployeeEmails.toString()
                         }, function (err, email) {
                           console.log(email);
                         });

                         Notification.create({
                           'to':toEmployeeNotification,
                           "subject":  'Pending Tasks',
                           "text":"Please Approve Project Transfer Founds Request",
                           "message":"Project Transfer Founds Request Approval is pending from your below level persons",
                           "cc":ccEmployeeNotification,
                           'urlData':'transferFunds ',
                           "type":"escalate"
                         }, function (err, notificationDetails) {
                         })
                       });
                     }else{

                       DhanbadEmail.create({
                         "to": toEmployeeEmails.toString(),
                         "subject":  'Pending Tasks',
                         "text":"Please Approve Project Transfer Founds Request",
                         "cc":ccEmployeeEmails.toString()
                       }, function (err, email) {
                         console.log(email);
                       });

                       Notification.create({
                         'to':toEmployeeNotification,
                         "subject":  'Pending Tasks',
                         "text":"Please Approve Project Transfer Founds Request",
                         "message":"Project Transfer Founds Request Approval is pending from your below level persons",
                         "cc":ccEmployeeNotification,
                         'urlData':'transferFunds',
                         "type":"escalate"
                       }, function (err, notificationDetails) {
                       })
                     }
                   });
                 }
               }
             }
           }

         });
         ProjectMilestone.find({"where":{"and":[{"lastUpdateTime":{"lt":new Date(yesterday)}},{'finalStatus':false}]}}, function (err, planDetails) {
           if(planDetails!=undefined && planDetails!=null && planDetails.length>0){
             for(var i=0;i<planDetails.length;i++){
               if(planDetails[i].acceptLevel!=undefined && planDetails[i].acceptLevel!=null && planDetails[i].acceptLevel!='' ){
                 var workflowDetails=planDetails[i].workflowData;
                 if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
                   var nextLevelEmployee=[];
                   var currentLevelEmployees=[];
                   for(var x=0;x<workflowDetails.length;x++){
                     if((parseInt(workflowDetails[x].levelNo)+1)>planDetails[i].maxlevel){
                       nextLevelEmployee.push({'employeeId':'projectadmin00001'});
                     }
                     else  if(workflowDetails[x].levelNo!=undefined && workflowDetails[x].levelNo!=null && workflowDetails[x].levelNo!='' && parseInt(workflowDetails[x].levelNo)==(planDetails[i].acceptLevel+2)){
                       if(workflowDetails[x].employees!=undefined && workflowDetails[x].employees!=null &&  workflowDetails[x].employees.length>0){
                         for(var count=0;count<workflowDetails[x].employees.length;count++){
                           var details={
                             'employeeId':workflowDetails[x].employees[count]
                           }
                           nextLevelEmployee.push(details);
                         }
                       }
                       nextLevelEmployee.push({'employeeId':'projectadmin00001'});
                     }
                     if(workflowDetails[x].levelNo!=undefined && workflowDetails[x].levelNo!=null && workflowDetails[x].levelNo!='' && parseInt(workflowDetails[x].levelNo)==(planDetails[i].acceptLevel+1)){
                       if(workflowDetails[x].employees!=undefined && workflowDetails[x].employees!=null &&  workflowDetails[x].employees.length>0){
                         for(var count=0;count<workflowDetails[x].employees.length;count++){
                           var details={
                             'employeeId':workflowDetails[x].employees[count]
                           }
                           currentLevelEmployees.push(details);
                         }
                       }
                     }
                   }
                   var  Employee=server.models.Employee;
                   var DhanbadEmail=server.models.DhanbadEmail;
                   var ccEmployeeEmails=[];
                   var toEmployeeEmails=[];
                   var toEmployeeNotification=[];
                   var ccEmployeeNotification=[];
                   Employee.find({'where':{'or':nextLevelEmployee}},function(err, nextLevelEmployeeData){
                     if(nextLevelEmployeeData!=undefined && nextLevelEmployeeData!=null && nextLevelEmployeeData.length>0){
                       for(var firstData=0;firstData<nextLevelEmployeeData.length;firstData++){
                         ccEmployeeEmails.push(nextLevelEmployeeData[firstData].email);
                         ccEmployeeNotification.push({'employeeId':nextLevelEmployeeData[firstData].employeeId,'readStatus':false});
                       }
                     }
                     if(currentLevelEmployees!=undefined && currentLevelEmployees!=null && currentLevelEmployees.length>0){
                       Employee.find({'where':{'or':currentLevelEmployees}},function(err, currentLevelEmployeeData){
                         if(currentLevelEmployeeData!=undefined && currentLevelEmployeeData!=null && currentLevelEmployeeData.length>0){
                           for(var firstData=0;firstData<currentLevelEmployeeData.length;firstData++){
                             toEmployeeEmails.push(currentLevelEmployeeData[firstData].email);
                             toEmployeeNotification.push({'employeeId':currentLevelEmployeeData[firstData].employeeId,'readStatus':false});
                           }
                         }
                         DhanbadEmail.create({
                           "to": toEmployeeEmails.toString(),
                           "subject":  'Pending Tasks',
                           "text":"Please Approve Project Milestone Request",
                           "cc":ccEmployeeEmails.toString()
                         }, function (err, email) {
                           console.log(email);
                         });

                         Notification.create({
                           'to':toEmployeeNotification,
                           "subject":  'Pending Tasks',
                           "text":"Please Approve Project Milestone Request",
                           "message":"Project Milestone Request Approval is pending from your below level persons",
                           "cc":ccEmployeeNotification,
                           'urlData':'projectMileStone ',
                           "type":"escalate"
                         }, function (err, notificationDetails) {
                         })
                       });
                     }else{

                       DhanbadEmail.create({
                         "to": toEmployeeEmails.toString(),
                         "subject":  'Pending Tasks',
                         "text":"Please Approve Project Milestone Request",
                         "cc":ccEmployeeEmails.toString()
                       }, function (err, email) {
                         console.log(email);
                       });

                       Notification.create({
                         'to':toEmployeeNotification,
                         "subject":  'Pending Tasks',
                         "text":"Please Approve Project Milestone Request",
                         "message":"Project Milestone Request Approval is pending from your below level persons",
                         "cc":ccEmployeeNotification,
                         'urlData':'projectMileStone',
                         "type":"escalate"
                       }, function (err, notificationDetails) {
                       })
                     }
                   });
                 }
               }
             }
           }

         });
         ProjectRebaseline.find({"where":{"and":[{"lastUpdateTime":{"lt":new Date(yesterday)}},{'finalStatus':false}]}}, function (err, planDetails) {
           if(planDetails!=undefined && planDetails!=null && planDetails.length>0){
             for(var i=0;i<planDetails.length;i++){
               if(planDetails[i].acceptLevel!=undefined && planDetails[i].acceptLevel!=null && planDetails[i].acceptLevel!='' ){
                 var workflowDetails=planDetails[i].workflowData;
                 if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
                   var nextLevelEmployee=[];
                   var currentLevelEmployees=[];
                   for(var x=0;x<workflowDetails.length;x++){
                     if((parseInt(workflowDetails[x].levelNo)+1)>planDetails[i].maxlevel){
                       nextLevelEmployee.push({'employeeId':'projectadmin00001'});
                     }
                     else  if(workflowDetails[x].levelNo!=undefined && workflowDetails[x].levelNo!=null && workflowDetails[x].levelNo!='' && parseInt(workflowDetails[x].levelNo)==(planDetails[i].acceptLevel+2)){
                       if(workflowDetails[x].employees!=undefined && workflowDetails[x].employees!=null &&  workflowDetails[x].employees.length>0){
                         for(var count=0;count<workflowDetails[x].employees.length;count++){
                           var details={
                             'employeeId':workflowDetails[x].employees[count]
                           }
                           nextLevelEmployee.push(details);
                         }
                       }
                       nextLevelEmployee.push({'employeeId':'projectadmin00001'});
                     }
                     if(workflowDetails[x].levelNo!=undefined && workflowDetails[x].levelNo!=null && workflowDetails[x].levelNo!='' && parseInt(workflowDetails[x].levelNo)==(planDetails[i].acceptLevel+1)){
                       if(workflowDetails[x].employees!=undefined && workflowDetails[x].employees!=null &&  workflowDetails[x].employees.length>0){
                         for(var count=0;count<workflowDetails[x].employees.length;count++){
                           var details={
                             'employeeId':workflowDetails[x].employees[count]
                           }
                           currentLevelEmployees.push(details);
                         }
                       }
                     }
                   }
                   var  Employee=server.models.Employee;
                   var DhanbadEmail=server.models.DhanbadEmail;
                   var ccEmployeeEmails=[];
                   var toEmployeeEmails=[];
                   var toEmployeeNotification=[];
                   var ccEmployeeNotification=[];
                   Employee.find({'where':{'or':nextLevelEmployee}},function(err, nextLevelEmployeeData){
                     if(nextLevelEmployeeData!=undefined && nextLevelEmployeeData!=null && nextLevelEmployeeData.length>0){
                       for(var firstData=0;firstData<nextLevelEmployeeData.length;firstData++){
                         ccEmployeeEmails.push(nextLevelEmployeeData[firstData].email);
                         ccEmployeeNotification.push({'employeeId':nextLevelEmployeeData[firstData].employeeId,'readStatus':false});
                       }
                     }
                     if(currentLevelEmployees!=undefined && currentLevelEmployees!=null && currentLevelEmployees.length>0){
                       Employee.find({'where':{'or':currentLevelEmployees}},function(err, currentLevelEmployeeData){
                         if(currentLevelEmployeeData!=undefined && currentLevelEmployeeData!=null && currentLevelEmployeeData.length>0){
                           for(var firstData=0;firstData<currentLevelEmployeeData.length;firstData++){
                             toEmployeeEmails.push(currentLevelEmployeeData[firstData].email);
                             toEmployeeNotification.push({'employeeId':currentLevelEmployeeData[firstData].employeeId,'readStatus':false});
                           }
                         }
                         DhanbadEmail.create({
                           "to": toEmployeeEmails.toString(),
                           "subject":  'Pending Tasks',
                           "text":"Please Approve Project Rebaseline Request",
                           "cc":ccEmployeeEmails.toString()
                         }, function (err, email) {
                           console.log(email);
                         });

                         Notification.create({
                           'to':toEmployeeNotification,
                           "subject":  'Pending Tasks',
                           "text":"Please Approve Project Rebaseline Request",
                           "message":"Project Rebaseline Request Approval is pending from your below level persons",
                           "cc":ccEmployeeNotification,
                           'urlData':'projectRebaselineDetails ',
                           "type":"escalate"
                         }, function (err, notificationDetails) {
                           //console.log('notiifcation created '+notificationDetails);
                         })
                       });
                     }else{

                       DhanbadEmail.create({
                         "to": toEmployeeEmails.toString(),
                         "subject":  'Pending Tasks',
                         "text":"Please Approve Project Rebaseline Request",
                         "cc":ccEmployeeEmails.toString()
                       }, function (err, email) {
                         console.log(email);
                       });

                       Notification.create({
                         'to':toEmployeeNotification,
                         "subject":  'Pending Tasks',
                         "text":"Please Approve Project Rebaseline Request",
                         "message":"Project Rebaseline Request Approval is pending from your below level persons",
                         "cc":ccEmployeeNotification,
                         'urlData':'projectRebaselineDetails',
                         "type":"escalate"
                       }, function (err, notificationDetails) {
                         //console.log('notiifcation created '+notificationDetails);
                       })
                     }
                   });
                 }
               }
             }
           }

         });

       }
      }
    })

  });

};
