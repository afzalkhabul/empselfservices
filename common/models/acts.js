var server = require('../../server/server');
module.exports = function(Acts) {
  Acts.validatesUniquenessOf('actNumber', {message: 'Act Number could be unique'});
  Acts.observe('before save', function (ctx, next) {
    if (ctx.instance != undefined && ctx.instance != null) {
      ctx.instance.actNumber=(ctx.instance.actNumber.toLowerCase());
      ctx.instance.createdTime = new Date();
      next();
    } else {
      if(ctx.data.actNumber!=undefined && ctx.data.actNumber!=null && ctx.data.actNumber!=''){
        ctx.data.actNumber=(ctx.data.actNumber.toLowerCase());
      }
      ctx.data.updatedTime = new Date();
      next();
    }
  });
};
