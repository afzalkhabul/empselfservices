module.exports = function(AssetLifeCyclePlan) {
  AssetLifeCyclePlan.validatesUniquenessOf('assetPhase', {message: 'Asset Phase could be unique'});
  AssetLifeCyclePlan.observe('before save', function (ctx, next) {
    if (ctx.instance != undefined && ctx.instance != null) {
              ctx.instance.assetPhase=(ctx.instance.assetPhase.toLowerCase());
              ctx.instance.createdTime = new Date();
              next();
            } else {
              if(ctx.data.assetPhase!=undefined && ctx.data.assetPhase!=null && ctx.data.assetPhase!=''){
                ctx.data.assetPhase=(ctx.data.assetPhase.toLowerCase());
              }
              ctx.data.updatedTime = new Date();
              next();
            }
  });
};
