var server = require('../../server/server');
module.exports = function(Projectmeasurement) {

  Projectmeasurement.observe('before save', function (ctx, next) {
    if(ctx.instance){
      checkMeasurementId(ctx,next);
    }else{
      next();
    }
  });

  function checkMeasurementId(ctx,next){
    var randomstring = require("randomstring");
    var uniqueId = randomstring.generate({
      length: 7,
      charset: 'alphanumeric'
    });
    var date=new Date();
    var month;
    var dateIS;
    if((date.getMonth()+1)<10){month='0'+(date.getMonth()+1);}else{month=(date.getMonth()+1);}
    if(date.getDate()<10){dateIS='0'+date.getDate();}else{dateIS=date.getDate();}
    var string=date.getFullYear()+''+month+dateIS;
    uniqueId=string+uniqueId;
    uniqueId=uniqueId.toUpperCase();
    var notification=server.models.notification;
    Projectmeasurement.find({'where':{'measurementId':uniqueId}},function(err, billData){
      if(billData!=null && billData.length==0){
        ctx.instance.measurementId=uniqueId;
        ctx.instance.createdTime=new Date();
        var reviewPersonList=[{'employeeId':ctx.instance.contractorName,'readStatus':false}];
        if(reviewPersonList.length>0){
          notification.create({
            'to':reviewPersonList,
            "subject":  'New Tasks',
            "text":"Your Measurement details is created successfully",
            "message":"Your Measurement details  is created successfully",
            'urlData':'measurementBook',
            "type":"newRequest"
          });
        }
        next();
      }else{
        checkMeasurementId(ctx,next);
      }
    });
  }


  Projectmeasurement.observe('loaded', function (ctx, next) {
    if(ctx.instance){
      if(ctx.instance.contractorName!=undefined && ctx.instance.contractorName!=null){
        var Employee=server.models.Employee;
        Employee.find({'where':{'employeeId':ctx.instance.contractorName}},function(err, employee){
          if(employee.length>0){
            ctx.instance.reviewPersonName=employee[0].name;
            next();
          }else{
            next();
          }
        });
      } else{
        next();
      }
    }else{
      next();
    }
  });

};
