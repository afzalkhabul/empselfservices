var server = require('../../server/server');
module.exports = function(Projectrebaseline) {
  Projectrebaseline.observe('before save', function (ctx, next) {

    var WorkflowForm=server.models.WorkflowForm;
    var Workflow=server.models.Workflow;
    var WorkflowEmployees=server.models.WorkflowEmployees;
    var notification=server.models.notification;
    if(ctx.instance){

      WorkflowForm.findOne({"where": {"and":[{"status": "Active"},{"schemeUniqueId": "projectRebaseLine"}]}}, function (err, workflowForm) {

        if(workflowForm!=undefined && workflowForm!=null ){
          Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
            WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
              var workflowData = [];
              var employeeDetailsForNotification=[];
              if (listData != undefined && listData != null && listData.length > 0) {
                for (var i = 0; i < listData.length; i++) {
                  var flowData = {
                    workflowId: listData[i].workflowId,
                    levelId: listData[i].levelId,
                    status: listData[i].status,
                    maxLevels: listData[i].maxLevels,
                    levelNo: listData[i].levelNo
                  }
                  workflowData.push(flowData);
                  if(listData[i].levelNo=="1"){
                    if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                      for(var j=0;j<listData[i].employees.length;j++){
                        employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                      }
                    }
                  }
                }
              }
              ctx.instance.workflow = workflowData;
              ctx.instance.workflowId=workflowForm.workflowId;
              ctx.instance.maxlevel=flowDataList.maxLevel;
              ctx.instance.acceptLevel=0;
              ctx.instance.finalStatus=false;
              ctx.instance.createdTime=new Date();
              ctx.instance.lastUpdateTime=new Date();
              if(employeeDetailsForNotification.length>0){
                notification.create({
                  'to':employeeDetailsForNotification,
                  "subject":  'New Tasks',
                  "text":"You got New Project Rebaseline Request",
                  "message":"You got New Project Rebaseline Request",
                  'urlData':'projectRebaselineDetails',
                  "type":"newRequest"
                }, function (err, notificationDetails){
                  next();
                });
              }else{
                next();
              }
            });

          });
        }else{
          next();
        }
      });

    }else{
      next();
    }

  });
  Projectrebaseline.observe('loaded', function(ctx, next) {
    if (ctx.instance) {
      if (ctx.instance.workflowId != undefined && ctx.instance.workflowId != null && ctx.instance.workflowId != '') {
        var WorkflowEmployees = server.models.WorkflowEmployees;
        var ProjectRisks = server.models.ProjectRisks;
        var ProjectIssues = server.models.ProjectIssues;
        WorkflowEmployees.find({'where': {'and': [{'workflowId': ctx.instance.workflowId}]}}, function (err, workflowEmployeeList) {
          if (workflowEmployeeList != null && workflowEmployeeList.length > 0) {
            ctx.instance.workflowData = workflowEmployeeList;
            if(ctx.instance.risk!=undefined && ctx.instance.risk!=null && ctx.instance.risk.length>0 ){
              var riskList=[];
              for(var i=0;i<ctx.instance.risk.length;i++){
                riskList.push({'id':ctx.instance.risk[i]+''});
              }
              ProjectRisks.find({'where':{'or':riskList}}, function (err, riskDetails) {
                ctx.instance.risks=riskDetails;
                if(ctx.instance.issue!=undefined && ctx.instance.issue!=null && ctx.instance.issue.length>0 ){
                  var issueList=[];
                  for(var j=0;j<ctx.instance.issue.length;j++){
                    issueList.push({'id':ctx.instance.issue[j]+''});
                  }
                  ProjectIssues.find({'where':{'or':issueList}}, function (err, riskDetails) {
                    ctx.instance.issues=riskDetails;
                    next();
                  });
                }else{
                  next();
                }
              })

            }
            else{
              if(ctx.instance.issue!=undefined && ctx.instance.issue!=null && ctx.instance.issue.length>0 ){
                var issueList=[];
                for(var j=0;j<ctx.instance.issue.length;j++){
                  issueList.push({'id':ctx.instance.issue[j]+''});
                }
                ProjectIssues.find({'where':{'or':issueList}}, function (err, riskDetails) {
                  ctx.instance.issues=riskDetails;
                  next();
                });
              }else{
                next();
              }
            }

            //next();
          } else {
            if(ctx.instance.risk!=undefined && ctx.instance.risk!=null && ctx.instance.risk.length>0 ){
              var riskList=[];
              for(var i=0;i<ctx.instance.risk.length;i++){
                riskList.push({'id':ctx.instance.risk[i]+''});
              }
              ProjectRisks.find({'where':{'or':riskList}}, function (err, riskDetails) {
                ctx.instance.risks=riskDetails;
                if(ctx.instance.issue!=undefined && ctx.instance.issue!=null && ctx.instance.issue.length>0 ){
                  var issueList=[];
                  for(var j=0;j<ctx.instance.issue.length;j++){
                    issueList.push({'id':ctx.instance.issue[j]+''});
                  }
                  ProjectIssues.find({'where':{'or':issueList}}, function (err, riskDetails) {
                    ctx.instance.issues=riskDetails;
                    next();
                  });
                }else{
                  next();
                }
              })

            }
            else{
              if(ctx.instance.issue!=undefined && ctx.instance.issue!=null && ctx.instance.issue.length>0 ){
                var issueList=[];
                for(var j=0;j<ctx.instance.issue.length;j++){
                  issueList.push({'id':ctx.instance.issue[j]+''});
                }
                ProjectIssues.find({'where':{'or':issueList}}, function (err, riskDetails) {
                  ctx.instance.issues=riskDetails;
                  next();
                });
              }else{
                next();
              }
            }
          }
        })
      }else{
        if(ctx.instance.risk!=undefined && ctx.instance.risk!=null && ctx.instance.risk.length>0 ){
          var riskList=[];
          for(var i=0;i<ctx.instance.risk.length;i++){
            riskList.push({'id':ctx.instance.risk[i]+''});
          }
          ProjectRisks.find({'where':{'or':riskList}}, function (err, riskDetails) {
            ctx.instance.risks=riskDetails;
            if(ctx.instance.issue!=undefined && ctx.instance.issue!=null && ctx.instance.issue.length>0 ){
              var issueList=[];
              for(var j=0;j<ctx.instance.issue.length;j++){
                issueList.push({'id':ctx.instance.issue[j]+''});
              }
              ProjectIssues.find({'where':{'or':issueList}}, function (err, riskDetails) {
                ctx.instance.issues=riskDetails;
                next();
              });
            }else{
              next();
            }
          })

        }
        else{
          if(ctx.instance.issue!=undefined && ctx.instance.issue!=null && ctx.instance.issue.length>0 ){
            var issueList=[];
            for(var j=0;j<ctx.instance.issue.length;j++){
              issueList.push({'id':ctx.instance.issue[j]+''});
            }
            ProjectIssues.find({'where':{'or':issueList}}, function (err, riskDetails) {
              ctx.instance.issues=riskDetails;
              next();
            });
          }else{
            next();
          }
        }
      }

    }else{
      next();
    }
  });

  Projectrebaseline.getDetails = function (employee, cb) {
   Projectrebaseline.find({},function(err, fieldVisitData){
      var fieldVisitList=[];
      if(fieldVisitData!=null && fieldVisitData.length>0){

        var Employee=server.models.Employee;
        var adminStatus=false;
        Employee.find({'where':{"employeeId":employee}}, function (err, employeeList) {
          if(employeeList!=null && employeeList.length>0) {
            var firstEmployeeDetails = employeeList[0];
            if (firstEmployeeDetails.role == 'superAdmin' || firstEmployeeDetails.role == 'projectAdmin') {
              adminStatus = true;
            } else {
            }
            for (var i = 0; i < fieldVisitData.length; i++) {
              var data = fieldVisitData[i];
              var workflowDetails = data.workflowData;
              var accessFiledVisit = false;
              var approveFiledVisit = false;
              if (workflowDetails != undefined && workflowDetails != null && workflowDetails.length > 0) {
                var acceptLevel = data.acceptLevel;
                var editDetails;
                if (data.createdPersonId == employee) {
                  editDetails = true;
                }
                var availableStatus = false;
                if (data.finalStatus == false) {
                  for (var j = 0; j < workflowDetails.length; j++) {
                    var availableStatusLevel = false;
                    if (workflowDetails[j].employees != undefined && workflowDetails[j].employees != null && workflowDetails[j].employees.length > 0) {
                      var employeeList = workflowDetails[j].employees;
                      if (employeeList != undefined && employeeList != null && employeeList.length > 0) {
                        for (var x = 0; x < employeeList.length; x++) {
                          if (employeeList[x] == employee) {
                            availableStatus = true;
                            availableStatusLevel = true;
                            break;
                          }
                        }
                      }
                      if (availableStatusLevel) {
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);

                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                        }
                      }else if(adminStatus){
                        availableStatus=true;
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);

                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                        }
                      }
                    }

                  }
                  if (availableStatus || editDetails ) {
                    if (editDetails) {
                      if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                        data.editDetails = true;
                      } else {
                        data.editDetails = false;
                      }
                    }
                    data.approveFiledVisit = approveFiledVisit;
                    data.availableStatus = availableStatus;
                    fieldVisitList.push(data);
                  }
                }else{
                  if (editDetails) {
                    if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                      data.editDetails = true;
                    } else {
                      data.editDetails = false;
                    }
                  }
                  data.approveFiledVisit = false;
                  data.availableStatus = true;
                  fieldVisitList.push(data);
                }


              }
            }
            cb(null,fieldVisitList);
          }else{
            cb(null,fieldVisitList);
          }
        });

      }else{
        cb(null,fieldVisitList);
      }
    });
  };


  Projectrebaseline.remoteMethod('getDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'employeeId', type: 'string', http: {source: 'query'},required:true}],
    http: {
      path: '/getDetails',
      verb: 'GET'
    }
  });


  Projectrebaseline.updateDetails = function (approvalDetails, cb) {

    Projectrebaseline.findOne({'where':{'id':approvalDetails.requestId}},function(err, fieldVisitDetails){

      if(fieldVisitDetails!=undefined && fieldVisitDetails!=null){
        var finalObject={};
        var workflowDetails=fieldVisitDetails.workflow;
        if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
          var workflowData=[];
          for(var  i=0;i< workflowDetails.length;i++){
            var data=workflowDetails[i];

            var workFlowLevel=parseInt(workflowDetails[i].levelNo);
            if((approvalDetails.acceptLevel+1)==workFlowLevel){
              data.approvalStatus=approvalDetails.requestStatus;
              data.employeeName=approvalDetails.employeeName;
              data.employeeId=approvalDetails.employeeId;
              data.comment=approvalDetails.comment;
              data.date=new Date();
            }
            workflowData.push(data);

          }
          finalObject.workflow=workflowData;
          finalObject.lastUpdateTime=new Date();
        }
        if(approvalDetails.requestStatus=="Approval"){
          if(fieldVisitDetails.maxlevel==(approvalDetails.acceptLevel+1)){
            finalObject.finalStatus=approvalDetails.requestStatus;
            finalObject.acceptLevel=approvalDetails.acceptLevel+1;

          }else{
            finalObject.acceptLevel=approvalDetails.acceptLevel+1;
          }

        } else if(approvalDetails.requestStatus=="Rejected"){
          finalObject.finalStatus=approvalDetails.requestStatus;
          finalObject.acceptLevel=approvalDetails.acceptLevel+1;
          finalObject.rejectComment=approvalDetails.comment;


        }
        var Sms = server.models.Sms;
        fieldVisitDetails.updateAttributes(finalObject, function(err, finalData){
          if(err){
            console.log('error message'+err);
          }

          if(finalObject.finalStatus!=undefined && finalObject.finalStatus!=null && finalObject.finalStatus=="Rejected" ){
            if(finalData.createdPersonId!=undefined && finalData.createdPersonId!=null && finalData.createdPersonId!='' ){
              var Employee=server.models.Employee;
              var EmailTemplete = server.models.EmailTemplete;
              Employee.find({'where':{'employeeId':finalData.createdPersonId}}, function (err, employeeDetails) {
                if(employeeDetails!=null && employeeDetails.length>0){

                  EmailTemplete.findOne({"where":{"emailType": "projectWardWorks"}},function (err, emailTemplete) {
                    if(emailTemplete!=null ) {
                      var message = emailTemplete.rebaseLineRejectedMessage + '<br>Reason is ' + approvalDetails.comment;
                      var Dhanbademail = server.models.DhanbadEmail;
                      var notification=server.models.notification;
                      notification.create({
                        'to':[{'employeeId':employeeDetails[0].employeeId,'readStatus':false}],
                        "subject":  'New Tasks',
                        "text":"Your project  Rebaseline Request is Rejected",
                        "message":"Your project rebaselin Request is Rejected",
                        'urlData':'projectRebaselineDetails',
                        "type":"approvedRequest"
                      }, function (err, notificationDetails){
                      });
                      Dhanbademail.create({
                        "to": employeeDetails[0].emailId,
                        "subject":  emailTemplete.rebaseLineRejectedEmail,
                        "text":message
                      }, function (err, email) {

                      });
                      if(emailTemplete.rebaseLineRejectedSMS && employeeDetails[0].mobile) {
                        var smsData = {
                          "message": emailTemplete.rebaseLineRejectedSMS,
                          "mobileNo": employeeDetails[0].mobile,
                          "smsservicetype": "singlemsg"
                        };

                        Sms.create(smsData, function (err, smsInfo) {

                        });
                      }
                    }
                  });
                  cb(null, finalData);
                }else{
                  cb(null, finalData);
                }
              });

            }else{
              cb(null, finalData);
            }

          }else if(finalObject.finalStatus!=undefined && finalObject.finalStatus!=null && finalObject.finalStatus=="Approval"){

            if(finalData.createdPersonId!=undefined && finalData.createdPersonId!=null && finalData.createdPersonId!='' ){
              var Employee=server.models.Employee;
              var EmailTemplete = server.models.EmailTemplete;
              Employee.find({'where':{'employeeId':finalData.createdPersonId}}, function (err, employeeDetails) {
                if(employeeDetails!=null && employeeDetails.length>0){
                  EmailTemplete.findOne({"where":{"emailType": "projectWardWorks"}},function (err, emailTemplete) {
                    if(emailTemplete!=null) {
                      var message = emailTemplete.rebaseLineRequestApprovalMessage + '<br>Reason is ' + approvalDetails.comment;
                      var Dhanbademail = server.models.DhanbadEmail;
                      var notification=server.models.notification;
                      notification.create({
                        'to':[{'employeeId':employeeDetails[0].employeeId,'readStatus':false}],
                        "subject":  'New Tasks',
                        "text":"Your project Rebaseline Request is approved",
                        "message":"Your project Rebaseline Request is approved",
                        'urlData':'projectRebaselineDetails',
                        "type":"approvedRequest"
                      }, function (err, notificationDetails){

                      });
                      Dhanbademail.create({
                        "to": employeeDetails[0].emailId,
                        "subject":  emailTemplete.rebaseLineRequestApprovalEmail,
                        "text":message
                      }, function (err, email) {

                      });
                      if(emailTemplete.rebaseLineRequestApprovalSMS && employeeDetails[0].mobile) {
                        var smsData = {
                          "message": emailTemplete.rebaseLineRequestApprovalSMS,
                          "mobileNo": employeeDetails[0].mobile,
                          "smsservicetype": "singlemsg"
                        };

                        Sms.create(smsData, function (err, smsInfo) {
                        });
                      }
                    }
                  });
                  rebaseLine(finalData.planId,cb);
                }else{
                  rebaseLine(finalData.planId,cb);
                }
              });
            }
          }else{

            if(finalData.workflowId!=undefined && finalData.workflowId!=null && finalData.workflowId!=''){
              var WorkflowEmployees=server.models.WorkflowEmployees;
              var notification=server.models.notification;
              WorkflowEmployees.find({'where':{'workflowId':finalData.workflowId}},function (err, listData) {
                var workflowData = [];
                var employeeDetailsForNotification=[];
                if (listData != undefined && listData != null && listData.length > 0) {
                  for (var i = 0; i < listData.length; i++) {
                    if(parseInt(listData[i].levelNo)==(finalData.acceptLevel+1)){
                      if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                        for(var j=0;j<listData[i].employees.length;j++){
                          employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                        }
                      }
                    }
                  }
                }
                if(employeeDetailsForNotification.length>0){
                  notification.create({
                    'to':employeeDetailsForNotification,
                    "subject":  'New Tasks',
                    "text":"You got New Project Rebaseline Request",
                    "message":"You got New Project Rebaseline Request",
                    'urlData':'projectRebaselineDetails',
                    "type":"newRequest"
                  }, function (err, notificationDetails){
                    cb(null, finalData);
                  });

                }else{
                  cb(null, finalData);
                }
              });
            }
            else{
              cb(null, finalData);
            }
          }
        });


      }else {
        var message = 'Please provide valid details';
        cb(message,null);
      }

    });


  };


  function rebaseLine(planId, cb) {

    var ProjectTasks = server.models.ProjectTasks;
    var TaskStatus=server.models.TaskStatus;
    var ProjectCharter=server.models.ProjectCharter;
    var ProjectPlan=server.models.ProjectPlan;
    var details=planId;
    var planDetails={
      'planDetails':[],
      'taskDetails':[],
      'comments':[],
      'filedVisit':[],
      'uploads':[],
      'paymentMilestones':[],
      'projectRisks':[],
      'projectCharter':[]
    };
    ProjectCharter.findOne({'where':{'projectId':details.planId}},function (err, projectcharter) {
      if(err)
        console.log('Error message is'+err);
      var charter=projectcharter;
      var object={
        "status":"reBaseline"
      };

      TaskStatus.findOne({where:{name:object.status}}, function (err, statusInfo) {
        if (err) {
        } else {
          projectcharter.updateAttributes({statusId:statusInfo.id},function (err, updatedcharter) {
          });
        }
      });

      var charterDetails={
        'name':charter.name,
        'description':charter.description,
        'createdPerson':charter.createdPerson,
        'status':'new',
        'charterUniqueId':charter.charterUniqueId
      };

      var projectPlandetails;
      var projectTaskDetails;
      ProjectPlan.findOne({'where':{'planId':details.planId}},function (err, planDetailsList) {
        if(err)
          console.log('Error message is'+err);

        if(planDetailsList!=undefined && planDetailsList!=null){
          projectPlandetails=planDetailsList;
        }
        var object={
          "taskStatus":"reBaseline"
        };

        TaskStatus.findOne({where:{name:object.taskStatus}}, function (err, statusInfo) {
          if (err) {

          } else {
            planDetailsList.updateAttributes({taskStatusId:statusInfo.id},function (err, updatedProjectPlan) {

            });
          }
        });

      });
      ProjectTasks.find({'where':{'planId':details.planId}},function (err, taskDetailsList) {
        if(err)
          console.log('Error message is'+err);
        if(taskDetailsList!=undefined && taskDetailsList!=null && taskDetailsList.length>0){
          projectTaskDetails=taskDetailsList;

          var taskList=taskDetailsList;
          for(var i=0;i<taskList.length;i++){
            var projecTask=taskList[i];
            var taskList=[];
            if(projecTask.taskList!=null && projecTask.taskList.length>0){
              for(var j=0;j<projecTask.taskList.length;j++){
                var subTask=projecTask.taskList[j];

                if(subTask.status =='Completed'){
                  taskList.push(subTask);
                }else{
                  subTask.status="ReBaseline"
                  taskList.push(subTask);
                }
              }
            }
            var object={
              "taskList":taskList
            }
            projecTask.updateAttributes(object,function (err, updatedProjectTasks) {

            });
          }





        }


      });


      TaskStatus.findOne({where:{name:charterDetails.status}}, function (err, statusInfo) {
        if (err) {

        } else {
          charterDetails['statusId'] = statusInfo.id;
          delete charterDetails['status'];
          ProjectCharter.create(charterDetails,function (err, createdCharter) {


            function getFormattedDate(date) {
              var year = date.getFullYear();
              /// Add 1 because JavaScript months start at 0
              var month = (1 + date.getMonth()).toString();
              month = month.length > 1 ? month : '0' + month;
              var day = date.getDate().toString();
              day = day.length > 1 ? day : '0' + day;
              return month + '/' + day + '/' + year;
            }
            var date=getFormattedDate(new Date());

            if(projectPlandetails!=null){
              var createDataForPlan={
                "planId":createdCharter.projectId,
                "name":projectPlandetails.name,
                "description":projectPlandetails.description,
                "department": projectPlandetails.department,
                "applicantId": projectPlandetails.applicantId,
                "startDate": date,
                "taskStatusId": statusInfo.id,
                "palnStatus": "new",
                "createdPerson":projectPlandetails.createdPerson,
              };
              ProjectPlan.create(createDataForPlan,function (err, createPlan) {

              });

            }
            if(projectTaskDetails !=null && projectTaskDetails .length>0){
              for(var i=0;i<projectTaskDetails.length;i++){
                var projecTask=projectTaskDetails[i];
                var taskList=[];
                if(projecTask.taskList!=null && projecTask.taskList.length>0){
                  for(var j=0;j<projecTask.taskList.length;j++){
                    var subTask=projecTask.taskList[j];

                    if(subTask.status !='Completed'){
                      var task={
                        "subTaskName":subTask.subTaskName ,
                        "subTaskComment": subTask.subTaskComment ,
                        "startDate": date,
                        "completePercenatge": "0",
                        "status": "new",
                        "utilizedCost": "0"
                      };
                      taskList.push(task);
                    }
                  }
                }
                var createPlanData={
                  "name": projecTask.name,
                  "headerId": projecTask.headerId,
                  "planId": createdCharter.projectId,
                  "planName": projectPlandetails.name,
                  "createdPerson": projecTask.createdPerson,
                  "taskList":taskList
                };
                ProjectTasks.create(createPlanData,function (err, createdprojectTask) {
                });
              }

            }
            cb(null,'success');
          });
        }
      });



    });


  };



  Projectrebaseline.remoteMethod('updateDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateDetails',
      verb: 'POST'
    }
  });

  Projectrebaseline.updateContent= function (updatedDetails, cb) {
    Projectrebaseline.findOne({'where':{'id':updatedDetails.id}},function(err, fieldVisitDetails){
      var WorkflowForm=server.models.WorkflowForm;
      var Workflow=server.models.Workflow;
      var WorkflowEmployees=server.models.WorkflowEmployees;
      var notification=server.models.notification;
      WorkflowForm.findOne({"where": {"and":[{"status": "Active"},{"schemeUniqueId": "fieldVisit"}]}}, function (err, workflowForm) {
        if(workflowForm!=undefined && workflowForm!=null ){
          Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
            WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
              var workflowData = [];
              var employeeDetailsForNotification=[];
              if (listData != undefined && listData != null && listData.length > 0) {
                for (var i = 0; i < listData.length; i++) {
                  var flowData = {
                    workflowId: listData[i].workflowId,
                    levelId: listData[i].levelId,
                    status: listData[i].status,
                    maxLevels: listData[i].maxLevels,
                    levelNo: listData[i].levelNo

                  }
                  workflowData.push(flowData);
                  if(listData[i].levelNo=="1"){
                    if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                      for(var j=0;j<listData[i].employees.length;j++){
                        employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                      }
                    }
                  }
                }
              }
              updatedDetails.workflow = workflowData;
              updatedDetails.workflowId=workflowForm.workflowId;
              updatedDetails.maxlevel=flowDataList.maxLevel;
              updatedDetails.acceptLevel=0;
              updatedDetails.finalStatus=false;
              updatedDetails.lastUpdateTime=new Date();
              updatedDetails['updatedTime']=new Date();
              notification.create({
                'to':employeeDetailsForNotification,
                "subject":  'New Tasks',
                "text":"You got edit Project Rebaseline Request",
                "message":"You got edit Project Rebaseline Request",
                'urlData':'projectRebaselineDetails',
                "type":"newRequest"
              }, function (err, notificationDetails){

              });
              fieldVisitDetails.updateAttributes(updatedDetails,function(err,data){
              });
              cb(null, fieldVisitDetails);
            });

          });
        }
      });
    });
  };

  Projectrebaseline.remoteMethod('updateContent', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateContent',
      verb: 'POST'
    }
  });
  Projectrebaseline.observe('after save', function (ctx, next) {

 next();

  });


};
