var server = require('../../server/server');
module.exports = function(Scheme) {
  Scheme.validatesUniquenessOf('name', {message: 'Scheme Name could be unique'});


  Scheme.observe('before save', function (ctx, next) {
    var Beneficiaries = server.models.beneficiariesList;
    if(ctx.data!=undefined && ctx.data!=null){
      if(ctx.data.name!=undefined && ctx.data.name!=null && ctx.data.name!=''){
        ctx.data.name=(ctx.data.name.toLowerCase());
      }
      ctx.data['updatedTime']=new Date();
      if(ctx.data.bineficiaries.length > 0){
        var beneficiariesInfo = [];
        function updateBeneficiary(i){
          Beneficiaries.findOne({"where":{"name": ctx.data.bineficiaries[i]}},function(err, beneficiaryData){
            if(err){
              next(err, null);
            }else{
              beneficiariesInfo.push(beneficiaryData.id);
              if(i+1 == ctx.data.bineficiaries.length){
                ctx.data.bineficiaries = [];
                ctx.data.bineficiaries = beneficiariesInfo;
                next();
              }else{
                updateBeneficiary(i+1);
              }
            }
          });
        }
        updateBeneficiary(0);
      }else {
        next();
      }
    }else {
      ctx.instance.createdTime=new Date();
      checkUniqueId(ctx,next);
    }
  });
  function checkUniqueId(ctx,next){
    var randomstring = require("randomstring");
    var uniqueId = randomstring.generate({
      length: 7,
      charset: 'alphanumeric'
    });
    var date=new Date();
    var month;
    var dateIS;
    if((date.getMonth()+1)<10){
      month='0'+(date.getMonth()+1);
    }else{
      month=(date.getMonth()+1);
    }
    if(date.getDate()<10){
      dateIS='0'+date.getDate();
    }else{
      dateIS=date.getDate();
    }
    var string=date.getFullYear()+''+month+dateIS;
    uniqueId=string+uniqueId;
    Scheme.find({"where": {"schemeId": uniqueId}}, function (err, customers) {
      if(customers.length==0){
        ctx.instance.name=(ctx.instance.name.toLowerCase());
        ctx.instance.createdTime = new Date();
        ctx.instance['schemeUniqueId'] = uniqueId;
        next();
      }else{
        checkUniqueId(ctx,next);
      }
    });
  }

  Scheme.observe('loaded', function(ctx, next) {
    if(ctx.instance) {
      var contactOneEmployeeId;
      var Employee = server.models.Employee;
      var Department = server.models.Department;
      var Ministeries = server.models.ministeriesList;
      var Beneficiaries = server.models.beneficiariesList;

	  if(ctx.instance.contactOne){
	  if(ctx.instance.contactOne.employeeId){
		  contactOneEmployeeId = ctx.instance.contactOne.employeeId;
	  }
	  }
      var contactTwoEmployee;
      var contactThreeEmployee;
	  if(ctx.instance.contactTwo){
	  if(ctx.instance.contactTwo.employeeId){
		  contactTwoEmployee = ctx.instance.contactTwo.employeeId;
	  }
	  }
	  if(ctx.instance.contactThree){
	  if(ctx.instance.contactThree.employeeId){
		  contactThreeEmployee = ctx.instance.contactThree.employeeId;
	  }
	  }
      if(contactOneEmployeeId!=undefined && contactOneEmployeeId!=null){
        Employee.findOne({'where':{'employeeId':contactOneEmployeeId}},function (err, details) {
          if(details!=null){
            ctx.instance.contactOne['name']=details.name;
            ctx.instance.contactOne['email']=details.email;
            ctx.instance.contactOne['designation']=details.designation;
            ctx.instance.contactOne['phoneOne']=details.mobile;
            ctx.instance.contactOne['phoneTwo']=details.workMobile;
          }
        }) ;
      }

      if(contactTwoEmployee!=undefined && contactTwoEmployee!=null){
        Employee.findOne({'where':{'employeeId':contactTwoEmployee}},function (err, details) {
          if(details!=null){
            ctx.instance.contactTwo.name=details.name;
            ctx.instance.contactTwo.email=details.email;
            ctx.instance.contactTwo.designation=details.designation;
            ctx.instance.contactTwo.phoneOne=details.mobile;
            ctx.instance.contactTwo.phoneTwo=details.workMobile;
          }

        });
      }

      if(contactThreeEmployee!=undefined && contactThreeEmployee!=null) {
        Employee.findOne({'where': {'employeeId': contactThreeEmployee}}, function (err, details) {
          if (details != null) {
            ctx.instance.contactThree.name = details.name;
            ctx.instance.contactThree.email = details.email;
            ctx.instance.contactThree.designation = details.designation;
            ctx.instance.contactThree.phoneOne = details.mobile;
            ctx.instance.contactThree.phoneTwo = details.workMobile;
          }

        });
      }

      Department.findById(ctx.instance.departmentId,function(err,dept){
        if(err){
          next(err,null);
        }else{
          if(dept){
            ctx.instance['departmentName'] = dept.name;
          }
          Ministeries.findById(ctx.instance.ministryId, function(err, ministry){
            if(err){
              next(err,null);
            }else {
              if(ministry) {
                ctx.instance['ministryName'] = ministry.name;
              }
              var benificiariesArray = [];
              if (ctx.instance.bineficiaries.length > 0) {
                function benificiaryList(j) {
                  Beneficiaries.findById(ctx.instance.bineficiaries[j], function(err, beneficiaryInfo){
                    if (err) {
                      next(err, null);
                    } else {
                      if(beneficiaryInfo) {
                        benificiariesArray.push({"id": beneficiaryInfo.id, 'label': beneficiaryInfo.name});
                      }
                      j = j + 1;
                      if (j == ctx.instance.bineficiaries.length) {
                        ctx.instance.bineficiaries = [];
                        for (var i = 0; i < benificiariesArray.length; i++) {
                          ctx.instance.bineficiaries.push(benificiariesArray[i]);
                        }
                        next();
                      } else {
                        benificiaryList(j);
                      }
                    }
                  });
                }

                benificiaryList(0);
              }else{
                next();
              }
            }

          });
        }
      });
    }
    else{
      next();
    }

  });

  Scheme.getSchemeReport = function (filterData, cb) {

      var filter=[];

    if(filterData.deptId!=undefined && filterData.deptId!=null && filterData.deptId!='' && filterData.deptId!='all'){
      filter.push({'departmentId':filterData.deptId});
    }
      if(filterData.minstryId!=undefined && filterData.minstryId!=null && filterData.minstryId!=''  && filterData.minstryId!='all'){
        filter.push({'ministryId':filterData.minstryId});
    }
      if(filterData.category!=undefined && filterData.category!=null && filterData.category!='' && filterData.category!='all'){
        filter.push({'category':filterData.category});
    }
      if(filterData.status!=undefined && filterData.status!=null && filterData.status!='' && filterData.status!='all'){
        filter.push({'status':filterData.status});
    }
      console.log('filter data'+JSON.stringify(filter));
    if(filter.length>0){
      Scheme.find({'where':{'and':filter}}, function (err, schemeList) {
        console.log('scheme length'+schemeList.length);
        cb(null,schemeList);
      });
    }else{
      Scheme.find({}, function (err, schemeList) {
        console.log('scheme length'+schemeList.length);
        cb(null,schemeList);
      });
    }





   };

  Scheme.remoteMethod('getSchemeReport', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'filter', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/getSchemeReport',
      verb: 'POST'
    }
  });

  Scheme.getReport = function (dates, cb) {
    var Request = server.models.Request;
    if(dates.duration!=undefined && dates.duration!=null ){
       var startDate;
       var endDate;

      var todayDate=new Date();

        if(dates.duration=='1'){
          startDate=new Date(new Date() - (7 * 24 * 60 * 60 * 1000));
        } else  if(dates.duration=='2'){
          startDate=new Date(new Date() - (30 * 24 * 60 * 60 * 1000));
        }else{
          startDate=new Date(new Date() - (60 * 24 * 60 * 60 * 1000));
        }
      Scheme.find({}, function (err,schemeLength) {
        var finalObject=[];
        if(schemeLength!=undefined && schemeLength.length>0){
          var x = 0;
          var falseCount=0
          var approvedCount=0
          var rejectedCount=0
          var totalCount=0
          var count=0;
          for (var x=0;x < schemeLength.length;x++) {

            var queryData=[{'schemeUniqueId':schemeLength[x].schemeUniqueId},{'createdTime': {'gt': startDate}},{"finalStatus" : false}]

            Request.count({'and':queryData}, function (err, reports) {
              console.log('first one'+JSON.stringify(reports));
              falseCount=reports;
              //cb(null,{'details':falseCount});
            });
            queryData=[{'schemeUniqueId':schemeLength[x].schemeUniqueId},{'createdTime': {'gt': startDate}},{"finalStatus" : 'Approved'}];
            Request.count({'and':queryData}, function (err, approved) {
              console.log('Secound one'+JSON.stringify(approved));
              approvedCount=approved;

            });
            queryData=[{'schemeUniqueId':schemeLength[x].schemeUniqueId},{'createdTime': {'gt': startDate}},{"finalStatus" : 'Rejected'}]
            Request.count({'and':queryData}, function (err, reject) {
              rejectedCount=reject;
              console.log('thrid one'+JSON.stringify(reject));
              totalCount=falseCount+approvedCount+rejectedCount;
              var data={
                'schmeDetails':schemeLength[count],
                'falseCount':falseCount,
                'approvedCount':approvedCount,
                'rejectCount':rejectedCount,
                'totalCount':totalCount
              }
              finalObject.push(data);
              count++;
              if(schemeLength.length==count){
                cb(null,finalObject);
              }
            });
          }

        }
      });

    }else if(dates.startDate!=undefined && dates.startDate!=null && dates.endDate!=undefined && dates.endDate!=null ){
        startDate=new  Date(dates.startDate);
        endDate=new  Date(dates.endDate);
      Scheme.find({}, function (err,schemeLength) {
        var finalObject=[];
        if(schemeLength!=undefined && schemeLength.length>0){
          var x = 0;
          var falseCount=0
          var approvedCount=0
          var rejectedCount=0
          var totalCount=0
          var count=0;
          for (var x=0;x < schemeLength.length;x++) {

            //console.log('qureyData'+schemeLength[x].schemeUniqueId);
            var queryData=[{'schemeUniqueId':schemeLength[x].schemeUniqueId},{'createdTime': {'gt': startDate}},{'createdTime': {'lt': endDate}},{"finalStatus" : false}]

            Request.count({'and':queryData}, function (err, reports) {
              console.log('first one'+JSON.stringify(reports));
              falseCount=reports;
              //cb(null,{'details':falseCount});
            });
            queryData=[{'schemeUniqueId':schemeLength[x].schemeUniqueId},{'createdTime': {'gt': startDate}},{'createdTime': {'lt': endDate}},{"finalStatus" : 'Approved'}];
            Request.count({'and':queryData}, function (err, approved) {
              console.log('Secound one'+JSON.stringify(approved));
              approvedCount=approved;

            });
            queryData=[{'schemeUniqueId':schemeLength[x].schemeUniqueId},{'createdTime': {'gt': startDate}},{'createdTime': {'lt': endDate}},{"finalStatus" : 'Rejected'}]
            Request.count({'and':queryData}, function (err, reject) {
              rejectedCount=reject;
              console.log('thrid one'+JSON.stringify(reject));
              totalCount=falseCount+approvedCount+rejectedCount;
              var data={
                'schmeDetails':schemeLength[count],
                'falseCount':falseCount,
                'approvedCount':approvedCount,
                'rejectCount':rejectedCount,
                'totalCount':totalCount
              }
              finalObject.push(data);
              count++;
              if(schemeLength.length==count){
                cb(null,finalObject);
              }
            });
          }

        }
      });
    }else{
      var error = new Error('Please provide valid details');
      error.statusCode = 200;
      cb(null,error);
    }
    //startDate=new Date(new Date().getTime() - (7 * 24 * 60 * 60 * 1000));





   };

  Scheme.remoteMethod('getReport', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'filter', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/getReport',
      verb: 'POST'
    }
  });

 Scheme.getPendingReq = function ( cb) {
   var Request = server.models.Request;
   Scheme.find({}, function (err, schemeLength) {
     var finalObject = [];
     if (schemeLength != undefined && schemeLength.length > 0) {
       var x = 0;
       var tenDaysCount = 0
       var oneMonthCount = 0
       var threeMonthCount = 0
       //var totalCount = 0
       var count = 0;
       var startDate;
       var endDate;
       for (var x = 0; x < schemeLength.length; x++) {

         //console.log('qureyData'+schemeLength[x].schemeUniqueId);
         startDate=new Date(new Date().getTime() - (10 * 24 * 60 * 60 * 1000));
         var queryData = [{'schemeUniqueId': schemeLength[x].schemeUniqueId}, {'createdTime': {'gt': startDate}}, {"finalStatus": false}]

         Request.count({'and': queryData}, function (err, reports) {
           console.log('first one' + JSON.stringify(reports));
           tenDaysCount = reports;
           //cb(null,{'details':falseCount});
         });
         startDate=new Date(new Date().getTime() - (30 * 24 * 60 * 60 * 1000));
         endDate=new Date(new Date().getTime() - (10 * 24 * 60 * 60 * 1000));
         queryData = [{'schemeUniqueId': schemeLength[x].schemeUniqueId}, {'createdTime': {'gt': startDate}},{'createdTime': {'lt': endDate}}, {"finalStatus": false}];
         Request.count({'and': queryData}, function (err, approved) {
           console.log('Secound one' + JSON.stringify(approved));
           oneMonthCount = approved;

         });
         startDate=new Date(new Date().getTime() - (90 * 24 * 60 * 60 * 1000));
         endDate=new Date(new Date().getTime() - (30 * 24 * 60 * 60 * 1000));
         queryData = [{'schemeUniqueId': schemeLength[x].schemeUniqueId}, {'createdTime': {'gt': startDate}},{'createdTime': {'lt': endDate}}, {"finalStatus": false}]
         Request.count({'and': queryData}, function (err, reject) {
           threeMonthCount = reject;
           console.log('thrid one' + JSON.stringify(reject));
           //totalCount = falseCount + approvedCount + rejectedCount;
           var data = {
             'schmeDetails': schemeLength[count],
             'tenDaysCount': tenDaysCount,
             'oneMonthCount': oneMonthCount,
             'threeMonthCount': threeMonthCount
           }
           finalObject.push(data);
           count++;
           if (schemeLength.length == count) {
             cb(null, finalObject);
           }
         });
       }

     }else{
       cb(null,[]);
     }
   });

   };

  Scheme.remoteMethod('getPendingReq', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [],
    http: {
      path: '/getPendingReq',
      verb: 'GET'
    }
  });


Scheme.getEmpReport = function (input, cb) {
   var Request = server.models.Request;

      if(input!=undefined && input!=null && input.workflowId!=undefined &&  input.workflowId!=null && input.workflowId!=''){
        var qureyData;
       if(input.startDate!=undefined && input.startDate!=null && input.endDate!=undefined && input.endDate!=null){
        var startDate=new  Date(input.startDate);
         var endDate=new  Date(input.endDate);
         qureyData={'where':{'and':[{'workflowId':input.workflowId},{'createdTime': {'gt': startDate}},{'createdTime': {'lt': endDate}}]}};
       }else{
         qureyData={'where':{'workflowId':input.workflowId}}
       }
        var finalRequestList=[];
        if(input.workflowLevel!=undefined && input.workflowLevel!=null && input.workflowLevel!=''){
          Request.find(qureyData,function(err, reportsList){
            if(reportsList!=undefined && reportsList!=null && reportsList.length){
              for(var x=0;x<reportsList.length;x++){
                if(reportsList[x].workflow!=undefined && reportsList[x].workflow!=null && reportsList[x].workflow.length>0){
                  var report=reportsList[x].workflow;
                for(var work=0;work<report.length;work++){
                  var requestData;
                  if(report[work].levelId==input.workflowLevel){
                    if(input.employeeId!=undefined && input.employeeId!=null && input.employeeId!=''
                      && input.employeeId!='all' && report[work].levelId==input.employeeId){
                    requestData={
                        "emailId" : reportsList[x].emailId,
                        "name" :reportsList[x].name,
                        "mobileNumber" : reportsList[x].mobileNumber,
                        "schemeName" : reportsList[x].schemeName,
                        "schemeUniqueId" : reportsList[x].schemeUniqueId,
                        "finalStatus" : reportsList[x].finalStatus,
                        "requestId" : reportsList[x].requestId,
                        "approvedEmployee":report[work].employeeName,
                        "approvedStatus":report[work].requestStatus,
                        "approvedTime":report[work].createTime
                      }
                    }else if(report[work].levelId==input.employeeId){
                      requestData={
                        "emailId" : reportsList[x].emailId,
                        "name" :reportsList[x].name,
                        "mobileNumber" : reportsList[x].mobileNumber,
                        "schemeName" : reportsList[x].schemeName,
                        "schemeUniqueId" : reportsList[x].schemeUniqueId,
                        "finalStatus" : reportsList[x].finalStatus,
                        "requestId" : reportsList[x].requestId,
                        "approvedEmployee":report[work].employeeName,
                        "approvedStatus":report[work].requestStatus,
                        "approvedTime":report[work].createTime
                      }
                    }
                  }
                  finalRequestList.push(requestData);
                }
                  cb(null, finalRequestList);
                }else{
                  cb(null,[]);
                }

              }
            }else{
              cb(null,[]);
            }

          });
        }
        else{
          var error = new Error('Please Select Workflow Level');
          error.statusCode = 200;
          cb(null,error);
        }
      }else{

      }

 /*     Request.find({},function(err, report){

      });*/

   };

  Scheme.remoteMethod('getEmpReport', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'filter', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/getEmpReport',
      verb: 'POST'
    }
  });
  Scheme.remoteMethod('getEmployeeData', {
    description: "Send Valid Details",
    returns: {
      arg: 'data',
      type: "object"
    },
    accepts: [{arg: 'searchData', type: 'object', http: {source: 'query'}}],
    http: {
      path: '/getEmployeeData',
      verb: 'GET'
    }
  });

  Scheme.getEmployeeData = function (data, cb) {
    var Employee=server.models.Employee;
    var RoleEmployees=server.models.RoleEmployees;
    var AccessToken=server.models.AccessToken;
    var RoleModel=server.models.RoleModel;
    Employee.find({'where':{'employeeId':data.employeeId}},function (err, employeeDetails) {
      if(employeeDetails.length>0){
        var employeeData={
          'userId':employeeDetails[0].id,
          "ttl" : 1209600
        }
        var userInfo=employeeDetails[0];
        AccessToken.create(employeeData,function(err, accessTokenData){
          console.log('accesstokenData');
          accessTokenData['roleObject']={};
          accessTokenData['userInfo'] = userInfo;
          if(userInfo.role=='superAdmin'){
            addSchemeRole(accessTokenData,cb);
            addProjectRole(accessTokenData,cb);
            addLegalRole(accessTokenData,cb);
            addLandRole(accessTokenData,cb);
            addWorkflowRole(accessTokenData,cb);
          }
          else if(userInfo.role=='schemeAdmin'){
            addSchemeRole(accessTokenData,cb);
            addWorkflowRole(accessTokenData,cb);
          }
          else if(userInfo.role=='projectAdmin'){

            addProjectRole(accessTokenData,cb);
            addWorkflowRole(accessTokenData,cb);
          }
          else if(userInfo.role=='legalAdmin'){

            addLegalRole(accessTokenData,cb);
            addWorkflowRole(accessTokenData,cb);
          }
          else if(userInfo.role=='landAdmin'){

            addLandRole(accessTokenData,cb);
            addWorkflowRole(accessTokenData,cb);
          }
          else {
            RoleEmployees.find({"where":{"status": "Active"}}, function (err, roleModelList) {
              if(roleModelList!=null && roleModelList.length>0){
                var roleList=[];
                for(var i=0;i<roleModelList.length;i++){
                  if(roleModelList[i].formDetails!=undefined && roleModelList[i].formDetails!=null && roleModelList[i].formDetails.length>0){
                    for(var j=0;j<roleModelList[i].formDetails.length;j++){
                      if(roleModelList[i].formDetails[j].view!=undefined && roleModelList[i].formDetails[j].view){
                        if(roleModelList[i].formDetails[j].employeeId==accessTokenData.userInfo.employeeId){
                          var object={
                            'id':roleModelList[i].roleId
                          }
                          roleList.push(object);
                        }
                      }

                    }
                  }
                }
                RoleModel.find({'where':{'or':roleList}},function(err, roleFormDetails){
                  if(roleFormDetails!=null && roleFormDetails.length>0){
                    checkAccessDetailsDetails(accessTokenData,roleFormDetails,cb);
                  }else{
                    cb(null,accessTokenData);
                  }
                });
              }else{
                cb(null,accessTokenData);
              }
            });
          }
          //cb(null,accessTokenData);
        })

      }else{
        console.log('getting error here');
        var error = new Error('This employee is not availble for this Module');
        error.statusCode = 200;
        cb(error, null);
      }




    });
  };
  function addSchemeRole(ctx,cb){
    ctx.roleObject['configuration']=true;
    ctx.roleObject['ministriesAccess']=true;
    ctx.roleObject['ministriesview'] = true;
    ctx.roleObject['ministriesedit'] = true;

    ctx.roleObject['beneficiariesAccess'] = true;
    ctx.roleObject['beneficiariesview'] = true;
    ctx.roleObject['beneficiariesedit'] = true;

    ctx.roleObject['schemeDepartmentAccess'] = true;
    ctx.roleObject['schemeDepartmentview'] = true;
    ctx.roleObject['schemeDepartmentedit'] = true;

    ctx.roleObject['emailAccess']=true;
    ctx.roleObject['emailview']=true;
    ctx.roleObject['emailedit']=true;

    ctx.roleObject['scheme']=true;
    ctx.roleObject['schemeManagementAccess']=true;
    ctx.roleObject['schemeManagementview']=true;
    ctx.roleObject['schemeManagementedit']=true;

    ctx.roleObject['schemeRequestsAccess']=true;
    ctx.roleObject['schemeRequestsview']=true;
    ctx.roleObject['schemeRequestsedit']=true;



  }
  function addProjectRole(ctx,cb){

    ctx.roleObject['projectMaster']=true;
    ctx.roleObject['project']=true;
    ctx.roleObject['headOrSchemeAccess'] = true;
    ctx.roleObject['headOrSchemeview'] = true;
    ctx.roleObject['headOrSchemeedit'] = true;

    ctx.roleObject['ulbAccess'] = true;
    ctx.roleObject['ulbview'] = true;
    ctx.roleObject['ulbedit'] = true;

    ctx.roleObject['taskStatusAccess'] = true;
    ctx.roleObject['taskStatusview'] = true;
    ctx.roleObject['taskStatusedit'] = true;

    ctx.roleObject['planStateAccess'] = true;
    ctx.roleObject['planStateview'] = true;
    ctx.roleObject['planStateedit'] = true;

    ctx.roleObject['nocRequestsAccess'] = true;
    ctx.roleObject['nocRequestsview'] = true;
    ctx.roleObject['nocRequestsedit'] = true;

    ctx.roleObject['fieldReportsAccess'] = true;
    ctx.roleObject['fieldReportsview'] = true;
    ctx.roleObject['fieldReportsedit'] = true;

    ctx.roleObject['projectRequestsAccess'] = true;
    ctx.roleObject['projectRequestsview'] = true;
    ctx.roleObject['projectRequestsedit'] = true;

    ctx.roleObject['projectWardworksEmailTempleteAccess'] = true;
    ctx.roleObject['projectWardworksEmailTempleteview'] = true;
    ctx.roleObject['projectWardworksEmailTempleteedit'] = true;

    ctx.roleObject['documentTypeAccess'] = true;
    ctx.roleObject['documentTypeview'] = true;
    ctx.roleObject['documentTypeedit'] = true;


    ctx.roleObject['planDepartmentAccess'] = true;
    ctx.roleObject['planDepartmentview'] = true;
    ctx.roleObject['planDepartmentedit'] = true;


    ctx.roleObject['projectMgmt']=true;
    ctx.roleObject['projectClosureAccess']=true;
    ctx.roleObject['projectClosureview']=true;
    ctx.roleObject['projectClosureedit']=true;

    ctx.roleObject['projectRisksAccess']=true;
    ctx.roleObject['projectRisksview']=true;
    ctx.roleObject['projectRisksedit']=true;

    ctx.roleObject['projectDepartmentWiseBudgetAccess']=true;
    ctx.roleObject['projectDepartmentWiseBudgetview']=true;
    ctx.roleObject['projectDepartmentWiseBudgetedit']=true;

    ctx.roleObject['projectPaymentMilestonesAccess']=true;
    ctx.roleObject['projectPaymentMilestonesview']=true;
    ctx.roleObject['projectPaymentMilestonesedit']=true;

    ctx.roleObject['projectDeliverableAccess']=true;
    ctx.roleObject['projectDeliverableview']=true;
    ctx.roleObject['projectDeliverabledit']=true;

    ctx.roleObject['projectLayoutUploadAccess']=true;
    ctx.roleObject['projectLayoutUploadview']=true;
    ctx.roleObject['projectLayoutUploadedit']=true;

    ctx.roleObject['projectMilestonesAccess']=true;
    ctx.roleObject['pprojectMilestonesview']=true;
    ctx.roleObject['projectMilestonesedit']=true;

    ctx.roleObject['projectCommentsAccess']=true;
    ctx.roleObject['projectCommentsview']=true;
    ctx.roleObject['projectCommentsedit']=true;

    ctx.roleObject['projectUploadsAccess']=true;
    ctx.roleObject['projectUploadsview']=true;
    ctx.roleObject['projectUploadsedit']=true;


    ctx.roleObject['projectTransferFoundsAccess']=true;
    ctx.roleObject['projectTransferFoundsview']=true;
    ctx.roleObject['projectTransferFoundsedit']=true;

    ctx.roleObject['projectFieldVisitAccess']=true;
    ctx.roleObject['projectFieldVisitview']=true;
    ctx.roleObject['projectFieldVisitedit']=true;

    ctx.roleObject['projectBillGenerationAccess']=true;
    ctx.roleObject['projectBillGenerationview']=true;
    ctx.roleObject['projectBillGenerationdit']=true;

    ctx.roleObject['projectMeasurementAccessAccess']=true;
    ctx.roleObject['projectMeasurementAccessview']=true;
    ctx.roleObject['projectMeasurementAccessedit']=true;

    ctx.roleObject['projectPaymentAccessAccess']=true;
    ctx.roleObject['projectPaymentAccessview']=true;
    ctx.roleObject['projectPaymentAccessedit']=true;

    ctx.roleObject['projectTaskAccess']=true;
    ctx.roleObject['projectTaskview']=true;
    ctx.roleObject['projectTaskedit']=true;

    ctx.roleObject['projectTORAccess']=true;
    ctx.roleObject['projectTORview']=true;
    ctx.roleObject['projectTORedit']=true;

    ctx.roleObject['projectIssuesAccess']=true;
    ctx.roleObject['projectIssuesview']=true;
    ctx.roleObject['projectIssuesedit']=true;

    ctx.roleObject['projectWorkOrderAccess'] = true;
    ctx.roleObject['projectWorkOrderview'] = true;
    ctx.roleObject['projectWorkOrderedit'] = true;

    ctx.roleObject['projectRebaseLineAccess']=true;
    ctx.roleObject['projectRebaseLineview']=true;
    ctx.roleObject['projectRebaseLineedit']=true;

    ctx.roleObject['projectPlanAccess']=true;
    ctx.roleObject['projectPlanview']=true;
    ctx.roleObject['projectPlanedit']=true;

    ctx.roleObject['projectCharterAccess']=true;
    ctx.roleObject['projectCharterview']=true;
    ctx.roleObject['projectCharteredit']=true;

    ctx.roleObject['misReport'] = true;

    ctx.roleObject['weeklyReportAccess'] = true;
    ctx.roleObject['weeklyReportview'] = true;
    ctx.roleObject['weeklyReportedit'] = true;

    ctx.roleObject['projectPerformanceAccess'] = true;
    ctx.roleObject['projectPerformanceview'] = true;
    ctx.roleObject['projectPerformanceedit'] = true;

    ctx.roleObject['deptOverspentAccess'] = true;
    ctx.roleObject['deptOverspentview'] = true;
    ctx.roleObject['deptOverspentedit'] = true;

    ctx.roleObject['budgetUtilizationAccess'] = true;
    ctx.roleObject['budgetUtilizationview'] = true;
    ctx.roleObject['budgetUtilizationedit'] = true;

  }
  function addLegalRole(ctx,cb){

    ctx.roleObject['legalManagement']=true;
    ctx.roleObject['legalMater']=true;
    ctx.roleObject['caseTypeAccess']=true;
    ctx.roleObject['caseTypeview']=true;
    ctx.roleObject['caseTypeedit']=true;

    ctx.roleObject['holidaysAccess']=true;
    ctx.roleObject['holidaysview']=true;
    ctx.roleObject['holidaysedit']=true;

    ctx.roleObject['empannelGroupAccess']=true;
    ctx.roleObject['empannelGroupview']=true;
    ctx.roleObject['empannelGroupedit']=true;

    ctx.roleObject['advocateListsAccess']=true;
    ctx.roleObject['advocateListsview']=true;
    ctx.roleObject['advocateListsedit']=true;

    ctx.roleObject['statusAccess']=true;
    ctx.roleObject['statusview']=true;
    ctx.roleObject['statusedit']=true;

    ctx.roleObject['priorityAccess']=true;
    ctx.roleObject['priorityview']=true;
    ctx.roleObject['priorityedit']=true;

    ctx.roleObject['actsAccess']=true;
    ctx.roleObject['actsview']=true;
    ctx.roleObject['actsedit']=true;

    ctx.roleObject['stakeHoldersAccess']=true;
    ctx.roleObject['stakeHoldersview']=true;
    ctx.roleObject['stakeHoldersedit']=true;

    ctx.roleObject['courtInfoAccess']=true;
    ctx.roleObject['courtInfoview']=true;
    ctx.roleObject['courtInfoedit']=true;

    ctx.roleObject['caseDepartmentsAccess']=true;
    ctx.roleObject['caseDepartmentsview']=true;
    ctx.roleObject['caseDepartmentsedit']=true;

    ctx.roleObject['paymentModeAccess']=true;
    ctx.roleObject['paymentModeview']=true;
    ctx.roleObject['paymentModesedit']=true;

    ctx.roleObject['legalEmailTemplateAccess']=true;
    ctx.roleObject['legalEmailTemplateview']=true;
    ctx.roleObject['legalEmailTemplateedit']=true;

    ctx.roleObject['legalCase']=true;
    ctx.roleObject['createCaseAccess']=true;
    ctx.roleObject['createCaseview']=true;
    ctx.roleObject['createCaseedit']=true;

    ctx.roleObject['advocatePaymentsAccess']=true;
    ctx.roleObject['advocatePaymentsview']=true;
    ctx.roleObject['advocatePaymentsedit']=true;

    ctx.roleObject['hearingDateInfoAccess']=true;
    ctx.roleObject['hearingDateInfoview']=true;
    ctx.roleObject['hearingDateInfoedit']=true;

    ctx.roleObject['noticeCommentsAccess']=true;
    ctx.roleObject['noticeCommentsview']=true;
    ctx.roleObject['noticeCommentsedit']=true;

    ctx.roleObject['caseStakeHolderAccess']=true;
    ctx.roleObject['caseStakeHolderview']=true;
    ctx.roleObject['caseStakeHolderedit']=true;

    ctx.roleObject['docUploadAccess']=true;

    ctx.roleObject['docUploadview']=true;
    ctx.roleObject['docUploadedit']=true;

    ctx.roleObject['writePetitionAccess']=true;
    ctx.roleObject['writePetitionview']=true;
    ctx.roleObject['writePetitionedit']=true;

    ctx.roleObject['judgementAccess']=true;
    ctx.roleObject['judgementview']=true;
    ctx.roleObject['judgementedit']=true;

    ctx.roleObject['caseReportsAccess']=true;
    ctx.roleObject['caseReportsview']=true;
    ctx.roleObject['caseReportsedit']=true;

  }
  function addLandRole(ctx,cb){
    ctx.roleObject['landAndAsset']=true;
    ctx.roleObject['landAndAssetManagement']=true;
    ctx.roleObject['masterData']=true;
    ctx.roleObject['assetTypeAccess']=true;
    ctx.roleObject['assetTypeview']=true;
    ctx.roleObject['assetTypeedit']=true;

    ctx.roleObject['assetGroupAccess']=true;
    ctx.roleObject['assetGroupview']=true;
    ctx.roleObject['assetGroupedit']=true;

    ctx.roleObject['assetProfileAccess']=true;
    ctx.roleObject['assetProfileview']=true;
    ctx.roleObject['assetProfileedit']=true;

    ctx.roleObject['assetCriticalityAccess']=true;
    ctx.roleObject['assetCriticalityview']=true;
    ctx.roleObject['assetCriticalityedit']=true;

    ctx.roleObject['maintenanceObjectivesAccess']=true;
    ctx.roleObject['maintenanceObjectivesview']=true;
    ctx.roleObject['maintenanceObjectivesedit']=true;

    ctx.roleObject['typeOfDutyAccess']=true;
    ctx.roleObject['typeOfDutyview']=true;
    ctx.roleObject['typeOfDutyedit']=true;

    ctx.roleObject['accountDetailsAccess']=true;
    ctx.roleObject['accountDetailsview']=true;
    ctx.roleObject['accountDetailsedit']=true;

    ctx.roleObject['failureClassesAccess']=true;
    ctx.roleObject['failureClassesview']=true;
    ctx.roleObject['failureClassesedit']=true;

    ctx.roleObject['assetSparesAndConsumablesAccess']=true;
    ctx.roleObject['assetSparesAndConsumablesview']=true;
    ctx.roleObject['assetSparesAndConsumablesedit']=true;

    ctx.roleObject['documentListMasterAccess']=true;
    ctx.roleObject['documentListMasterview']=true;
    ctx.roleObject['documentListMasteredit']=true;

    ctx.roleObject['assetStatusAccess']=true;
    ctx.roleObject['assetStatusview']=true;
    ctx.roleObject['assetStatusedit']=true;

    ctx.roleObject['assetKPIsAccess']=true;
    ctx.roleObject['assetKPIsview']=true;
    ctx.roleObject['assetKPIsedit']=true;

    ctx.roleObject['assetServiceLevelAccess']=true;
    ctx.roleObject['assetServiceLevelview']=true;
    ctx.roleObject['assetServiceLeveledit']=true;

    ctx.roleObject['assetDisposalMethodsAccess']=true;
    ctx.roleObject['assetDisposalMethodsview']=true;
    ctx.roleObject['assetDisposalMethodsedit']=true;

    ctx.roleObject['assetRisksAccess']=true;
    ctx.roleObject['assetRisksview']=true;
    ctx.roleObject['assetRisksedit']=true;

    ctx.roleObject['costCenterAccess']=true;
    ctx.roleObject['costCenterview']=true;
    ctx.roleObject['costCenteredit']=true;

    ctx.roleObject['assetPoliciesAccess']=true;
    ctx.roleObject['assetPoliciesview']=true;
    ctx.roleObject['assetPoliciesedit']=true;

    ctx.roleObject['assetLifeCyclePlanAccess']=true;
    ctx.roleObject['assetLifeCyclePlanview']=true;
    ctx.roleObject['assetLifeCyclePlanedit']=true;

    ctx.roleObject['assetDepartmentAccess']=true;
    ctx.roleObject['assetDepartmentview']=true;
    ctx.roleObject['assetDepartmentedit']=true;

    ctx.roleObject['assetEmailConfigurationAccess']=true;
    ctx.roleObject['assetEmailConfigurationview']=true;
    ctx.roleObject['assetEmailConfigurationedit']=true;

    ctx.roleObject['createAssetLandAccess']=true;
    ctx.roleObject['createAssetLandview']=true;
    ctx.roleObject['createAssetLandedit']=true;

    ctx.roleObject['createAssetOthersAccess'] = true;
    ctx.roleObject['createAssetOthersview'] = true;
    ctx.roleObject['createAssetOthersedit'] = true;

    ctx.roleObject['costTransferFundsAccess']=true;
    ctx.roleObject['costTransferFundsview']=true;
    ctx.roleObject['costTransferFundsedit']=true;

    ctx.roleObject['createAssetObjectivesAndKPIsAccess']=true;
    ctx.roleObject['createAssetObjectivesAndKPIsview']=true;
    ctx.roleObject['createAssetObjectivesAndKPIsedit']=true;

    ctx.roleObject['createAssetSparesAndConsumablesAccess']=true;
    ctx.roleObject['createAssetSparesAndConsumablesview']=true;
    ctx.roleObject['createAssetSparesAndConsumablesedit']=true;

    ctx.roleObject['createAssetFailureAndMaintenanceAccess']=true;
    ctx.roleObject['createAssetFailureAndMaintenanceview']=true;
    ctx.roleObject['createAssetFailureAndMaintenanceedit']=true;

    ctx.roleObject['createAssetRisksAccess']=true;
    ctx.roleObject['createAssetRisksview']=true;
    ctx.roleObject['createAssetRisksedit']=true;

    ctx.roleObject['createAssetCausesAccess']=true;
    ctx.roleObject['createAssetCausesview']=true;
    ctx.roleObject['createAssetCausesedit']=true;

    ctx.roleObject['createAssetServiceLevelForecastAccess']=true;
    ctx.roleObject['createAssetServiceLevelForecastview']=true;
    ctx.roleObject['createAssetServiceLevelForecastedit']=true;

    ctx.roleObject['assetDemandProjectionAccess']=true;
    ctx.roleObject['assetDemandProjectionview']=true;
    ctx.roleObject['assetDemandProjectionedit']=true;

    ctx.roleObject['vehicleManagement']=true;
    ctx.roleObject['purchaseRequestsAccess']=true;
    ctx.roleObject['requestsview']=true;
    ctx.roleObject['requestsedit']=true;

    ctx.roleObject['purchaseRequestsReportAccess']=true;
    ctx.roleObject['purchaseRequestsReportview']=true;
    ctx.roleObject['purchaseRequestsReportedit']=true;

    ctx.roleObject['purchaseAgendaAccess']=true;
    ctx.roleObject['purchaseAgendaview']=true;
    ctx.roleObject['purchaseAgendaedit']=true;

    ctx.roleObject['assignDriverAccess']=true;
    ctx.roleObject['assignDriverview']=true;
    ctx.roleObject['assignDriveredit']=true;

    ctx.roleObject['logBookAccess']=true;
    ctx.roleObject['logBookview']=true;
    ctx.roleObject['logBookedit']=true;

    ctx.roleObject['submitBillsAccess']=true;
    ctx.roleObject['submitBillsview']=true;
    ctx.roleObject['submitBillsedit']=true;
  }
  function addWorkflowRole(ctx,cb){

    ctx.roleObject['userAdmin']=true;

    ctx.roleObject['appConfigAccess']=true;
    ctx.roleObject['appConfigview']=true;
    ctx.roleObject['appConfigedit']=true;

    ctx.roleObject['workflowFormsAccess']=true;
    ctx.roleObject['workflowFormsview']=true;
    ctx.roleObject['workflowFormsedit']=true;

    ctx.roleObject['workflowEmployeeAccess']=true;
    ctx.roleObject['workflowEmployeeview']=true;
    ctx.roleObject['workflowEmployeeedit']=true;

    ctx.roleObject['contractorsListAccess']=true;
    ctx.roleObject['contractorsListview']=true;
    ctx.roleObject['contractorsListedit']=true;


    ctx.roleObject['workflowLevelsAccess']=true;
    ctx.roleObject['workflowLevelsview']=true;
    ctx.roleObject['workflowLevelsedit']=true;

    ctx.roleObject['workflowAccess']=true;
    ctx.roleObject['workflowview']=true;
    ctx.roleObject['workflowedit']=true;

    ctx.roleObject['roleAccess']=true;
    ctx.roleObject['roleview']=true;
    ctx.roleObject['roleedit']=true;

    ctx.roleObject['languageDataAccess']=true;
    ctx.roleObject['languageDataview']=true;
    ctx.roleObject['languageDataedit']=true;


    ctx.roleObject['employeeTypeAccess']=true;
    ctx.roleObject['employeeTypeview']=true;
    ctx.roleObject['employeeTypeedit']=true;

    ctx.roleObject['employeeDepartmentAccess']=true;
    ctx.roleObject['employeeDepartmentview']=true;
    ctx.roleObject['employeeDepartmentedit']=true;

    ctx.roleObject['employeeDesignationAccess']=true;
    ctx.roleObject['employeeDesignationview']=true;
    ctx.roleObject['employeeDesignationedit']=true;

    ctx.roleObject['employeeAccess']=true;
    ctx.roleObject['employeeview']=true;
    ctx.roleObject['employeeedit']=true;

    cb(null,ctx);
  }

  function checkAccessDetailsDetails(ctx, roleForms,cb){
    if(roleForms.length>0){

      for(var i=0;i<roleForms.length;i++){
        if(roleForms[i].formDetails!=undefined && roleForms[i].formDetails!=null && roleForms[i].formDetails.length>0){
          for(var j=0;j<roleForms[i].formDetails.length;j++) {
            var formDetails = roleForms[i].formDetails[j];
            if (formDetails.value != undefined && formDetails.value == "schemeManagement") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['scheme'] = true;
                ctx.roleObject['schemeManagementAccess'] = true;
                ctx.roleObject['schemeManagementview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['scheme'] = true;
                ctx.roleObject['schemeManagementAccess'] = true;
                ctx.roleObject['schemeManagementview'] = true;
                ctx.roleObject['schemeManagementedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "schemeRequests") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['scheme'] = true;
                ctx.roleObject['schemeRequestsAccess'] = true;
                ctx.roleObject['schemeRequestsview'] = true;
              }

              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['scheme'] = true;
                ctx.roleObject['schemeRequestsAccess'] = true;
                ctx.roleObject['schemeRequestsview'] = true;
                ctx.roleObject['schemeRequestsedit'] = true;
              }
            }
            // for Scheme Master Details
            else if (formDetails.value != undefined && formDetails.value == "ministries") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['configuration'] = true;
                ctx.roleObject['ministriesAccess'] = true;
                ctx.roleObject['ministriesview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['configuration'] = true;
                ctx.roleObject['ministriesAccess'] = true;
                ctx.roleObject['ministriesview'] = true;
                ctx.roleObject['ministriesedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "schemeDepartment") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['configuration'] = true;
                ctx.roleObject['schemeDepartmentAccess'] = true;
                ctx.roleObject['schemeDepartmentview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['configuration'] = true;
                ctx.roleObject['schemeDepartmentAccess'] = true;
                ctx.roleObject['schemeDepartmentview'] = true;
                ctx.roleObject['schemeDepartmentedit'] = true
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "beneficiaries") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['configuration'] = true;
                ctx.roleObject['beneficiariesAccess'] = true;
                ctx.roleObject['beneficiariesview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['configuration'] = true;
                ctx.roleObject['beneficiariesAccess'] = true;
                ctx.roleObject['beneficiariesview'] = true;
                ctx.roleObject['beneficiariesedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "emailAccess") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['configuration'] = true;
                ctx.roleObject['emailAccess'] = true;
                ctx.roleObject['emailview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['configuration'] = true;
                ctx.roleObject['emailAccess'] = true;
                ctx.roleObject['emailview'] = true;
                ctx.roleObject['emailedit'] = true;

              }
            }
            // For Project ward works Master Details
            else if (formDetails.value != undefined && formDetails.value == "headOrScheme") {
              if (formDetails.view != undefined && formDetails.view) {

                ctx.roleObject['projectMaster'] = true;
                console.log('details for all are showing' + ctx.roleObject.projectMaster);
                ctx.roleObject['project'] = true;
                ctx.roleObject['headOrSchemeAccess'] = true;
                ctx.roleObject['headOrSchemeview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['headOrSchemeAccess'] = true;
                ctx.roleObject['headOrSchemeview'] = true;
                ctx.roleObject['headOrSchemeedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "ulb") {
              if (formDetails.view != undefined && formDetails.view) {

                ctx.roleObject['projectMaster'] = true;
                console.log('details for all are showing' + ctx.roleObject.projectMaster);
                ctx.roleObject['project'] = true;
                ctx.roleObject['ulbAccess'] = true;
                ctx.roleObject['ulbview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['ulbAccess'] = true;
                ctx.roleObject['ulbview'] = true;
                ctx.roleObject['ulbedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "taskStatus") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['taskStatusAccess'] = true;
                ctx.roleObject['taskStatusview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['taskStatusAccess'] = true;
                ctx.roleObject['taskStatusview'] = true;
                ctx.roleObject['taskStatusedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "planState") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['planStateAccess'] = true;
                ctx.roleObject['planStateview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['planStateAccess'] = true;
                ctx.roleObject['planStateview'] = true;
                ctx.roleObject['planStateedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "projectWardworksEmailTemplete") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectWardworksEmailTempleteAccess'] = true;
                ctx.roleObject['projectWardworksEmailTempleteview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectWardworksEmailTempleteAccess'] = true;
                ctx.roleObject['projectWardworksEmailTempleteview'] = true;
                ctx.roleObject['projectWardworksEmailTempleteedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "documentType") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['documentTypeAccess'] = true;
                ctx.roleObject['documentTypeview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['documentTypeAccess'] = true;
                ctx.roleObject['documentTypeview'] = true;
                ctx.roleObject['documentTypeedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "planDepartment") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['planDepartmentAccess'] = true;
                ctx.roleObject['planDepartmentview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMaster'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['planDepartmentAccess'] = true;
                ctx.roleObject['planDepartmentview'] = true;
                ctx.roleObject['planDepartmentedit'] = true;
              }
            }

            // Email and Document List is not displayed


            // For Plan Details
            else if (formDetails.value != undefined && formDetails.value == "projectCharter") {
              if (formDetails.view != undefined && formDetails.view) {

                ctx.roleObject['project'] = true;
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['projectCharterAccess'] = true;
                ctx.roleObject['projectCharterview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectCharterAccess'] = true;
                ctx.roleObject['projectCharterview'] = true;
                ctx.roleObject['projectCharteredit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "nocRequests") {
              if (formDetails.view != undefined && formDetails.view) {

                ctx.roleObject['project'] = true;
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['nocRequestsAccess'] = true;
                ctx.roleObject['nocRequestsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['nocRequestsAccess'] = true;
                ctx.roleObject['nocRequestsview'] = true;
                ctx.roleObject['nocRequestsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "fieldReports") {
              if (formDetails.view != undefined && formDetails.view) {

                ctx.roleObject['project'] = true;
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['fieldReportsAccess'] = true;
                ctx.roleObject['fieldReportsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['fieldReportsAccess'] = true;
                ctx.roleObject['fieldReportsview'] = true;
                ctx.roleObject['fieldReportsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectRequests") {
              if (formDetails.view != undefined && formDetails.view) {

                ctx.roleObject['project'] = true;
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['projectRequestsAccess'] = true;
                ctx.roleObject['projectRequestsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectRequestsAccess'] = true;
                ctx.roleObject['projectRequestsview'] = true;
                ctx.roleObject['projectRequestsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectPlan") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectPlanAccess'] = true;
                ctx.roleObject['projectPlanview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectPlanAccess'] = true;
                ctx.roleObject['projectPlanview'] = true;
                ctx.roleObject['projectPlanedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectTask") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectTaskAccess'] = true;
                ctx.roleObject['projectTaskview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectTaskAccess'] = true;
                ctx.roleObject['projectTaskview'] = true;
                ctx.roleObject['projectTaskedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectMeasurementBook") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectMeasurementAccessAccess'] = true;
                ctx.roleObject['projectMeasurementAccessview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectMeasurementAccessAccess'] = true;
                ctx.roleObject['projectMeasurementAccessview'] = true;
                ctx.roleObject['projectMeasurementAccessedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectPaymentOrder") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectPaymentAccessAccess'] = true;
                ctx.roleObject['projectPaymentAccessview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectPaymentAccessAccess'] = true;
                ctx.roleObject['projectPaymentAccessview'] = true;
                ctx.roleObject['projectPaymentAccessedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectTOR") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectTORAccess'] = true;
                ctx.roleObject['projectTORview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectTORAccess'] = true;
                ctx.roleObject['projectTORview'] = true;
                ctx.roleObject['projectTORedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectIssues") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectIssuesAccess'] = true;
                ctx.roleObject['projectIssuesview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectIssuesAccess'] = true;
                ctx.roleObject['projectIssuesview'] = true;
                ctx.roleObject['projectIssuesedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectRebaseLine") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectRebaseLineAccess'] = true;
                ctx.roleObject['projectRebaseLinesview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectRebaseLineAccess'] = true;
                ctx.roleObject['projectRebaseLineview'] = true;
                ctx.roleObject['projectRebaseLineedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectLayoutUpload") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectLayoutUploadAccess'] = true;
                ctx.roleObject['projectLayoutUploadview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectLayoutUploadAccess'] = true;
                ctx.roleObject['projectLayoutUploadview'] = true;
                ctx.roleObject['projectLayoutUploadedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectBillGeneration") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectBillGenerationAccess'] = true;
                ctx.roleObject['projectBillGenerationview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectBillGenerationAccess'] = true;
                ctx.roleObject['projectBillGenerationview'] = true;
                ctx.roleObject['projectBillGenerationedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectFieldVisit") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectFieldVisitAccess'] = true;
                ctx.roleObject['projectFieldVisitview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectFieldVisitAccess'] = true;
                ctx.roleObject['projectFieldVisitview'] = true;
                ctx.roleObject['projectFieldVisitedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectTransferFounds") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectTransferFoundsAccess'] = true;
                ctx.roleObject['projectTransferFoundsview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectTransferFoundsAccess'] = true;
                ctx.roleObject['projectTransferFoundsview'] = true;
                ctx.roleObject['projectTransferFoundsedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectUploads") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectUploadsAccess'] = true;
                ctx.roleObject['projectUploadsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectUploadsAccess'] = true;
                ctx.roleObject['projectUploadsview'] = true;
                ctx.roleObject['projectUploadsedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectWorkOrder") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectWorkOrderAccess'] = true;
                ctx.roleObject['projectWorkOrderview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectWorkOrderAccess'] = true;
                ctx.roleObject['projectWorkOrderview'] = true;
                ctx.roleObject['projectWorkOrderedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectComments") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectCommentsAccess'] = true;
                ctx.roleObject['projectCommentsview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectCommentsAccess'] = true;
                ctx.roleObject['projectCommentsview'] = true;
                ctx.roleObject['projectCommentsedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectDeliverable") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectDeliverableAccess'] = true;
                ctx.roleObject['projectDeliverableview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectDeliverableAccess'] = true;
                ctx.roleObject['projectDeliverableview'] = true;
                ctx.roleObject['projectDeliverabledit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectPaymentMilestones") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectPaymentMilestonesAccess'] = true;
                ctx.roleObject['projectPaymentMilestonesview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectPaymentMilestonesAccess'] = true;
                ctx.roleObject['projectPaymentMilestonesview'] = true;
                ctx.roleObject['projectPaymentMilestonesedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectMilestones") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectMilestonesAccess'] = true;
                ctx.roleObject['pprojectMilestonesview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectMilestonesAccess'] = true;
                ctx.roleObject['pprojectMilestonesview'] = true;
                ctx.roleObject['projectMilestonesedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectDepartmentWiseBudget") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectDepartmentWiseBudgetAccess'] = true;
                ctx.roleObject['projectDepartmentWiseBudgetview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectDepartmentWiseBudgetAccess'] = true;
                ctx.roleObject['projectDepartmentWiseBudgetview'] = true;
                ctx.roleObject['projectDepartmentWiseBudgetedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectRisks") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectRisksAccess'] = true;
                ctx.roleObject['projectRisksview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectRisksAccess'] = true;
                ctx.roleObject['projectRisksview'] = true;
                ctx.roleObject['projectRisksedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectClosure") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectClosureAccess'] = true;
                ctx.roleObject['projectClosureview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['projectMgmt'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectClosureAccess'] = true;
                ctx.roleObject['projectClosureview'] = true;
                ctx.roleObject['projectClosureedit'] = true;
              }
            }
            // MIS report
            else if (formDetails.value != undefined && formDetails.value == "budgetUtilization") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['misReport'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['budgetUtilizationAccess'] = true;
                ctx.roleObject['budgetUtilizationview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['misReport'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['budgetUtilizationAccess'] = true;
                ctx.roleObject['budgetUtilizationview'] = true;
                ctx.roleObject['budgetUtilizationedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "deptOverspent") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['misReport'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['deptOverspentAccess'] = true;
                ctx.roleObject['deptOverspentview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['misReport'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['deptOverspentAccess'] = true;
                ctx.roleObject['deptOverspentview'] = true;
                ctx.roleObject['deptOverspentedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "projectPerformance") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['misReport'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectPerformanceAccess'] = true;
                ctx.roleObject['projectPerformanceview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['misReport'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['projectPerformanceAccess'] = true;
                ctx.roleObject['projectPerformanceview'] = true;
                ctx.roleObject['projectPerformanceedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "weeklyReport") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['misReport'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['weeklyReportAccess'] = true;
                ctx.roleObject['weeklyReportview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['misReport'] = true;
                ctx.roleObject['project'] = true;
                ctx.roleObject['weeklyReportAccess'] = true;
                ctx.roleObject['weeklyReportview'] = true;
                ctx.roleObject['weeklyReportedit'] = true;
              }
            }

            // For legal mgmt Master
            else if (formDetails.value != undefined && formDetails.value == "holidays") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['holidaysAccess'] = true;
                ctx.roleObject['holidaysview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['holidaysAccess'] = true;
                ctx.roleObject['holidaysview'] = true;
                ctx.roleObject['holidaysedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "empannelGroup") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['empannelGroupAccess'] = true;
                ctx.roleObject['empannelGroupview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['empannelGroupAccess'] = true;
                ctx.roleObject['empannelGroupview'] = true;
                ctx.roleObject['empannelGroupedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "advocateLists") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['advocateListsAccess'] = true;
                ctx.roleObject['advocateListsview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['advocateListsAccess'] = true;
                ctx.roleObject['advocateListsview'] = true;
                ctx.roleObject['advocateListsedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "status") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['statusAccess'] = true;
                ctx.roleObject['statusview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['statusAccess'] = true;
                ctx.roleObject['statusview'] = true;
                ctx.roleObject['statusedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "priority") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['priorityAccess'] = true;
                ctx.roleObject['priorityview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['priorityAccess'] = true;
                ctx.roleObject['priorityview'] = true;
                ctx.roleObject['priorityedit'] = true;

              }
            }

            else if (formDetails.value != undefined && formDetails.value == "acts") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['actsAccess'] = true;
                ctx.roleObject['actsview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['actsAccess'] = true;
                ctx.roleObject['actsview'] = true;
                ctx.roleObject['actsedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "stakeHolders") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['stakeHoldersAccess'] = true;
                ctx.roleObject['stakeHoldersview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['stakeHoldersAccess'] = true;
                ctx.roleObject['stakeHoldersview'] = true;
                ctx.roleObject['stakeHoldersedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "courtInfo") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['courtInfoAccess'] = true;
                ctx.roleObject['courtInfoview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['courtInfoAccess'] = true;
                ctx.roleObject['courtInfoview'] = true;
                ctx.roleObject['courtInfoedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "caseDepartments") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;

                ctx.roleObject['caseDepartmentsAccess'] = true;
                ctx.roleObject['caseDepartmentsview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;

                ctx.roleObject['caseDepartmentsAccess'] = true;
                ctx.roleObject['caseDepartmentsview'] = true;
                ctx.roleObject['caseDepartmentsedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "paymentMode") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['paymentModeAccess'] = true;
                ctx.roleObject['paymentModeview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['paymentModeAccess'] = true;
                ctx.roleObject['paymentModeview'] = true;
                ctx.roleObject['paymentModesedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "legalEmailTemplate") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['legalEmailTemplateAccess'] = true;
                ctx.roleObject['legalEmailTemplateview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['legalEmailTemplateAccess'] = true;
                ctx.roleObject['legalEmailTemplateview'] = true;
                ctx.roleObject['legalEmailTemplateedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "caseType") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['caseTypeAccess'] = true;
                ctx.roleObject['caseTypeview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalMater'] = true;
                ctx.roleObject['caseTypeAccess'] = true;
                ctx.roleObject['caseTypeview'] = true;
                ctx.roleObject['caseTypeedit'] = true;
              }
            }

            // For Case details
            else if (formDetails.value != undefined && formDetails.value == "createCase") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['createCaseAccess'] = true;
                ctx.roleObject['createCaseview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['createCaseAccess'] = true;
                ctx.roleObject['createCaseview'] = true;
                ctx.roleObject['createCaseedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "advocatePayments") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['advocatePaymentsAccess'] = true;
                ctx.roleObject['advocatePaymentsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['advocatePaymentsAccess'] = true;
                ctx.roleObject['advocatePaymentsview'] = true;
                ctx.roleObject['advocatePaymentsedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "hearingDateInfo") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['hearingDateInfoAccess'] = true;
                ctx.roleObject['hearingDateInfoview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['hearingDateInfoAccess'] = true;
                ctx.roleObject['hearingDateInfoview'] = true;
                ctx.roleObject['hearingDateInfoedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "noticeComments") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['noticeCommentsAccess'] = true;
                ctx.roleObject['noticeCommentsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['noticeCommentsAccess'] = true;
                ctx.roleObject['noticeCommentsview'] = true;
                ctx.roleObject['noticeCommentsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "docUpload") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['docUploadAccess'] = true;
                ctx.roleObject['docUploadview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['docUploadAccess'] = true;
                ctx.roleObject['docUploadview'] = true;
                ctx.roleObject['docUploadedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "writePetition") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['writePetitionAccess'] = true;
                ctx.roleObject['writePetitionview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['writePetitionAccess'] = true;
                ctx.roleObject['writePetitionview'] = true;
                ctx.roleObject['writePetitionedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "judgement") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['judgementAccess'] = true;
                ctx.roleObject['judgementview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['judgementAccess'] = true;
                ctx.roleObject['judgementview'] = true;
                ctx.roleObject['judgementedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "caseReports") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['caseReportsAccess'] = true;
                ctx.roleObject['caseReportsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['legalManagement'] = true;
                ctx.roleObject['legalCase'] = true;
                ctx.roleObject['caseReportsAccess'] = true;
                ctx.roleObject['caseReportsview'] = true;
                ctx.roleObject['caseReportsedit'] = true;

              }
            }


            // For User Administartion
            else if (formDetails.value != undefined && formDetails.value == "workflowForms") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['workflowFormsAccess'] = true;
                ctx.roleObject['workflowFormsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['workflowFormsAccess'] = true;
                ctx.roleObject['workflowFormsview'] = true;
                ctx.roleObject['workflowFormsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "workflowEmployee") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['workflowEmployeeAccess'] = true;
                ctx.roleObject['workflowEmployeeview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['workflowEmployeeAccess'] = true;
                ctx.roleObject['workflowEmployeeview'] = true;
                ctx.roleObject['workflowEmployeeedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "workflowLevels") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['workflowLevelsAccess'] = true;
                ctx.roleObject['workflowLevelsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['workflowLevelsAccess'] = true;
                ctx.roleObject['workflowLevelsview'] = true;
                ctx.roleObject['workflowLevelsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "workflow") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['workflowAccess'] = true;
                ctx.roleObject['workflowview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['workflowAccess'] = true;
                ctx.roleObject['workflowview'] = true;
                ctx.roleObject['workflowedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "contractorsList") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['contractorsListAccess'] = true;
                ctx.roleObject['contractorsListview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['contractorsListAccess'] = true;
                ctx.roleObject['contractorsListview'] = true;
                ctx.roleObject['contractorsListedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "role") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['roleAccess'] = true;
                ctx.roleObject['roleview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['roleAccess'] = true;
                ctx.roleObject['roleview'] = true;
                ctx.roleObject['roleedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "employeeType") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['employeeTypeAccess'] = true;
                ctx.roleObject['employeeTypeview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['employeeTypeAccess'] = true;
                ctx.roleObject['employeeTypeview'] = true;
                ctx.roleObject['employeeTypeedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "employeeDepartment") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['employeeDepartmentAccess'] = true;
                ctx.roleObject['employeeDepartmentview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['employeeDepartmentAccess'] = true;
                ctx.roleObject['employeeDepartmentview'] = true;
                ctx.roleObject['employeeDepartmentedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "employeeDesignation") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['employeeDesignationAccess'] = true;
                ctx.roleObject['employeeDesignationview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['employeeDesignationAccess'] = true;
                ctx.roleObject['employeeDesignationview'] = true;
                ctx.roleObject['employeeDesignationedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "appConfig") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['appConfigAccess'] = true;
                ctx.roleObject['appConfigview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['appConfigAccess'] = true;
                ctx.roleObject['appConfigview'] = true;
                ctx.roleObject['appConfigedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "languageData") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['languageDataAccess'] = true;
                ctx.roleObject['languageDataview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['languageDataAccess'] = true;
                ctx.roleObject['languageDataview'] = true;
                ctx.roleObject['languageDataedit'] = true;

              }
            }

            else if (formDetails.value != undefined && formDetails.value == "employee") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['employeeAccess'] = true;
                ctx.roleObject['employeeview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['userAdmin'] = true;
                ctx.roleObject['employeeAccess'] = true;
                ctx.roleObject['employeeview'] = true;
                ctx.roleObject['employeeedit'] = true;
              }
            }

            // Land And Asset Management
            else if (formDetails.value != undefined && formDetails.value == "submitBills") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['submitBillsAccess'] = true;
                ctx.roleObject['submitBillsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['submitBillsAccess'] = true;
                ctx.roleObject['submitBillsview'] = true;
                ctx.roleObject['submitBillsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "logBook") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['logBookAccess'] = true;
                ctx.roleObject['logBookview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['logBookAccess'] = true;
                ctx.roleObject['logBookview'] = true;
                ctx.roleObject['logBookedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assignDriver") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['assignDriverAccess'] = true;
                ctx.roleObject['assignDriverview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['assignDriverAccess'] = true;
                ctx.roleObject['assignDriverview'] = true;
                ctx.roleObject['assignDriveredit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "purchaseAgenda") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['purchaseAgendaAccess'] = true;
                ctx.roleObject['purchaseAgendaview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['purchaseAgendaAccess'] = true;
                ctx.roleObject['purchaseAgendaview'] = true;
                ctx.roleObject['purchaseAgendaedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "purchaseRequestsReport") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['purchaseRequestsReportAccess'] = true;
                ctx.roleObject['purchaseRequestsReportview'] = true;


              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['purchaseRequestsReportAccess'] = true;
                ctx.roleObject['purchaseRequestsReportview'] = true;
                ctx.roleObject['purchaseRequestsReportedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "purchaseRequests") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['purchaseRequestsAccess'] = true;
                ctx.roleObject['requestsview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['vehicleManagement'] = true;
                ctx.roleObject['purchaseRequestsAccess'] = true;
                ctx.roleObject['requestsview'] = true;
                ctx.roleObject['requestsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetDemandProjection") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetDemandProjectionAccess'] = true;
                ctx.roleObject['assetDemandProjectionview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetDemandProjectionAccess'] = true;
                ctx.roleObject['assetDemandProjectionview'] = true;
                ctx.roleObject['assetDemandProjectionedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "createAssetRisks") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetRisksAccess'] = true;
                ctx.roleObject['createAssetRisksview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetRisksAccess'] = true;
                ctx.roleObject['createAssetRisksview'] = true;
                ctx.roleObject['createAssetRisksedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "createAssetCauses") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetCausesAccess'] = true;
                ctx.roleObject['createAssetCausesview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetCausesAccess'] = true;
                ctx.roleObject['createAssetCausesview'] = true;
                ctx.roleObject['createAssetCausesedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "createAssetServiceLevelForecast") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetServiceLevelForecastAccess'] = true;
                ctx.roleObject['createAssetServiceLevelForecastview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetServiceLevelForecastAccess'] = true;
                ctx.roleObject['createAssetServiceLevelForecastview'] = true;
                ctx.roleObject['createAssetServiceLevelForecastedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "createAssetFailureAndMaintenance") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetFailureAndMaintenanceAccess'] = true;
                ctx.roleObject['createAssetFailureAndMaintenanceview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetFailureAndMaintenanceAccess'] = true;
                ctx.roleObject['createAssetFailureAndMaintenanceview'] = true;
                ctx.roleObject['createAssetFailureAndMaintenanceedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "createAssetSparesAndConsumables") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetSparesAndConsumablesAccess'] = true;
                ctx.roleObject['createAssetSparesAndConsumablesview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetSparesAndConsumablesAccess'] = true;
                ctx.roleObject['createAssetSparesAndConsumablesview'] = true;
                ctx.roleObject['createAssetSparesAndConsumablesedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "createAssetObjectivesAndKPIs") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetObjectivesAndKPIsAccess'] = true;
                ctx.roleObject['createAssetObjectivesAndKPIsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetObjectivesAndKPIsAccess'] = true;
                ctx.roleObject['createAssetObjectivesAndKPIsview'] = true;
                ctx.roleObject['createAssetObjectivesAndKPIsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "costTransferFunds") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['costTransferFundsAccess'] = true;
                ctx.roleObject['costTransferFundsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['costTransferFundsAccess'] = true;
                ctx.roleObject['costTransferFundsview'] = true;
                ctx.roleObject['costTransferFundsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "createAssetOthers") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetOthersAccess'] = true;
                ctx.roleObject['createAssetOthersview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetOthersAccess'] = true;
                ctx.roleObject['createAssetOthersview'] = true;
                ctx.roleObject['createAssetOthersedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "createAssetLand") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetLandAccess'] = true;
                ctx.roleObject['createAssetLandview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['createAssetLandAccess'] = true;
                ctx.roleObject['createAssetLandview'] = true;
                ctx.roleObject['createAssetLandedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetEmailConfiguration") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetEmailConfigurationAccess'] = true;
                ctx.roleObject['assetEmailConfigurationview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetEmailConfigurationAccess'] = true;
                ctx.roleObject['assetEmailConfigurationview'] = true;
                ctx.roleObject['assetEmailConfigurationedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetLifeCyclePlan") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetLifeCyclePlanAccess'] = true;
                ctx.roleObject['assetLifeCyclePlanview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetLifeCyclePlanAccess'] = true;
                ctx.roleObject['assetLifeCyclePlanview'] = true;
                ctx.roleObject['assetLifeCyclePlanedit'] = true;
              }
            }

            else if (formDetails.value != undefined && formDetails.value == "assetDepartment") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetDepartmentAccess'] = true;
                ctx.roleObject['assetDepartmentview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetDepartmentAccess'] = true;
                ctx.roleObject['assetDepartmentview'] = true;
                ctx.roleObject['assetDepartmentedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetPolicies") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetPoliciesAccess'] = true;
                ctx.roleObject['assetPoliciesview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetPoliciesAccess'] = true;
                ctx.roleObject['assetPoliciesview'] = true;
                ctx.roleObject['assetPoliciesedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "costCenter") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['costCenterAccess'] = true;
                ctx.roleObject['costCenterview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['costCenterAccess'] = true;
                ctx.roleObject['costCenterview'] = true;
                ctx.roleObject['costCenteredit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetRisks") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetRisksAccess'] = true;
                ctx.roleObject['assetRisksview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetRisksAccess'] = true;
                ctx.roleObject['assetRisksview'] = true;
                ctx.roleObject['assetRisksedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetDisposalMethods") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetDisposalMethodsAccess'] = true;
                ctx.roleObject['assetDisposalMethodsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetDisposalMethodsAccess'] = true;
                ctx.roleObject['assetDisposalMethodsview'] = true;
                ctx.roleObject['assetDisposalMethodsedit'] = true;
              }
            }


            else if (formDetails.value != undefined && formDetails.value == "assetDisposalMethods") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetDisposalMethodsAccess'] = true;
                ctx.roleObject['assetDisposalMethodsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetDisposalMethodsAccess'] = true;
                ctx.roleObject['assetDisposalMethodsview'] = true;
                ctx.roleObject['assetDisposalMethodsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "accountDetails") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['accountDetailsAccess'] = true;
                ctx.roleObject['accountDetailsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['accountDetailsAccess'] = true;
                ctx.roleObject['accountDetailsview'] = true;
                ctx.roleObject['accountDetailsedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetKPIs") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetKPIsAccess'] = true;
                ctx.roleObject['assetKPIsview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetServiceLevelAccess'] = true;
                ctx.roleObject['assetServiceLevelview'] = true;
                ctx.roleObject['assetServiceLeveledit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetStatus") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetStatusAccess'] = true;
                ctx.roleObject['assetStatusview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetStatusAccess'] = true;
                ctx.roleObject['assetStatusview'] = true;
                ctx.roleObject['assetStatusedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "documentListMaster") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['documentListMasterAccess'] = true;
                ctx.roleObject['documentListMasterview'] = true;

              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['documentListMasterAccess'] = true;
                ctx.roleObject['documentListMasterview'] = true;
                ctx.roleObject['documentListMasteredit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetSparesAndConsumables") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetSparesAndConsumablesAccess'] = true;
                ctx.roleObject['assetSparesAndConsumablesview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetSparesAndConsumablesAccess'] = true;
                ctx.roleObject['assetSparesAndConsumablesview'] = true;
                ctx.roleObject['assetSparesAndConsumablesedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "failureClasses") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['failureClassesAccess'] = true;
                ctx.roleObject['failureClassesview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['failureClassesAccess'] = true;
                ctx.roleObject['failureClassesview'] = true;
                ctx.roleObject['failureClassesedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "typeOfDuty") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['typeOfDutyAccess'] = true;
                ctx.roleObject['typeOfDutyview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['typeOfDutyAccess'] = true;
                ctx.roleObject['typeOfDutyview'] = true;
                ctx.roleObject['typeOfDutyedit'] = true;

              }
            }
            else if (formDetails.value != undefined && formDetails.value == "maintenanceObjectives") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['maintenanceObjectivesAccess'] = true;
                ctx.roleObject['maintenanceObjectivesview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['maintenanceObjectivesAccess'] = true;
                ctx.roleObject['maintenanceObjectivesview'] = true;
                ctx.roleObject['maintenanceObjectivesedit'] = true;


              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetCriticality") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetCriticalityAccess'] = true;
                ctx.roleObject['assetCriticalityview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetCriticalityAccess'] = true;
                ctx.roleObject['assetCriticalityview'] = true;
                ctx.roleObject['assetCriticalityedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetProfile") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;

                ctx.roleObject['assetProfileAccess'] = true;
                ctx.roleObject['assetProfileview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;

                ctx.roleObject['assetProfileAccess'] = true;
                ctx.roleObject['assetProfileview'] = true;
                ctx.roleObject['assetProfileedit'] = true;


              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetGroup") {
              if (formDetails.view != undefined && formDetails.view) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetGroupAccess'] = true;
                ctx.roleObject['assetGroupview'] = true;
              }
              if (formDetails.edit != undefined && formDetails.edit) {
                ctx.roleObject['landAndAsset'] = true;
                ctx.roleObject['landAndAssetManagement'] = true;
                ctx.roleObject['assetGroupAccess'] = true;
                ctx.roleObject['assetGroupview'] = true;
                ctx.roleObject['assetGroupedit'] = true;
              }
            }
            else if (formDetails.value != undefined && formDetails.value == "assetType") {
                if (formDetails.view != undefined && formDetails.view) {
                  ctx.roleObject['landAndAsset'] = true;
                  ctx.roleObject['landAndAssetManagement'] = true;
                  ctx.roleObject['assetTypeAccess'] = true;
                  ctx.roleObject['assetTypeview'] = true;
                }
                if (formDetails.edit != undefined && formDetails.edit) {
                  ctx.roleObject['landAndAsset'] = true;
                  ctx.roleObject['landAndAssetManagement'] = true;
                  ctx.roleObject['assetTypeAccess'] = true;
                  ctx.roleObject['assetTypeview'] = true;
                  ctx.roleObject['assetTypeedit'] = true;
                }
              }
          }
        }
      }
      cb(null,ctx);

    }else{
      cb(null,ctx);
    }

  }







};
