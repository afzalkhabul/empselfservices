var server = require('../../server/server');
module.exports = function(Transferfund) {

  Transferfund.observe('before save', function (ctx, next) {
    var WorkflowForm=server.models.WorkflowForm;
    var Workflow=server.models.Workflow;
    var WorkflowEmployees=server.models.WorkflowEmployees;
    var notification=server.models.notification;
    if(ctx.instance){
      WorkflowForm.findOne({"where": {"and":[{"status": "Active"},{"schemeUniqueId": "transferFounds"}]}}, function (err, workflowForm) {
        if(workflowForm!=undefined && workflowForm!=null ){
          Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
            WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
              var workflowData = [];
              var employeeDetailsForNotification=[];
              if (listData != undefined && listData != null && listData.length > 0) {
                for (var i = 0; i < listData.length; i++) {
                  var flowData = {
                    workflowId: listData[i].workflowId,
                    levelId: listData[i].levelId,
                    status: listData[i].status,
                    maxLevels: listData[i].maxLevels,
                    levelNo: listData[i].levelNo
                  }
                  workflowData.push(flowData);
                  if(listData[i].levelNo=="1"){
                    if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                      for(var j=0;j<listData[i].employees.length;j++){
                        employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                      }
                    }
                  }
                }
              }
              ctx.instance.workflow = workflowData;
              ctx.instance.workflowId=workflowForm.workflowId;
              ctx.instance.maxlevel=flowDataList.maxLevel;
              ctx.instance.acceptLevel=0;
              ctx.instance.finalStatus=false;
              ctx.instance.lastUpdateTime=new Date();
              if(employeeDetailsForNotification.length>0){
                notification.create({
                  'to':employeeDetailsForNotification,
                  "subject":  'New Tasks',
                  "text":"You got New Plan Request",
                  "message":"You got New Plan Request",
                  'urlData':'projectPlanDetails',
                  "type":"newRequest"
                }, function (err, notificationDetails){
                  next();
                });

              }else{
                next();
              }

            });
          });
        }
      });

    }
    else{

      next();
    }

  });

  Transferfund.observe('loaded', function (ctx, next) {
    var ProjectHeader=server.models.ProjectHeader;

    if(ctx.instance){
      function getWorkflowData() {
        if (ctx.instance.workflowId != undefined && ctx.instance.workflowId != null && ctx.instance.workflowId != '') {
          var WorkflowEmployees = server.models.WorkflowEmployees;
          WorkflowEmployees.find({'where': {'and': [{'workflowId': ctx.instance.workflowId}, {"status": "Active"}]}}, function (err, workflowEmployeeList) {
            if (workflowEmployeeList != null && workflowEmployeeList.length > 0) {
              ctx.instance.workflowData = workflowEmployeeList;
              next();
            } else {
              next();
            }
          })
        } else {
          next();
        }
      }
      ProjectHeader.findById(ctx.instance.fromHeaderId, function(err, headerData){
        if(err){
        }else{
          if(headerData!=null){
            if(headerData.name!=undefined && headerData.name!=null && headerData.name!=''){
              ctx.instance.fromHeader = headerData.name;
            }
          }
        }
        ProjectHeader.findById(ctx.instance.toHeaderId, function(err, toHeaderData){
          if(err){
          }else{
            if(toHeaderData!=null && toHeaderData!='' &&toHeaderData.name!=undefined && toHeaderData.name!=null){
              ctx.instance.toHeader = toHeaderData.naProjectUploadsme;
            }
          }
          getWorkflowData();
        });
      });
    }else{
      next();
    }
  });

  Transferfund.getDetails = function (employee,planId, cb) {
    Transferfund.find({"where":{"planId":planId}},function(err, fieldVisitData){
      var fieldVisitList=[];
      var adminStatus=false;
      if(fieldVisitData!=null && fieldVisitData.length>0){
       var Employee=server.models.Employee;
        var adminStatus=false;
        Employee.find({'where':{"employeeId":employee}}, function (err, employeeList) {
          if(employeeList!=null && employeeList.length>0) {
            var firstEmployeeDetails = employeeList[0];
            if (firstEmployeeDetails.role == 'superAdmin' || firstEmployeeDetails.role == 'projectAdmin') {
              adminStatus = true;
            } else {

            }
            for (var i = 0; i < fieldVisitData.length; i++) {
              var data = fieldVisitData[i];
              var workflowDetails = data.workflowData;
              var accessFiledVisit = false;
              var approveFiledVisit = false;
              if (workflowDetails != undefined && workflowDetails != null && workflowDetails.length > 0) {
                var acceptLevel = data.acceptLevel;
                var editDetails;
                if (data.createdPersonId == employee) {
                  editDetails = true;
                }
                var availableStatus = false;
                if (data.finalStatus == false) {
                  for (var j = 0; j < workflowDetails.length; j++) {
                    var availableStatusLevel = false;
                    if (workflowDetails[j].employees != undefined && workflowDetails[j].employees != null && workflowDetails[j].employees.length > 0) {


                      var employeeList = workflowDetails[j].employees;
                      if (employeeList != undefined && employeeList != null && employeeList.length > 0) {
                        for (var x = 0; x < employeeList.length; x++) {
                          if (employeeList[x] == employee) {
                            availableStatus = true;
                            availableStatusLevel = true;
                            break;
                          }
                        }
                      }
                      if (availableStatusLevel) {
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                        console.log('workflow level' + workFlowLevel + " accepct level" + acceptLevel);
                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                        }
                      }else if(adminStatus){
                        availableStatus=true;
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                        console.log('workflow level' + workFlowLevel + " accepct level" + acceptLevel);
                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                        }
                      }

                    }

                  }
                  if (availableStatus || editDetails ) {
                    if (editDetails) {
                      if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                        data.editDetails = true;
                      } else {
                        data.editDetails = false;
                      }
                    }
                    data.approveFiledVisit = approveFiledVisit;
                    data.availableStatus = availableStatus;
                    fieldVisitList.push(data);
                  }
                }else{
                  if (editDetails) {
                    if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                      data.editDetails = true;
                    } else {
                      data.editDetails = false;
                    }
                  }
                  data.approveFiledVisit = false;
                  data.availableStatus = true;
                  fieldVisitList.push(data);
                }


              }
            }
            cb(null,fieldVisitList);
          }else{
            cb(null,fieldVisitList);
          }
        });
      }else{
        cb(null,fieldVisitList);
      }
    });
  };

  Transferfund.remoteMethod('getDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'employeeId', type: 'string', http: {source: 'query'},required:true},
      {arg: 'planId', type: 'string', http: {source: 'query'},required:true}],
    http: {
      path: '/getDetails',
      verb: 'GET'
    }
  });

  Transferfund.updateDetails = function (approvalDetails, cb) {

    Transferfund.findOne({'where':{'id':approvalDetails.requestId}},function(err, fieldVisitDetails){
      if(fieldVisitDetails!=undefined && fieldVisitDetails!=null){
        var finalObject={};
        if(fieldVisitDetails.acceptLevel!=undefined && fieldVisitDetails.acceptLevel!=null && fieldVisitDetails.acceptLevel==approvalDetails.acceptLevel){
          var workflowDetails=fieldVisitDetails.workflow;
        if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
          var workflowData=[];
          var statusOfFunds=false;
          for(var  i=0;i< workflowDetails.length;i++){
            var data=workflowDetails[i];
            var workFlowLevel=parseInt(workflowDetails[i].levelNo);
            if((approvalDetails.acceptLevel+1)==workFlowLevel){
              data.approvalStatus=approvalDetails.requestStatus;
              data.employeeName=approvalDetails.employeeName;
              data.employeeId=approvalDetails.employeeId;
              data.comment=approvalDetails.comment;
              data.date=new Date();
            }
            workflowData.push(data);

          }
          finalObject.workflow=workflowData;
          finalObject.lastUpdateTime=new Date();
        }

        if(approvalDetails.requestStatus=="Approval"){
          if(fieldVisitDetails.maxlevel==(approvalDetails.acceptLevel+1)){
            finalObject.finalStatus=approvalDetails.requestStatus;
            finalObject.acceptLevel=approvalDetails.acceptLevel+1;
            statusOfFunds=true;
            updateFunds(fieldVisitDetails,cb);

          /*  if(fieldVisitDetails!=null && fieldVisitDetails.transferAmount!=undefined &&
              fieldVisitDetails.transferAmount!=null && fieldVisitDetails.transferAmount!=''){
              var ProjectTasks=server.models.ProjectTasks;
              if(fieldVisitDetails.fromHeaderId!=undefined &&
                fieldVisitDetails.fromHeaderId!=null && fieldVisitDetails.fromHeaderId!=''){
                ProjectTasks.findOne({'where':{'id':fieldVisitDetails.fromHeaderId}},function(err, fromHeaderData){
                  if(fromHeaderData!=null && fromHeaderData.taskList!=null && fromHeaderData.taskList.length>0) {

                    var taskList = fromHeaderData.taskList;
                    if (taskList != null && fromHeaderData.taskList.length > 0) {
                      var estimatedCost;
                      var utilizedCost;
                      for (var i = 0; i < taskList.length; i++) {
                        if (approvalDetails.fromSubTask == taskList[i].subTaskName) {
                          if (taskList[i].utilizedCost != undefined && taskList[i].utilizedCost != null && taskList[i].utilizedCost != ''
                            && taskList[i].estimatedCost != undefined && taskList[i].estimatedCost != null && taskList[i].estimatedCost != '') {
                            estimatedCost = parseInt(taskList[i].estimatedCost);
                            utilizedCost = parseInt(taskList[i].utilizedCost);
                            break;
                          } else {
                            estimatedCost = 0;
                            utilizedCost = 0;
                            break;
                          }

                        }
                      }

                      if (estimatedCost > 0 && utilizedCost > 0) {
                        var finalAvalibleAmount = estimatedCost - utilizedCost;
                        var transferAmount = parseInt(fieldVisitDetails.transferAmount);
                        if (transferAmount <= finalAvalibleAmount) {
                          var upDatedData = {};
                          var updatedTaskList = [];
                          for (var i = 0; i < taskList.length; i++) {
                            if (fieldVisitDetails.fromSubTask == taskList[i].subTaskName) {
                              if (taskList[i].utilizedCost != undefined && taskList[i].utilizedCost != null && taskList[i].utilizedCost != ''
                                && taskList[i].estimatedCost != undefined && taskList[i].estimatedCost != null && taskList[i].estimatedCost != '') {
                                // estimatedCost= parseInt(taskList[i].estimatedCost);
                                taskList[i].utilizedCost = (utilizedCost + transferAmount);
                                updatedTaskList.push(taskList[i]);


                              } else {
                                updatedTaskList.push(taskList[i]);
                              }

                            }
                          }
                          upDatedData.taskList = updatedTaskList;
                          fromHeaderData.updateAttributes(updatedTaskList, function (err, data) {

                          });

                          ProjectTasks.findOne({'where': {'id': fieldVisitDetails.toHeaderId}}, function (err, toHeaderData) {
                            var toTaskList = toHeaderData.taskList;
                            if (toTaskList != null && toTaskList.length > 0) {
                              var toEstimatedCost;
                              for (var j = 0; j < toTaskList.length; j++) {
                                if (fieldVisitDetails.toSubTask == toTaskList[j].subTaskName) {
                                  if(toTaskList[j].subTaskName!=undefined && toTaskList[j].subTaskName!=null && toTaskList[j].subTaskName!=''){
                                    toEstimatedCost=toTaskList[j].estimatedCost;
                                    break;
                                  }else{
                                    toEstimatedCost=0;
                                    break;
                                  }

                                }


                              }
                              toEstimatedCost=toEstimatedCost+transferAmount;
                              var finalObjectForUpdate={};
                              var finaltaskList=[];
                              for (var j = 0; j < toTaskList.length; j++) {
                                if (fieldVisitDetails.toSubTask == toTaskList[j].subTaskName) {
                                  if(toTaskList[j].subTaskName!=undefined && toTaskList[j].subTaskName!=null && toTaskList[j].subTaskName!=''){
                                   toTaskList[j].estimatedCost=toEstimatedCost;
                                    finaltaskList.push(toTaskList[j]);

                                  }else{
                                    finaltaskList.push(toTaskList[j]);
                                  }

                                }


                              }
                              toHeaderData.updateAttributes(finaltaskList,function(err, finalData){

                              });


                            } else {

                              cb(null, 'The amount is lessthan slected task');
                            }


                          });


                        } else {
                          cb(null, 'The amount is lessthan slected task');

                        }


                      } else {
                        cb(null, 'Please provide valid from plan tasks');
                      }


                    } else {
                      cb(null, 'Please provide valid from plan tasks');
                    }
                  }else {
                    cb(null, 'Please provide valid from plan tasks');
                  }

                });
              }







            }*/
          }else{
            finalObject.acceptLevel=approvalDetails.acceptLevel+1;
          }

        } else if(approvalDetails.requestStatus=="Rejected"){
          finalObject.finalStatus=approvalDetails.requestStatus;
          finalObject.acceptLevel=approvalDetails.acceptLevel+1;
          finalObject.rejectComment=approvalDetails.comment;
        }
           if(!statusOfFunds){
            fieldVisitDetails.updateAttributes(finalObject, function(err, finalData){
              console.log(err);
              var Sms = server.models.Sms;
              if(finalObject.finalStatus!=undefined && finalObject.finalStatus!=null && finalObject.finalStatus=="Rejected" ){
                if(finalData.createdPersonId!=undefined && finalData.createdPersonId!=null && finalData.createdPersonId!='' ){
                  var Employee=server.models.Employee;
                  var EmailTemplete = server.models.EmailTemplete;
                  Employee.find({'where':{'employeeId':finalData.createdPersonId}}, function (err, employeeDetails) {
                    if(employeeDetails!=null && employeeDetails.length>0){
                      EmailTemplete.findOne({"where":{"emailType": "projectWardWorks"}},function (err, emailTemplete) {
                        if(emailTemplete!=null ) {
                          var message = emailTemplete.transferRejectedMessage + '<br>Reason is ' + approvalDetails.comment;
                          var Dhanbademail = server.models.DhanbadEmail;
                          var notification=server.models.notification;
                          notification.create({
                            'to':[{'employeeId':employeeDetails[0].employeeId,'readStatus':false}],
                            "subject":  'New Tasks',
                            "text":"Your project Transfer fund Request is Rejected",
                            "message":"Your project Transfer fund Request is Rejected",
                            'urlData':'transferFunds',
                            "type":"approvedRequest"
                          }, function (err, notificationDetails){
                          });
                          Dhanbademail.create({
                            "to": employeeDetails[0].emailId,
                            "subject":  emailTemplete.transferRejectedEmail,
                            "text":message
                          }, function (err, email) {

                          });

                          if(emailTemplete.transferRejectedSMS && employeeDetails[0].mobile) {
                            var Sms = server.models.Sms;
                            var smsData = {
                              "message": emailTemplete.transferRejectedSMS,
                              "mobileNo": employeeDetails[0].mobile,
                              "smsservicetype": "singlemsg"
                            };

                            Sms.create(smsData, function (err, smsInfo) {
                            });
                          }
                        }
                      });
                      cb(null, finalData);
                    }else{
                      cb(null, finalData);
                    }
                  });

                }else{
                  cb(null, finalData);
                }

              }
              else if(finalObject.finalStatus!=undefined && finalObject.finalStatus!=null && finalObject.finalStatus=="Approval"){

                if(finalData.createdPersonId!=undefined && finalData.createdPersonId!=null && finalData.createdPersonId!='' ){
                  var Employee=server.models.Employee;
                  var EmailTemplete = server.models.EmailTemplete;
                  Employee.find({'where':{'employeeId':finalData.createdPersonId}}, function (err, employeeDetails) {
                    if(employeeDetails!=null && employeeDetails.length>0){
                      EmailTemplete.findOne({"where":{"emailType": "projectWardWorks"}},function (err, emailTemplete) {
                        if(emailTemplete!=null) {
                          var message = emailTemplete.transferRequestApprovalMessage + '<br>Reason is ' + approvalDetails.comment;
                          var Dhanbademail = server.models.DhanbadEmail;
                          var notification=server.models.notification;
                          notification.create({
                            'to':[{'employeeId':employeeDetails[0].employeeId,'readStatus':false}],
                            "subject":  'New Tasks',
                            "text":"Your project Transfer funds Request is approved",
                            "message":"Your project Transfer funds Request is approved",
                            'urlData':'transferFunds',
                            "type":"approvedRequest"
                          }, function (err, notificationDetails){
                          });
                          Dhanbademail.create({
                            from: emailTemplete.emailId,
                            to: employeeDetails[0].email,
                            subject: emailTemplete.transferRequestApprovalEmail, // Subject line
                            "text":message
                          }, function (err, email) {
                          });
                          if(emailTemplete.transferRequestApprovalSMS && employeeDetails[0].mobile) {
                            var Sms = server.models.Sms;
                            var smsData = {
                              "message": emailTemplete.transferRequestApprovalSMS,
                              "mobileNo": employeeDetails[0].mobile,
                              "smsservicetype": "singlemsg"
                            };
                            Sms.create(smsData, function (err, smsInfo) {
                            });
                          }
                        }
                      });
                      cb(null, finalData);
                    }else{
                      cb(null, finalData);
                    }
                  });
                }else{
                  cb(null, finalData);
                }
              }else{
                if(finalData.workflowId!=undefined && finalData.workflowId!=null && finalData.workflowId!=''){
                  var WorkflowEmployees=server.models.WorkflowEmployees;
                  var notification=server.models.notification;
                  WorkflowEmployees.find({'where':{'workflowId':finalData.workflowId}},function (err, listData) {
                    var workflowData = [];
                    var employeeDetailsForNotification=[];
                    if (listData != undefined && listData != null && listData.length > 0) {
                      for (var i = 0; i < listData.length; i++) {
                        if(parseInt(listData[i].levelNo)==(finalData.acceptLevel+1)){
                          if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                            for(var j=0;j<listData[i].employees.length;j++){
                              employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                            }
                          }
                        }
                      }
                    }
                    if(employeeDetailsForNotification.length>0){
                      notification.create({
                        'to':employeeDetailsForNotification,
                        "subject":  'New Tasks',
                        "text":"You got New Project Transfer Funds Request",
                        "message":"You got New Project Transfer Funds Request",
                        'urlData':'transferFunds',
                        "type":"approvedRequest"
                      }, function (err, notificationDetails){
                      });

                    }else{
                      cb(null, finalData);
                    }
                  });
                }else{
                  cb(null, finalData);
                }
              }
              cb(null, finalData);
            });
          }
        }
        else{
          var error = new Error('This level is already approved. Can you please refresh page');
          error.statusCode = 200;
          cb(error,null);
        }

      }else {
        var error = new Error('Your Transfer funds details does not exits');
        error.statusCode = 200;
        cb(error,null);
      }

    });
  };

  function updateFunds(fieldVisitDetails,cb){
    if(fieldVisitDetails!=null && fieldVisitDetails.transferAmount!=undefined &&
      fieldVisitDetails.transferAmount!=null && fieldVisitDetails.transferAmount!=''){
      var ProjectTasks=server.models.ProjectTasks;
      if(fieldVisitDetails.fromHeaderId!=undefined &&
        fieldVisitDetails.fromHeaderId!=null && fieldVisitDetails.fromHeaderId!=''){
        ProjectTasks.findOne({'where':{'id':fieldVisitDetails.fromHeaderId}},function(err, fromHeaderData){
          if(fromHeaderData!=null && fromHeaderData.taskList!=null && fromHeaderData.taskList.length>0) {
            var taskList = fromHeaderData.taskList;
            if (taskList != null && fromHeaderData.taskList.length > 0) {
              var estimatedCost;
              var utilizedCost;
              for (var i = 0; i < taskList.length; i++) {
                if (fieldVisitDetails.fromSubTask == taskList[i].subTaskName) {
                  if (taskList[i].utilizedCost != undefined && taskList[i].utilizedCost != null && taskList[i].utilizedCost != ''
                    && taskList[i].estimatedCost != undefined && taskList[i].estimatedCost != null && taskList[i].estimatedCost != '') {
                    estimatedCost = parseInt(taskList[i].estimatedCost);
                    utilizedCost = parseInt(taskList[i].utilizedCost);
                    break;
                  } else {
                    estimatedCost = 0;
                    utilizedCost = 0;
                    break;
                  }
                }
              }

              if (estimatedCost > 0 && utilizedCost >= 0) {
                var finalAvalibleAmount = estimatedCost - utilizedCost;
                var transferAmount = parseInt(fieldVisitDetails.transferAmount);
                if (transferAmount <= finalAvalibleAmount) {
                  var upDatedData = {};
                  var updatedTaskList = [];
                  for (var i = 0; i < taskList.length; i++) {
                    if (fieldVisitDetails.fromSubTask == taskList[i].subTaskName) {
                      if (taskList[i].utilizedCost != undefined && taskList[i].utilizedCost != null && taskList[i].utilizedCost != ''
                        && taskList[i].estimatedCost != undefined && taskList[i].estimatedCost != null && taskList[i].estimatedCost != '') {
                        taskList[i].utilizedCost = (utilizedCost + transferAmount);
                        updatedTaskList.push(taskList[i]);
                      } else {
                        updatedTaskList.push(taskList[i]);
                      }
                    }
                  }
                  upDatedData.taskList = updatedTaskList;
                  fromHeaderData.updateAttributes(updatedTaskList, function (err, data) {
                  });
                  ProjectTasks.findOne({'where': {'id': fieldVisitDetails.toHeaderId}}, function (err, toHeaderData) {
                    var toTaskList = toHeaderData.taskList;
                    if (toTaskList != null && toTaskList.length > 0) {
                      var toEstimatedCost;
                      for (var j = 0; j < toTaskList.length; j++) {
                        if (fieldVisitDetails.toSubTask == toTaskList[j].subTaskName) {
                          if(toTaskList[j].subTaskName!=undefined && toTaskList[j].subTaskName!=null && toTaskList[j].subTaskName!=''){
                            toEstimatedCost=toTaskList[j].estimatedCost;
                            break;
                          }else{
                            toEstimatedCost=0;
                            break;
                          }
                        }
                      }
                      toEstimatedCost=toEstimatedCost+transferAmount;
                      var finalObjectForUpdate={};
                      var finaltaskList=[];
                      for (var j = 0; j < toTaskList.length; j++) {
                        if (fieldVisitDetails.toSubTask == toTaskList[j].subTaskName) {
                          if(toTaskList[j].subTaskName!=undefined && toTaskList[j].subTaskName!=null && toTaskList[j].subTaskName!=''){
                            toTaskList[j].estimatedCost=toEstimatedCost;
                            finaltaskList.push(toTaskList[j]);
                          }else{
                            finaltaskList.push(toTaskList[j]);
                          }
                        }
                      }
                      upDatedData.taskList = finaltaskList;
                      toHeaderData.updateAttributes(upDatedData,function(err, finalData){
                        cb(null, upDatedData);
                      });
                    } else {
                      var error = new Error('To header task details does not exits');
                      error.statusCode = 200;
                      cb(error,null);
                    }
                  });
                } else {
                  var error = new Error('The amount is less than selected task');
                  error.statusCode = 200;
                  cb(error,null);
                }
              } else {
                var error = new Error('Please provide Amount Details');
                error.statusCode = 200;
                cb(error,null);
              }


            } else {
              var error = new Error('Please provide valid from plan tasks');
              error.statusCode = 200;
              cb(error,null);
            }
          }else {
            var error = new Error('Please provide valid from plan tasks');
            error.statusCode = 200;
            cb(error,null);
          }
        });
      }
      else{
        var error = new Error('Please select valid header details');
        error.statusCode = 200;
        cb(error,null);
      }
    }else{
      var error = new Error('Please enter valid details');
      error.statusCode = 200;
      cb(error,null);
    }
  }

  Transferfund.remoteMethod('updateDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateDetails',
      verb: 'POST'
    }
  });

  Transferfund.updateContent= function (updatedDetails, cb) {

    Transferfund.findOne({'where':{'id':updatedDetails.id}},function(err, fieldVisitDetails){
      var WorkflowForm=server.models.WorkflowForm;
      var Workflow=server.models.Workflow;
      var WorkflowEmployees=server.models.WorkflowEmployees;
      var notification=server.models.notification;
      WorkflowForm.findOne({"where": {"and":[{"status": "Active"},{"schemeUniqueId": "fieldVisit"}]}}, function (err, workflowForm) {
        if(workflowForm!=undefined && workflowForm!=null ){
          Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
            WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
              var workflowData = [];
              var employeeDetailsForNotification=[];
              if (listData != undefined && listData != null && listData.length > 0) {
                for (var i = 0; i < listData.length; i++) {
                  var flowData = {
                    workflowId: listData[i].workflowId,
                    levelId: listData[i].levelId,
                    status: listData[i].status,
                    maxLevels: listData[i].maxLevels,
                    levelNo: listData[i].levelNo

                  }
                  workflowData.push(flowData);
                  if(listData[i].levelNo=="1"){
                    if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                      for(var j=0;j<listData[i].employees.length;j++){
                        employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                      }
                    }
                  }
                }
              }

              updatedDetails.workflow = workflowData;
              updatedDetails.workflowId=workflowForm.workflowId;
              updatedDetails.maxlevel=flowDataList.maxLevel;
              updatedDetails.acceptLevel=0;
              updatedDetails.finalStatus=false;
              updatedDetails.lastUpdateTime=new Date();
              updatedDetails['updatedTime']=new Date();
              notification.create({
                'to':employeeDetailsForNotification,
                "subject":  'New Tasks',
                "text":"You got edit Transfer fund Request",
                "message":"You got edit Transfer fund  Request",
                'urlData':'transferFunds',
                "type":"newRequest"
              }, function (err, notificationDetails){

              });
              fieldVisitDetails.updateAttributes(updatedDetails,function(err,data){
              });
              cb(null, fieldVisitDetails);
            });

          });
        }
      });




    });
  };

  Transferfund.remoteMethod('updateContent', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateContent',
      verb: 'POST'
    }
  });

};
