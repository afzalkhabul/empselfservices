var server = require('../../server/server');
module.exports = function(Priority) {
  Priority.validatesUniquenessOf('priority', {message: 'Priority could be unique'});

  Priority.observe('before save', function (ctx, next) {

    if (ctx.instance != undefined && ctx.instance != null) {
      ctx.instance.priority=(ctx.instance.priority.toLowerCase());
      ctx.instance.createdTime = new Date();
      next();

    } else {
      if(ctx.data.priority!=undefined && ctx.data.priority!=null && ctx.data.priority!=''){
        ctx.data.priority=(ctx.data.priority.toLowerCase());
      }

      ctx.data.updatedTime = new Date();
      next();



    }
  });
};
