module.exports = function(AssetPolicies) {

  AssetPolicies.validatesUniquenessOf('policy', {message: 'Asset Policy could be unique'});
  AssetPolicies.observe('before save', function (ctx, next) {
    if (ctx.instance != undefined && ctx.instance != null) {
              ctx.instance.policy=(ctx.instance.policy.toLowerCase());
              ctx.instance.createdTime = new Date();
              next();
            } else {
              if(ctx.data.policy!=undefined && ctx.data.policy!=null && ctx.data.policy!=''){
                ctx.data.policy=(ctx.data.policy.toLowerCase());
              }
              ctx.data.updatedTime = new Date();
              next();
            }
  });
};
