var server = require('../../server/server');
module.exports = function(Nocrequest) {

  Nocrequest.observe('before save', function (ctx, next) {

    if(ctx.data!=undefined && ctx.data!=null) {
      ctx.data['updatedTime'] = new Date();
      next();
    }
    else {
      checkUniqueId(ctx,next);
      ctx.instance.createdTime=new Date();

    }
  });
  function checkUniqueId(ctx,next){
    var randomstring = require("randomstring");
    var uniqueId = randomstring.generate({
      length: 7,
      charset: 'alphanumeric'
    });
    var date=new Date();
    var month;
    var dateIS;
    if((date.getMonth()+1)<10){
      month='0'+(date.getMonth()+1);
    }else{
      month=(date.getMonth()+1);
    }
    if(date.getDate()<10){
      dateIS='0'+date.getDate();
    }else{
      dateIS=date.getDate();
    }
    var string=date.getFullYear()+''+month+dateIS;
    console.log('date is '+date.getFullYear()+'data'+string);
    uniqueId=string+uniqueId;

    Nocrequest.find({"where": {"applicationId": uniqueId}}, function (err, customers) {
      if(customers.length==0){
        ctx.instance.applicationId = uniqueId;
        var WorkflowForm=server.models.WorkflowForm;
        var Workflow=server.models.Workflow;
        var WorkflowEmployees=server.models.WorkflowEmployees;
        WorkflowForm.findOne({"where": {"schemeUniqueId": 'citizenNOCRequest'}}, function (err, workflowForm) {
          console.log('work form '+JSON.stringify(workflowForm));
          if(workflowForm!=undefined && workflowForm!=null ){
            Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
             WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
                var workflowData = [];
                if (listData != undefined && listData != null && listData.length > 0) {
                  for (var i = 0; i < listData.length; i++) {
                    var flowData = {
                      workflowId: listData[i].workflowId,
                      levelId: listData[i].levelId,
                      status: listData[i].status,
                      maxLevels: listData[i].maxLevels,
                      levelNo: listData[i].levelNo
                    }
                    workflowData.push(flowData);
                  }
                }
                ctx.instance.workflow = workflowData;
                ctx.instance.acceptLevel = 0;
                ctx.instance.finalStatus = false;
                ctx.instance.requestStatus = false;
                ctx.instance.filedStatus = false;
                ctx.instance.workflowId = workflowForm.workflowId;
                ctx.instance.maxlevel = flowDataList.maxLevel;
                ctx.instance.createdTime = new Date();
                next();
              });

            });
          }
        });
      }else{
        checkUniqueId(ctx,next);
      }
    });
  }


  Nocrequest.observe('loaded', function (ctx, next) {
    if(ctx.instance){
      if(ctx.instance.workflowId!=undefined && ctx.instance.workflowId!=null && ctx.instance.workflowId!='' ){
        var WorkflowEmployees=server.models.WorkflowEmployees;
        WorkflowEmployees.find({'where':{'and':[{'workflowId':ctx.instance.workflowId},{"status": "Active"}]}}, function (err, workflowEmployeeList) {
          if(workflowEmployeeList!=null && workflowEmployeeList.length>0){
            ctx.instance.workflowData=workflowEmployeeList;
            next();
          }else{
            next();
          }
        })
      }else{
        next();
      }
    }else{
      next();
    }
  });



  Nocrequest.getDetails = function (employee, cb) {
    Nocrequest.find({'where':{'or':[{'requestStatus':false},{finalStatus :false}]}},function (err, requestList) {
      var listRequest=[];

      if(requestList!=undefined && requestList!=null && requestList.length>0){

        var Employee=server.models.Employee;
        var adminStatus=false;
        Employee.find({'where':{"employeeId":employee.employeeId}}, function (err, employeeList) {
          if (employeeList != null && employeeList.length > 0) {
            var firstEmployeeDetails = employeeList[0];
            if (firstEmployeeDetails.role == 'superAdmin' || firstEmployeeDetails.role == 'projectAdmin') {
              adminStatus = true;
            }
            for (var i = 0; i < requestList.length; i++) {
              var data = requestList[i];
              if(data.requestStatus==false){
                var workflowDetails = data.workflowData;
                var approveFiledVisit = false;
                var acceptLevel = data.acceptLevel;
                if (workflowDetails != undefined && workflowDetails != null && workflowDetails.length > 0) {
                  for (var j = 0; j < workflowDetails.length; j++) {
                    var accessFiledVisit = false;
                    if (workflowDetails[j].employees != undefined && workflowDetails[j].employees != null && workflowDetails[j].employees.length > 0) {
                      var employeeList = workflowDetails[j].employees;
                      if (employeeList != undefined && employeeList != null && employeeList.length > 0) {
                        for (var x = 0; x < employeeList.length; x++) {
                          if (employeeList[x] == employee.employeeId) {
                            accessFiledVisit = true;
                            break;
                          }
                        }
                      }
                      if (accessFiledVisit || adminStatus) {
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                          break;
                        }
                      }
                    }
                  }
                }
                if (accessFiledVisit || adminStatus) {
                  data.editStatus = approveFiledVisit;
                  if(adminStatus){
                    data.availableStatus = adminStatus;
                  }else{
                    data.availableStatus = accessFiledVisit;
                  }
                  listRequest.push(data);
                }
              }
              else {
                  data.editStatus = false;
                    data.availableStatus = false;
                  listRequest.push(data);

                }
              }
            cb(null,listRequest);
          }else{
            cb(null,listRequest);
          }
        });

      }else{
        cb(null,listRequest);
      }
    });
  };

  Nocrequest.remoteMethod('getDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'employeeId', type: 'object', http: {source: 'query'}}],
    http: {
      path: '/getDetails',
      verb: 'GET'
    }
  });


  Nocrequest.updateDetails = function (request, cb) {
    Nocrequest.findOne({'where':{'id':request.requestId}},function (err, requestDetails) {
      if(requestDetails!=undefined  && requestDetails!=null){

        if(request.acceptLevel!=null){
          var acceptLevelStatus=false;
          var acceptLevel=request.acceptLevel;
          var rejectStatus=false;
          var finalApproval=false;
          var workflowList=[];
          var workflowData=requestDetails.workflow;
          if(workflowData!=undefined && workflowData!=null && workflowData.length>0){
            for(var i=0;i<workflowData.length;i++){
              var levelNo=parseInt(workflowData[i].levelNo);
              var maximumLevel=parseInt(workflowData[i].maxLevels);
              if(levelNo==(acceptLevel+1)){
                console.log('both are same');
                acceptLevelStatus=true;
                if((acceptLevel+1)==maximumLevel){
                  finalApproval=true;
                }
                if(request.acceptStatus=='Yes'){
                  workflowData[i].acceptStatus=request.acceptStatus;
                  workflowData[i].requestStatus=request.requestStatus;
                  workflowData[i].comment=request.comment;
                  workflowData[i].employeeId=request.employeeId;
                  workflowData[i].employeeName=request.employeeName;
                  workflowList.push(workflowData[i]);
                }else{
                  rejectStatus=true;
                  workflowData[i].acceptStatus=request.acceptStatus;
                  workflowData[i].requestStatus=request.requestStatus;
                  workflowData[i].comment=request.comment;
                  workflowData[i].employeeId=request.employeeId;
                  workflowData[i].employeeName=request.employeeName;
                  workflowList.push(workflowData[i]);
                }


              }else{
                workflowList.push(workflowData[i]);
              }
            }

          }
          if(acceptLevelStatus==true){
            var updatedData={};
            if(rejectStatus){

              updatedData.acceptLevel=request.acceptLevel+1;
              updatedData.workflow=workflowList;
              updatedData.finalStatus='Rejected';
              updatedData.requestStatus='Rejected';
              updatedData.acceptStatus='No';
              updatedData.comment=request.comment;
            }else{
              updatedData.acceptLevel=request.acceptLevel+1;
              updatedData.workflow=workflowList;
              if(finalApproval){
                updatedData.requestStatus='Approval';
                updatedData.acceptStatus='Yes';
                updatedData.comment=request.comment;
                if(request.filedStatus=='Approval'){
                  updatedData.finalStatus='Approved';
                }
              }
            }
            requestDetails.updateAttributes(updatedData,function (err, updatedDetails) {
              var Sms = server.models.Sms;
              console.log("first level if entered");
              if (updatedDetails.finalStatus != undefined && updatedDetails.finalStatus != "Approved" && updatedDetails.finalStatus != "Rejected") {
                var EmailTemplete = server.models.EmailTemplete;
                EmailTemplete.findOne({"where":{"emailType": "projectWardWorks"}},function (err, emailTemplete) {
                  var Dhanbademail = server.models.DhanbadEmail;
                  Dhanbademail.create({
                    "to": updatedDetails.emailId,
                    "subject": emailTemplete.levelEmail,
                    "text": emailTemplete.levelText
                  }, function (err, emailId) {
                    console.log(emailId);
                  });
                  if(emailTemplete.levelSMS && updatedDetails.mobile) {
                    var smsData = {
                      "message": emailTemplete.levelSMS,
                      "mobileNo": updatedDetails.mobile,
                      "smsservicetype": "singlemsg"
                    };
                    Sms.create(smsData, function (err, smsInfo) {
                    });
                  }
                });
              } else if (updatedDetails.finalStatus != undefined && updatedDetails.finalStatus == "Approved") {
                var Emailtemplete = server.models.EmailTemplete;
                EmailTemplete.findOne({"where":{"emailType": "projectWardWorks"}},function (err, emailTemplete) {
                  var Dhanbademail = server.models.DhanbadEmail;
                  Dhanbademail.create({
                    "to": updatedDetails.emailId,
                    "subject": emailTemplete.requestApprovalEmail,
                    "text": emailTemplete.requestApprovalMessage
                  }, function (err, emailId) {
                  });
                  if(emailTemplete.requestApprovalSMS && updatedDetails.mobile) {
                    var smsData = {
                      "message": emailTemplete.requestApprovalSMS,
                      "mobileNo": updatedDetails.mobile,
                      "smsservicetype": "singlemsg"
                    };
                    Sms.create(smsData, function (err, smsInfo) {
                    });
                  }
                });

              } else if (updatedDetails.finalStatus != undefined && updatedDetails.finalStatus == "Rejected") {
                var Emailtemplete = server.models.EmailTemplete;
                Emailtemplete.find({},function (err, emailTemplete) {
                  var Dhanbademail = server.models.DhanbadEmail;
                   Dhanbademail.create({
                   "to": updatedDetails.emailId,
                   "subject": emailTemplet.rejectedEmail,
                   "text": emailTemplete.rejectedMessage
                   }, function (err, emailId) {
                   });
                  if(emailTemplete.rejectedSMS && updatedDetails.mobile) {
                    var smsData = {
                      "message": emailTemplete.rejectedSMS,
                      "mobileNo": updatedDetails.mobile,
                      "smsservicetype": "singlemsg"
                    };
                    Sms.create(smsData, function (err, smsInfo) {
                    });
                  }
                });
              }
              cb(null,updatedDetails);
            })
          }
        }
      }
    })

  };

  Nocrequest.remoteMethod('updateDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object"
    },
    accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateDetails',
      verb: 'POST'
    }
  });

  //After Save Method
  Nocrequest.observe('after save', function (ctx, next) {

    if(ctx.isNewInstance){
      var Sms = server.models.Sms;
      var Emailtemplete = server.models.EmailTemplete;
      Emailtemplete.find({"where": {"emailType": "projectWardWorks"}},function (err, emailTemplete) {
        var Dhanbademail = server.models.DhanbadEmail;
         Dhanbademail.create({
         "to": ctx.instance.emailId,
         "subject": emailTemplete.requestEmail,
         "text": emailTemplete.requestText
         }, function (err, emailId) {

         });
        if(emailTemplete.requestSMS && updatedDetails.mobile) {
          var smsData = {
            "message": emailTemplete.requestSMS,
            "mobileNo": ctx.instance.mobile,
            "smsservicetype": "singlemsg"
          };
          Sms.create(smsData, function (err, smsInfo) {
          });
        }
        next();
      });
    } else {
      next();
    }
  });


};
