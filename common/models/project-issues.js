var server = require('../../server/server');
module.exports = function(Projectissues) {
  Projectissues.observe('before save', function (ctx, next) {

    if (ctx.instance != undefined && ctx.instance != null) {
      ctx.instance.createdTime = new Date();
      var notification=server.models.notification;
      var reviewPersonList=[{'employeeId':ctx.instance.employeeId,'readStatus':false}];
      if(reviewPersonList.length>0){
        notification.create({
          'to':reviewPersonList,
          "subject":  'New Tasks',
          "text":"You got one new Project issue",
          "message":"You got one new Project issue",
          'urlData':'measurementBook',
          "type":"newRequest"
        });
      }

      next();
    } else {
      ctx.data.updatedTime = new Date();
      next();
    }
  });

  Projectissues.observe('loaded', function (ctx, next) {
    if(ctx.instance){
      if(ctx.instance.employeeId!=undefined && ctx.instance.employeeId!=null){
        var Employee=server.models.Employee;
        Employee.find({'where':{'employeeId':ctx.instance.employeeId}},function(err, employee){
          if(employee.length>0){
            ctx.instance.employeeName=employee[0].name;
            next();
          }else{
            next();
          }
        });
      } else{
        next();
      }
    }else{
      next();
    }
  });

};
